cd bochs
mkdir HD
sudo mount -t vfat hd.img HD
sudo chmod 777 HD/bin/*.tsk
sudo rm HD/bin/*.tsk
cd ../taskfiles

#IDLE
nasm -f bin idle.asm
sudo cp idle ../bochs/HD/bin/idle.tsk

#HI
nasm -f bin hi.asm
sudo cp hi ../bochs/HD/bin/hi.tsk

#MEMO
nasm -f bin memo.asm
sudo cp memo ../bochs/HD/bin/memo.tsk

#VIRUS
nasm -f bin virus.asm
sudo cp virus ../bochs/HD/bin/virus.tsk

#TIMER
nasm -f bin timer.asm
sudo cp timer ../bochs/HD/bin/timer.tsk

#KEYS
nasm -f bin keys.asm
sudo cp keys ../bochs/HD/bin/keys.tsk

#COLOR
nasm -f bin color.asm
sudo cp color ../bochs/HD/bin/color.tsk

#NYAN
nasm -f bin nyan.asm
sudo cp nyan ../bochs/HD/bin/nyan.tsk

#CAT
nasm -f bin catt.asm
sudo cp catt ../bochs/HD/bin/cat.tsk

#VIM
nasm -f bin vim.asm
sudo cp vim ../bochs/HD/bin/vim.tsk

cd ../bochs
sudo umount HD
rm -rf HD
cd ..
