#ifndef __IDT_H__
#define __IDT_H__

//Initialize data in the IDT data structure
void initialize_idt();

/* Struct de descriptor de IDT */
typedef struct idt_register {
	unsigned short idt_length;
	unsigned long idt_addr;
} __attribute__((__packed__)) idt_register_t;

/* Struct de una entrada de la IDT */
typedef struct idt_entry {
		unsigned short offset_0_15;
		unsigned short segsel;
		unsigned short attr;
		unsigned short offset_16_31;
		unsigned int offset_32_63;
		unsigned int reservado;
} __attribute__((__packed__, aligned (8))) idt_entry_t;

extern idt_entry_t idt[];
extern idt_register_t idt_register;

#endif //__IDT_H__
