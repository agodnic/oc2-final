#include "idt.h"
#include "../isr/isr.h"
#include "../hw/ia32e.h"

#define IDT_ENTRY(numero) \
	idt[numero].offset_0_15 = (unsigned short) ((unsigned long)(&_isr ## numero) & (unsigned long) 0xFFFF); \
	idt[numero].segsel = (unsigned short) 0x0028; \
	idt[numero].attr = (unsigned short) 0x8e00; \
	idt[numero].offset_16_31 = (unsigned short) ((unsigned long)(&_isr ## numero) >> 16 & (unsigned long) 0xFFFF); \
	idt[numero].offset_32_63 = (unsigned int) ((unsigned long)(&_isr ## numero) >> 32 & (unsigned long) 0xFFFF); \
	idt[numero].reservado = (unsigned int) 0x00000000;

void initialize_idt() {
	IDT_ENTRY(0);//Divide error							-	Fault
	IDT_ENTRY(2);//NMI (Nonmaskable external interrupt)	-	Interrupt
	IDT_ENTRY(3);//Breakpoint							-	Trap
	IDT_ENTRY(4);//Overflow								-	Trap
	IDT_ENTRY(5);//Bound range exceeded					-	Fault
	IDT_ENTRY(6);//Invalid Opcode						-	Fault
	IDT_ENTRY(7);//Device not available 				-	Fault
	IDT_ENTRY(8);//Double fault							-	Abort
	IDT_ENTRY(10);//Invalid TSS							-	Fault
	IDT_ENTRY(11);//Segment not present					-	Fault
	IDT_ENTRY(12);//Stack segment fault					-	Fault
	IDT_ENTRY(13);//General protection Fault			-	Fault
	IDT_ENTRY(14);//Page fault							-	Fault
	IDT_ENTRY(16);//FPU error (math fault)				-	Fault
	IDT_ENTRY(17);//Aligment check						-	Fault
	IDT_ENTRY(18);//Machine check						-	Abort
	IDT_ENTRY(19);//SIMD exception						-	Fault
	IDT_ENTRY(32);//Clock
	IDT_ENTRY(33);//Teclado
	IDT_ENTRY(36);//Sys Call
}

idt_entry_t idt[255] = {};

idt_register_t idt_register = {sizeof(idt)-1, (unsigned long)&idt};
