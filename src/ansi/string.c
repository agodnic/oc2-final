#include "string.h"

void* memset(void* s, char c, size_t n) {

	char* dst = (char*) s;

	while(n--)
		dst[n] = c;
	return s;
}

void* memcpy(void* dest, const void* src, size_t n){
 char* d = dest, *s = (void*) src; 
 while(n--)
 *(d++) = *(s++);

 return dest;
}


char* strcpy(char* dst, const char* src){

	char* p = dst;
	while(*src)
		*(p++) = *(src++);

	return dst;
}

//this function does not respect ANSI c known strncmp() interface
int strncmp(const char* s1, const char* s2, size_t n) {

	int i;
	for (i=0; i<n; i++)
		if(s1[i] != s2[i]) return 1;

	return 0;
}
