#ifndef __STRING_H__
#define __STRING_H__

typedef unsigned int size_t;

void* memset(void* s, char c, size_t n);

void* memcpy(void* dest, const void* src, size_t n);

char* strcpy(char* dest, const char* src);

//this function does not respect ANSI c known strncmp() interface
int strncmp(const char* s1, const char* s2, size_t n);

#endif
