#ifndef __SCHED_H__
#define __SCHED_H__

//initialize scheduler structures
void sched_init();
//returns idle stack
unsigned long idle_stack();
//returns idle cr3
unsigned long idle_cr3();

//loads a new task from a file in path
//if succeed, returns pid
//if there is no more space for tasks, returns -1
//if the path is invalid returns 0
int load_task(char* path);
int load_task_q(char* path, unsigned int quantum);
int load_task_i(char* path, int pid, unsigned int quantum);

void run_task(int pid);

//returns the pid of the current task
int get_current_pid();
//returns the current quantum
int get_current_quantum();
//retuns the state of the task pid
int get_state(int pid);
//retuns the stack of the task pid
unsigned long get_stack(int pid);
//retuns the cr3 of the task pid
unsigned long get_cr3(int pid);
//returns if pid is running for the first time
char is_first_exec(int pid);

//tick: called on clock interrupt
unsigned long sched_tick(unsigned long current_stack);
//switch to the next task
unsigned long switch_task(unsigned long current_stack);
//switch to the next task
unsigned long task_ended();

//ends a task (syscall)
void end_task_sys();
//ends a task (command)
void end_task_cmd(int pid);

//blocks a task
void block_task(int pid);
//unblocks a task
void unblock(int pid);
//saves rax value on pid context
void save_syscall_result(int pid, unsigned long rax);

//sets a task's quantum
void set_quantum(int pid, int q);
//returns task's quantum
unsigned int get_quantum(int pid);

char* task_name(int pid);
void name_task(int pid, char* name);

void update_context(unsigned long rsp);

void print_task_registers(int pid);

#endif //__SCHED_H__
