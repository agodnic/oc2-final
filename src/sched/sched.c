#include "sched.h"

#include "../mmu/mmu.h"
#include "../cui/cui.h"
#include "../hw/ia32e.h"
#include "../fat/fat12.h"
#include "../screen/screen.h"
#include "../ansi/string.h"		// Para str_cpy
#include "../cui/dic.h"			// Para int_to_string
#include "../term/input.h"		// Para clear_input
#include "../isr/isr.h"			// Para init_blocking_sched
#include "../screen/taskscrn.h"	// Para init_task_scrn
#include "../constants.h"

//TODO: registros que faltan
typedef struct task_context {
	unsigned long cr3;
	unsigned long flags;
	unsigned long r15;
	unsigned long r14;
	unsigned long r13;
	unsigned long r12;
	unsigned long r11;
	unsigned long r10;
	unsigned long r9;
	unsigned long r8;
	unsigned long rbp1;
	unsigned long rbp2;
	unsigned long rdi;
	unsigned long rsi;
	unsigned long rdx;
	unsigned long rcx;
	unsigned long rbx;
	unsigned long rax;
	unsigned long rip;
} task_context_t;

static unsigned long task_RSP[NUMBER_OF_TASKS];			//array of RSPs registers
static unsigned long task_CR3[NUMBER_OF_TASKS];		//array of CR3s registers
unsigned int task_quantum[NUMBER_OF_TASKS];				//array of quantums
static int current_pid = 0;								//current task pid 
unsigned int current_quantum = 1;						//current quantum
char tasks[24*NUMBER_OF_TASKS];							//name of tasks
int task_state[NUMBER_OF_TASKS];						//state of task
unsigned long task_context[NUMBER_OF_TASKS];			//array of context init
char first_exec[NUMBER_OF_TASKS];						//bool array: ¿first exec?

//returns an empty pid, -1 if no more avaiable
static int new_task_id();
//returns the pid of the next task
static int next_task_id();
//return the index of the char between path & params
//if there are no params returns -1
int parse_path(char* path);
//sets params where the task can find them
void parse_params(char* params, task_context_t* context, int pid);
//search for a similar path when a file is not found
void smart_path(char* path, char* new_path, int path_end);

extern void sched_end();

void sched_init(){
	int i;
	char* unnamed = "Unnamed Task\n     \n";
	for(i = 0; i < NUMBER_OF_TASKS; i++) {
		task_RSP[i] = 0;
		task_CR3[i] = 0;
		task_quantum[i] = 0;
		name_task(i,unnamed);
		task_state[i] = EMPTY;
		task_context[i] = 0;
		first_exec[i] = 1;
	}
	init_blocking_sched();
}

unsigned long idle_stack() {
	return task_RSP[0];
}

unsigned long idle_cr3() {
	return task_CR3[0];
}

int load_task(char* path){
	return load_task_q(path, DEFAULT_QUANTUM);
}

int load_task_i(char* path, int pid, unsigned int quantum) {

	if (task_state[pid] != EMPTY) return 0;
	//Paginación
	unsigned long new_cr3 = init_user_page_dir(pid);
	task_CR3[pid] = new_cr3;

	//Código
	int path_end = parse_path(path);
	malloc_page_u(VIRTUAL_TASK_CODE, pid, HD);
	int file_size = read_file(path,
	(void*) (va2phy(VIRTUAL_TASK_CODE, new_cr3)), 0);

	if (!file_size) {
		char new_path[78];
		smart_path(path, (char*) new_path, path_end);
		file_size = read_file((char*) new_path,
		(void*) (va2phy(VIRTUAL_TASK_CODE, new_cr3)), 0);
		if (!file_size) {
			free_page_u(VIRTUAL_TASK_CODE, new_cr3);
			free_user_page_dir(pid);
			return 0;
		}
	}

	//Context
	unsigned long stack = malloc_page_u(NULL, pid, STACK);

	task_RSP[pid] = stack + 0x1000 - sizeof(task_context_t);
	task_context_t* context = (task_context_t*) (task_RSP[pid]);
	task_context[pid] = task_RSP[pid];

	context->cr3		= new_cr3;
	context->flags		= 0x206;				//if set
	context->rip		= VIRTUAL_TASK_CODE;

	//Parametros
	if (path_end != -1) {
		char* params = path+path_end;
		parse_params(params, context, pid);
	} else {
		context->rdi	= 0;
	}

	task_quantum[pid] = quantum;
	task_state[pid] = LOADED;
	first_exec[pid] = 1;

	init_task_scrn(pid);

	return pid;
}

int load_task_q(char* path, unsigned int quantum) {

	int pid = new_task_id();
	if (pid != NO_PID) pid = load_task_i(path, pid, quantum);
	return pid;
}

void run_task(int pid) {
	if (task_state[pid] == LOADED) task_state[pid] = RUNNING;
}

int get_current_pid() {return current_pid;}
int get_current_quantum() {return current_quantum;}
int get_state(int pid) {return task_state[pid];}
unsigned long get_stack(int pid) {return task_RSP[pid];}
unsigned long get_cr3(int pid) {return task_CR3[pid];}
char is_first_exec(int pid) {
	if (first_exec[pid]) {first_exec[pid]=0; return 1;}
	else return 0;
}

unsigned long sched_tick(unsigned long current_stack) {
	unsigned long stack = current_stack;
	current_quantum--;
	if (current_quantum == 0) stack = switch_task(current_stack);
	update_screen_tick();
	return stack;
}

unsigned long switch_task(unsigned long current_stack) {
	unsigned long stack = current_stack;
	int new_pid = next_task_id();
	current_quantum = task_quantum[new_pid];
	if (new_pid != current_pid) {
		task_RSP[current_pid] = current_stack;
		stack = task_RSP[new_pid];
		current_pid = new_pid;
	}
	return stack;
}

unsigned long task_ended() {
	int new_pid = next_task_id();
	unsigned long stack = task_RSP[new_pid];
	current_pid = new_pid;
	current_quantum = task_quantum[current_pid];
	return stack;
}

static int new_task_id(){
	int i;
	for(i = 0; i < NUMBER_OF_TASKS; i++)
		if(task_state[i] == EMPTY) return i;

	return -1;
}

static int next_task_id() {
	int i;
	int new_pid = current_pid;
	for(i = 1; i != NUMBER_OF_TASKS; i++) {
		if(task_state[ (current_pid + i) % NUMBER_OF_TASKS] == RUNNING) {
			new_pid = (current_pid + i) % NUMBER_OF_TASKS;
			if (task_state[current_pid] == EMPTY) update_screen_end(current_pid);
			else if (task_state[current_pid] == RUNNING) update_screen_time_out(current_pid);
			update_screen_switch(current_pid, new_pid);
			break;
		}
	}
	return new_pid;
}

void end_task_common(int pid) {
	end_block(pid);
	char* unnamed = "Unnamed Task\n     \n";
	name_task(pid, unnamed);
	task_state[pid] = EMPTY;
	update_screen_end(pid);
	end_task_scrn(pid);
	free_user_page_dir(pid);
	task_RSP[pid] = 0;
	task_context[pid] = 0;
	first_exec[pid] = 1;
	if (get_menu() == 6+pid) {
		set_menu(1);
		terminal_view();
	}
}

void end_task_sys() {
	end_task_common(current_pid);
	//never return from this call:
	sched_end();
}

void end_task_cmd(int pid) {
	end_task_common(pid);

	if (pid == current_pid) {
		if (get_focus()) clear_input();
		//never return from this call:
		sched_end();
	}
}

void block_task(int pid) {
	task_state[pid] = BLOCKED;
	update_screen_block(pid);
	if (pid == current_pid) {
		sched_block();
	}
}

void unblock(int pid) {
	if (task_state[pid]==BLOCKED) {
		task_state[pid] = RUNNING;
		update_screen_unblock(pid);
	}
}

void save_syscall_result(int pid, unsigned long rax) {
	task_context_t* context = (task_context_t*) (task_context[pid]);
	context->rax = rax;
}

void set_quantum(int pid, int q) {
	if (q < 1) task_quantum[pid] = 1;
	else if (q > 999) task_quantum[pid] = 999;
	else task_quantum[pid] = q;
}

unsigned int get_quantum(int pid) {
	if (task_state[pid] == EMPTY)
		return DEFAULT_QUANTUM;
	else
		return task_quantum[pid];
}

char* task_name(int pid) {
	char* res = &tasks[24*pid];
	return res;
}

void name_task(int pid, char* name) {
	int i = 0;
	char* task = &tasks[24*pid];
	task[2] = ']';
	int_to_string(pid, task, 2);
	for (; (i+4 < 23) && (name[i] != 10); i++) {
		task[i+4] = name[i];
	}
	task[i+4] = 10;
	update_screen_name(pid);
	update_task_screen_name(pid);
}

void update_context(unsigned long rsp) {
	task_context[current_pid] = rsp;
}

void smart_path(char* path, char* new_path, int path_end) {
	char* pre = "/BIN/A";
	char* pos = ".TSKZ";
	pre[5] = 0;
	pos[4] = 0;
	if (strncmp(path, pre, 5)) {
		strcpy(new_path, pre);
		new_path += 5;
	}
	if (path_end < 0) {
		path_end = 0;
		while (path[path_end] != 10) {
			path_end++;
		}
	}
	path[path_end] = 0;
	strcpy(new_path, path);
	new_path += path_end;
	char* tmp = path + path_end -4;
	if ((tmp < path) || strncmp(tmp, pos, 4)) {
		strcpy(new_path, pos);
	}
	new_path[4] = '\n';
}

int parse_path(char* path) {
	int i = 0;
	while (path[i] != 32) {
		if (path[i] == 10) {
			return -1;//no params
		}
		i++;
	}
	path[i] = '\n';
	return i;
}

//retorna la sigueinte dirección múltiplo de 8
unsigned long advance_next_packed_8(unsigned long dir) {
	unsigned long result = dir;
	while (result%8 != 0) result++;
	return result;
}

void parse_params(char* params, task_context_t* context, int pid) {
	char* page = (char*) malloc_page_u(NULL, pid, OTHER);
	memset((void*) page, 0, PAGE_SIZE);
	char* str_param = page;
	char* param_buf = params+1;
	int strlong = 0;
	int cant_params = 1;
	//Parametros en param_buf divididos por espacios (32) con un 10 al final.
	while (param_buf[strlong] != 10) {
		if (param_buf[strlong] == 32) {
			cant_params++;			//Si hay un espacio, va a haber otro...
			param_buf[strlong] = 0;
			if (strlong > 0) {
				strcpy((void*) str_param, (void*) param_buf);
				str_param += strlong;
				str_param[0] = 10;
				str_param++;
				str_param = (char*) advance_next_packed_8((unsigned long) str_param);
			} else {
				cant_params--;		//Si no hay chars, no era un parámetro
			}
			param_buf += strlong+1;
			strlong = -1;
		}
		strlong++;
	}
	param_buf[strlong] = 0;
	if (strlong > 0) {
		strcpy(str_param, (void*) param_buf);
		str_param += strlong;
		str_param[0] = 10;
		str_param++;
	} else {
		cant_params--;
	}
	str_param[0] = 0;
	str_param++;
	str_param = (char*) advance_next_packed_8((unsigned long) str_param);
	if (cant_params > 0) {
		//Parametros en page divididos por \n (10) con un 0 al final
		unsigned long* param_dir = (unsigned long*) str_param;
		int i = 0;
		int i_param = 0;
		char* str_param = page;
		param_dir[i_param] = (unsigned long) str_param;
		while (i_param < cant_params) {
			if (page[i]==10) {
				i++;
				while (i%8 != 0) i++;
				str_param = page+i;
				str_param = (char*) advance_next_packed_8((unsigned long) str_param);
				i_param++;
				param_dir[i_param] = (unsigned long) str_param;
			} else i++;
		}
		context->rdi = (unsigned long) cant_params;
		context->rsi = (unsigned long) param_dir;
	} else {
		context->rdi = 0;
	}
}

void print_task_registers(int pid) {
	pixel_t s = {4, 8}; pixel_t e = {20, 17};
	paint_c(s, e, 0x0F, 32);
	task_context_t* context = (task_context_t*) (task_context[pid]);
	char* rax = "0x        \n";
	rax[long_to_hex(context->rax, rax+2, 8)+2]=10;
	print(s, rax);
	s.row++;
	char* rbx = "0x        \n";
	rbx[long_to_hex(context->rbx, rbx+2, 8)+2]=10;
	print(s, rbx);
	s.row++;
	char* rcx = "0x        \n";
	rcx[long_to_hex(context->rcx, rcx+2, 8)+2]=10;
	print(s, rcx);
	s.row++;
	char* rdx = "0x        \n";
	rdx[long_to_hex(context->rdx, rdx+2, 8)+2]=10;
	print(s, rdx);
	s.row++;
	char* rdi = "0x        \n";
	rdi[long_to_hex(context->rdi, rdi+2, 8)+2]=10;
	print(s, rdi);
	s.row++;
	char* rsi = "0x        \n";
	rsi[long_to_hex(context->rsi, rsi+2, 8)+2]=10;
	print(s, rsi);
	s.row++;
	char* r8 = "0x        \n";
	r8[long_to_hex(context->r8, r8+2, 8)+2]=10;
	print(s, r8);
	s.row++;
	char* r9 = "0x        \n";
	r9[long_to_hex(context->r9, r9+2, 8)+2]=10;
	print(s, r9);
	s.row++;
	char* r10 = "0x        \n";
	r10[long_to_hex(context->r10, r10+2, 8)+2]=10;
	print(s, r10);
	s.row++;
	char* r11 = "0x        \n";
	r11[long_to_hex(context->r11, r11+2, 8)+2]=10;
	print(s, r11);
	s.row++;
	char* r12 = "0x        \n";
	r12[long_to_hex(context->r12, r12+2, 8)+2]=10;
	print(s, r12);
	s.row++;
	char* r13 = "0x        \n";
	r13[long_to_hex(context->r13, r13+2, 8)+2]=10;
	print(s, r13);
	s.row++;
	char* r14 = "0x        \n";
	r14[long_to_hex(context->r14, r14+2, 8)+2]=10;
	print(s, r14);
	s.row++;
	char* r15 = "0x        \n";
	r15[long_to_hex(context->r15, r15+2, 8)+2]=10;
	print(s, r15);
	s.row++;
	char* cr3 = "0x        \n";
	cr3[long_to_hex(context->cr3, cr3+2, 8)+2]=10;
	print(s, cr3);
	s.row++;
	char* flg = "0x        \n";
	flg[long_to_hex(context->flags, flg+2, 8)+2]=10;
	print(s, flg);
	s.row++;
	char* rip = "0x        \n";
	rip[long_to_hex(context->rip, rip+2, 8)+2]=10;
	print(s, rip);
	s.row++;
}
