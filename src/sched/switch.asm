%include 'isr/context.ha'
; returns as another task if context switch was needed
;
global sched_clock
sched_clock:

	PUSH_REGISTERS

	;exchange stack between current pid and next one 
	mov	rdi,	rsp
	extern	sched_tick
	call	sched_tick

	;Restore Context
	mov	rsp,	rax
	POP_AND_RETURN

global sched_block
sched_block:

	PUSH_REGISTERS

	;exchange stack between current pid and next one
	mov	rdi,	rsp
	extern	switch_task
	call	switch_task

	;Restore Context
	mov	rsp,	rax
	POP_AND_RETURN

global sched_end
sched_end:

	;exchange stack between current pid and next one
	extern	task_ended
	call	task_ended

	;Restore Context
	mov	rsp,	rax
	POP_AND_RETURN

global idle_start
idle_start:
	;load RSP
	extern	idle_stack
	call	idle_stack
	mov		rsp, rax
	POP_AND_RETURN
