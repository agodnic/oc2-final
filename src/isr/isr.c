#include "isr.h"

#include "../screen/screen.h"	// Para guardar y cargar la pantalla
#include "../screen/mmuscrn.h"	// Para teclado de mmu
#include "../screen/schedscrn.h"// Para teclado de scheduler
#include "../screen/taskscrn.h"	// Para teclado de task
#include "../cui/cui.h"			// Para 'print_m'
#include "../hw/pic.h"			// Para 'fin_intr_pic1'
#include "../term/input.h"		// Para keyboard input
#include "../term/term.h"		// Para keyboard terminal
#include "../sched/sched.h"
#include "../hw/ia32e.h"
#include "syscall.h"			// Para search_syscall
#include "../cui/clock.h"
#include "../cui/key.h"
#include "../constants.h"

// icon shown on clock status position
unsigned char counter = 0;
int timer_leng = 4;
char timer[] =  {('/'),('-'),('\\'),('|')};

unsigned char menu = 0;	// 0 : nada (cargando...)
							// 1 : terminal
							// 2 : Sched
							// 3 : File Browser
							// 4 : Regs
							// 5 : MMU

unsigned char focus_on_input = 1;	//is true if the input field is focused

int task_waiting_time[NUMBER_OF_TASKS];

unsigned int task_waiting_key[NUMBER_OF_TASKS];

//Strings to show on interrupt screen:
char* int_ids[] = {"#DE\n","\n","- -\n","#BP\n","#OF\n","#BR\n","#UD\n",
					"#NM\n","#DF\n","\n","#TS\n","#NP\n","#SS\n","#GP\n",
					"#PF\n","\n","#MF\n","#AC\n","#MC\n","#XM\n"};

char* int_names[] = {"Divide Error\n","\n","NMI Interrupt\n","Breakpoint\n",
					"Overflow\n","Bound Range Exceeded\n","Invalid Opcode\n",
					"Device Not Avaible\n","Double Fault\n","\n",
					"Invalid TSS\n","Segment Not Present\n",
					"Stack-Segment Fault\n","General Protection Fault\n",
					"Page Fault\n","\n","x87 FPU Floating-Point Error\n",
					"Aligment Check\n","Machine Check\n",
					"SIMD Floating-Point Exception\n"};

char* int_ns[] = {"0\n","\n","2\n","3\n","4\n","5\n","6\n","7\n","8\n","\n",
				"10\n","11\n","12\n","13\n","14\n","\n","16\n","17\n",
				"18\n","19\n"};

//secondary functions
void proximo_reloj();
void alt_focus();
void clock_task();
void keyboard_task(unsigned char k);

void isr_general(unsigned int interrupt) {

	interrupt_save_screen();	//saves the current screen so we can restore it
								//after the interrupt
	rainbow();

	pixel_t title = {10,3};
	print_t( title, "El Sistema ha sido interrumpido\n", 5 );

	char* interrupt_id = int_ids[interrupt];
	char* interrupt_name = int_names[interrupt];
	char* interrupt_n = int_ns[interrupt];

	pixel_t subtitle = {12,3};

	print_t( subtitle, "Tipo de interrupcion \n", 5 );

	subtitle.column += 21;
	print_t( subtitle, interrupt_n, 5 );

	subtitle.column += 2;
	print_t( subtitle, "(\n", 5 );

	subtitle.column ++;
	print_t( subtitle, interrupt_id, 5 );

	subtitle.column += 3;
	print_t( subtitle, "): \n", 5);

	subtitle.column += 2;
	print_t( subtitle, interrupt_name, 5 );

	pixel_t cont_msg = {14,3};

	print_t( cont_msg,
		"Presione Escape para ver el estado de los registros\n", 5);

	wait_for_key(KEY_ESC);	//esperar a que se presione escape

	print_registers(1);		//imprimir registros
	pixel_t end_msg = {21,3};	
	print_t( end_msg,
		"Presione Escape para continuar\n", 5);

	wait_for_key(KEY_ESC);	//esperar a que se presione escape

	interrupt_load_screen();
}

void clock_isr() {
	if (is_waiting()) {
		wait_clock();
	} else {
		proximo_reloj();
		if (menu==MENU_REGS) update_registers();
	}
	clock_task();
}

void keyboard_isr() {
	unsigned char key = pressed_key();
	keyboard_task(key);
	if ((!is_waiting()) && (menu != MENU_NULL)) {
		if (key == KEY_TAB) alt_focus();
		else if (focus_on_input) {			//writing on input field
			if (char_key(key)) {
				input_char(key_to_char(key));
			}
			else if (key == KEY_BACKSPACE) input_back();
			else if (key == KEY_ENTER) input_enter();
			else if (key == KEY_RIGHT) input_right();
			else if (key == KEY_LEFT) input_left();
	//		else if (key == KEY_UP) ... ;				//avaiable
	//		else if (key == KEY_DOWN) ... ;				//avaiable
			else if (key == KEY_INSERT) input_insert();
			else if (key == KEY_DELETE) input_delete();
			else if (key == KEY_HOME) input_home();
			else if (key == KEY_END) input_end();
			else if (key == KEY_L_SHIFT) input_shift();
			else if (key == KEY_R_SHIFT) input_shift();
			else if (key == KEY_CAPS) input_caps();
			else if (key == BREAK_L_SHIFT) break_shift();
			else if (key == BREAK_R_SHIFT) break_shift();
		} else if (menu == MENU_TERMINAL) {
				 if (key == KEY_UP) term_up();
			else if (key == KEY_DOWN) term_down();
			else if (key == KEY_PGUP) term_pgup();
			else if (key == KEY_PGDN) term_pgdn();
			else if (key == KEY_HOME) term_home();
			else if (key == KEY_END) term_end();
		} else if (menu == MENU_SCHED) {
				 if (key == KEY_UP) sched_up();
			else if (key == KEY_DOWN) sched_down();
			else if (key == KEY_LEFT) sched_left();
			else if (key == KEY_RIGHT) sched_right();
			else if (key == KEY_L) sched_load();
			else if (key == KEY_R) sched_run();
			else if (key == KEY_S) sched_run_all();
			else if (key == KEY_PLUS) sched_qu();
			else if (key == KEY_MINUS) sched_qd();
			else if (key == KEY_K) sched_kill();
			else if (key == KEY_B) sched_block_();
			else if (key == KEY_U) sched_unblock();
		} else if (menu == MENU_BROWSE) {
		} else if (menu == MENU_REGS) {
				 if (key == KEY_UP) regs_up();
			else if (key == KEY_DOWN) regs_dn();
			else if (key == KEY_RIGHT) regs_right();
			else if (key == KEY_LEFT) regs_left();
		} else if (menu == MENU_MMU) {
				 if (key == KEY_UP) mmu_up();
			else if (key == KEY_DOWN) mmu_down();
			else if (key == KEY_LEFT) mmu_left();
			else if (key == KEY_RIGHT) mmu_right();
			else if (key == KEY_M) mmu_malloc();
			else if (key == KEY_F) mmu_free();
			else if (key == KEY_PLUS) mmu_owner_up();
			else if (key == KEY_MINUS) mmu_owner_dn();
			else if (key == KEY_PGDN) mmu_type_up();
			else if (key == KEY_PGUP) mmu_type_dn();
		} else {										//Task
				 if (key == KEY_PLUS) task_qu();
			else if (key == KEY_MINUS) task_qd();
			else if (key == KEY_ENTER) task_run();
			else if (key == KEY_F4) task_kill();
		}
	}
}

void syscall_isr(unsigned int id, void* p1,
	void* p2, void* p3, void* p4, void* p5) {
	void* params[5];
	params[0] = p1;		//RSI
	params[1] = p2;		//RDX
	params[2] = p3;		//RCX
	params[3] = p4;		//R8
	params[4] = p5;		//R9
	search_syscall(id, params);
}

void set_menu(unsigned char m) {
	menu = m;
}

unsigned char get_menu() {
	return menu;
}

unsigned char get_focus() {
	return focus_on_input;
}

void init_blocking_sched() {
	int i = 0;
	for (; i< NUMBER_OF_TASKS; i++) {
		task_waiting_time[i] = 0;
		task_waiting_key[i] = 0;
	}
}

void task_wait(unsigned int time) {
	int pid = get_current_pid();
	task_waiting_time[pid] = time;
	block_task(pid);
}
void task_wait_for_key(unsigned char key) {
	int pid = get_current_pid();
	task_waiting_key[pid] = key;
	block_task(pid);
}

void end_block(int pid) {
	task_waiting_key[pid] = 0;
	task_waiting_time[pid] = 0;
}

void proximo_reloj() {
	pixel_t clock = {24, 79};
	counter++;
	if (counter==timer_leng) counter = 0;
	set_char( clock, timer[counter]);
}

void alt_focus() {
	pixel_t s = {23,2};
	pixel_t e = {23,77};
	focus_on_input = !focus_on_input;
	int pid = get_current_pid();
	if (focus_on_input) {
		paint((pixel_t){0,0}, (pixel_t){0,79}, BG_WHITE | BRIGHT | CHAR_BLACK);
		if (menu == MENU_TERMINAL) paint((pixel_t){2,1}, (pixel_t){22,78}, BG_WHITE | CHAR_BLACK);
		if (menu == MENU_SCHED) {
			paint((pixel_t){2,1}, (pixel_t){22,78}, BG_WHITE | CHAR_BLACK);
			update_screen_switch(pid, pid);
			paint_task();
		} else if (menu == MENU_MMU) {
			paint((pixel_t){2,7}, (pixel_t){22,72}, BG_BLACK | CHAR_WHITE);
			paint_pos(0);
		} else if (menu == MENU_REGS) {
			paint((pixel_t){2,1}, (pixel_t){22,78}, BG_BLACK | CHAR_WHITE);
		}
		paint(s, e, BG_YELLOW | CHAR_BLACK);
		paint_pointer();
	} else {
		paint((pixel_t){0,0}, (pixel_t){0,79}, BG_BLUE | BRIGHT | CHAR_YELLOW);
		if (menu == MENU_TERMINAL) paint((pixel_t){2,1}, (pixel_t){22,78}, BG_CYAN | CHAR_BLACK);
		if (menu == MENU_SCHED) {
			paint((pixel_t){2,1}, (pixel_t){22,78}, BG_YELLOW | CHAR_BLACK);
			update_screen_switch(pid, pid);
			paint_task();
		} else if (menu == MENU_MMU) {
			paint((pixel_t){2,7}, (pixel_t){22,72}, BG_WHITE | CHAR_RED);
			common_mmu_screen();
			paint_pos(1);
		} else if (menu == MENU_REGS) {
			paint((pixel_t){2,1}, (pixel_t){22,78}, BG_GREEN | CHAR_BLACK);
		}
		paint(s, e, BG_BLACK | BRIGHT | CHAR_BLACK);
	}
}

void clock_task() {
	int i = 0;
	for (; i < NUMBER_OF_TASKS; i++) {
		if (task_waiting_time[i] > 0) {
			task_waiting_time[i] --;
			if (task_waiting_time[i] == 0) {
				unblock(i);
			}
		}
	}
}

void keyboard_task(unsigned char k) {
	int i = 0;
	for (; i < NUMBER_OF_TASKS; i++) {
		if (task_waiting_key[i] == k) {
			task_waiting_key[i] = 0;
			unblock(i);
		}
	}
}
