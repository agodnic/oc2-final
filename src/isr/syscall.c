#include "syscall.h"

#include "../cui/dic.h"		// Para int_to_string
#include "../mmu/mmu.h"		// Para malloc y free
#include "../term/term.h"	// Para send_message
#include "../sched/sched.h"	// Para end_task
#include "../fat/fat12.h"	// Para read_file
#include "../isr/isr.h"		// Para wait y wait_for_key (tasks)
#include "../screen/taskscrn.h"// Para print y paint _task_scrn

/*
RDI: 0
*/
unsigned long s_malloc(void* args[]) {
	unsigned long page = (unsigned long) malloc_page_u(0, get_current_pid(), OTHER);
	if (page == 0) return ERROR;
	else return page;
}

/*
RDI: 1	RSI: dir
*/
unsigned long s_free(void* args[]) {
	free_page_u((unsigned long) args[0], get_cr3(get_current_pid()));
	return OK;
}

/*
RDI: 2	RSI: *msg
*/
unsigned long s_send(void* args[]) {
	send_message(get_current_pid(),
	(void*) va2phy((unsigned long) args[0], get_cr3(get_current_pid())));
	return OK;
}

/*
RDI: 3	RSI: mode	RDX: X	RCX: Y	R8: W	R9: H
*/
unsigned long s_paint(void* args[]) {
	int c1 = (long) args[1];		int r1 = (long) args[2];
	int c2 = (long) args[3] + c1-1;	int r2 = (long) args[4] + r1-1;
	unsigned char mode = (long) args[0];
	paint_task_scrn((pixel_t) {r1, c1}, (pixel_t) {r2, c2}, mode);
	return OK;
}

/*
RDI: 4	RSI: *msg	RDX: X	RCX: Y	R8: mode
*/
unsigned long s_print(void* args[]) {
	int c = (long) args[1];		int r = (long) args[2];
	unsigned char mode = (long) args[3];
	print_task_scrn((pixel_t) {r,c}, args[0], mode);
	return OK;
}

/*
RDI: 5	RSI: time
*/
unsigned long s_wait(void* args[]) {
	unsigned int time = (long) args[0];
	task_wait(time);
	return OK;
}

/*
RDI: 6	RSI: scancode
*/
unsigned long s_wait_k(void* args[]) {
	unsigned char key = (long) args[0];
	task_wait_for_key(key);
	return OK;
}

/*
RDI: 7	RSI: int	RDX: *buffer RCX: max_chars
*/
unsigned long s_int_to_string(void* args[]) {
	unsigned long cr3 = get_cr3(get_current_pid());
	unsigned long buf = va2phy((unsigned long) args[1], cr3);
	int_to_string((long) args[0], (char*) buf, (long) args[2]);
	return OK;
}

/*
RDI: 8	RSI: *Path	RDX: *buffer RCX: num_bytes
*/
unsigned long s_read(void* args[]) {
	unsigned long cr3 = get_cr3(get_current_pid());
	char* filename = (char*) va2phy((unsigned long) args[0], cr3);
	unsigned long buf = va2phy((unsigned long) args[1], cr3);
	unsigned int num_bytes = (unsigned long) args[2];
	int result = read_file(filename, (void*) buf, num_bytes);
	if (result == 0) return ERROR;
	return result;
}

/*
RDI: 9
*/
unsigned long s_end(void* args[]) {
	end_task_sys();//never return from this call
	return OK;
}

/*
RDI: 10	RSI: *newName
*/
unsigned long s_name(void* args[]) {
	int pid = get_current_pid();
	char* new_name = (char*) va2phy((unsigned long) args[0], get_cr3(pid));
	name_task(pid, new_name);
	return OK;
}

/*
RDI: 11
*/
unsigned long s_pid(void* args[]) {
	return (unsigned long) get_current_pid();
}

/*
RDI: 12	RSI: *Path
*/
unsigned long s_run(void* args[]) {
	int pid = load_task((void*)
	va2phy((unsigned long) args[0], get_cr3(get_current_pid())));
	run_task(pid);
	return (unsigned long) pid;
}

/*
RDI: 13	RSI: *Path	RDX: X	RCX: Y	R8: W	R9: H
*/
unsigned long s_stamp(void* args[]) {
	stamp_task_scrn((long)args[1], (long)args[2], (long)args[3], (long)args[4],
	(void*) va2phy((unsigned long) args[0], get_cr3(get_current_pid())));
	return OK;
}

/*
RDI: 14
*/
unsigned long s_yield(void* args[]) {
	sched_block();
	return OK;
}

/*
RDI: 15	RSI: *Path	RDX: *buffer RCX: num_bytes
*/
unsigned long s_write(void* args[]) {
	unsigned long cr3 = get_cr3(get_current_pid());
	char* filename = (char*) va2phy((unsigned long) args[0], cr3);
	unsigned long buf = va2phy((unsigned long) args[1], cr3);
	unsigned int num_bytes = (unsigned long) args[2];
	write_file(filename, (void*) buf, num_bytes);
	return OK;
}

void* syscalls[] = {	/*0*/	s_malloc,
						/*1*/	s_free,
						/*2*/	s_send,
						/*3*/	s_paint,
						/*4*/	s_print,
						/*5*/	s_wait,
						/*6*/	s_wait_k,
						/*7*/	s_int_to_string,
						/*8*/	s_read,
						/*9*/	s_end,
						/*10*/	s_name,
						/*11*/	s_pid,
						/*12*/	s_run,
						/*13*/	s_stamp,
						/*14*/	s_yield,
						/*15*/	s_write
					};

void search_syscall(unsigned int id, void* args[]) {
	int pid = get_current_pid();
	if (id < N_SYSCALLS) {
		unsigned long (*syscall)(void*[]);
		syscall = syscalls[id];
		save_syscall_result(pid, syscall(args));
	} else save_syscall_result(pid, NO_SYSCALL);
}
