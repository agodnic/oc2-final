// isr - interrupt service routines
#ifndef __ISR_H__
#define __ISR_H__

extern void _isr0();
extern void _isr2();
extern void _isr3();
extern void _isr4();
extern void _isr5();
extern void _isr6();
extern void _isr7();
extern void _isr8();
extern void _isr10();
extern void _isr11();
extern void _isr12();
extern void _isr13();
extern void _isr14();
extern void _isr16();
extern void _isr17();
extern void _isr18();
extern void _isr19();
extern void _isr32();
extern void _isr33();
extern void _isr36();
extern void sched_block();

//shows interrupt screen
void isr_general(unsigned int interrupt);

//clock ISR
void clock_isr();

//keyboard ISR
void keyboard_isr();

//syscall ISR
void syscall_isr(unsigned int id, void* p1,
	void* p2, void* p3, void* p4, void* p5);

void set_menu(unsigned char m);
unsigned char get_menu();
unsigned char get_focus();

void init_blocking_sched();

void task_wait(unsigned int time);
void task_wait_for_key(unsigned char key);
void end_block(int pid);

#endif // __ISR_H__
