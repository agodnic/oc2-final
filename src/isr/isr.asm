BITS 64

global _isr0
global _isr2
global _isr3
global _isr4
global _isr5
global _isr6
global _isr7
global _isr8
global _isr10
global _isr11
global _isr12
global _isr13
global _isr14
global _isr16
global _isr17
global _isr18
global _isr19
global _isr32
global _isr33
global _isr36

extern clock_isr
extern keyboard_isr
extern syscall_isr
extern isr_general
extern is_waiting
extern menu
extern kernel_rsp
%include 'sched/sched.ha'
%include 'isr/context.ha'

_isr0:
		cli
		PUSH_CONTEXT_INT
		mov		Rdi, 0
		jmp	error
_isr2:
		cli
		PUSH_CONTEXT_INT
		mov		Rdi, 2
		jmp	error
_isr3:
		cli
		PUSH_CONTEXT_INT
		mov		Rdi, 3
		jmp	error
_isr4:
		cli
		PUSH_CONTEXT_INT
		mov		Rdi, 4
		jmp	error
_isr5:
		cli
		PUSH_CONTEXT_INT
		mov		Rdi, 5
		jmp	error
_isr6:
		cli
		PUSH_CONTEXT_INT
		mov		Rdi, 6
		jmp	error
_isr7:
		cli
		PUSH_CONTEXT_INT
		mov		Rdi, 7
		jmp	error
_isr8:
		cli
		PUSH_CONTEXT_INT
		mov		Rdi, 8
		jmp	error
_isr10:
		cli
		PUSH_CONTEXT_INT
		mov		Rdi, 10
		jmp	error
_isr11:
		cli
		PUSH_CONTEXT_INT
		mov		Rdi, 11
		jmp	error
_isr12:
		cli
		PUSH_CONTEXT_INT
		mov		Rdi, 12
		jmp	error
_isr13:
		cli
		PUSH_CONTEXT_INT
		mov		Rdi, 13
		jmp error
_isr14:
		cli
		PUSH_CONTEXT_INT
		mov		Rdi, 14
		jmp	error
_isr16:
		cli
		PUSH_CONTEXT_INT
		mov		Rdi, 16
		jmp	error
_isr17:
		cli
		PUSH_CONTEXT_INT
		mov		Rdi, 17
		jmp	error
_isr18:
		cli
		PUSH_CONTEXT_INT
		mov		Rdi, 18
		jmp	error
_isr19:
		cli
		PUSH_CONTEXT_INT
		mov		Rdi, 19
		jmp	error

_isr32:
		cli
		PUSH_CONTEXT_INT
		extern fin_intr_pic1
		call fin_intr_pic1

		call	clock_isr

		cmp byte [menu], 0
		je skip_sched
		cmp byte [interrupted], 1
		je skip_sched
		cmp byte [interrupted_k], 1
		je skip_sched
		call is_waiting
		cmp rax, 0
		jne skip_sched

		call	sched_clock
	skip_sched:

		POP_REGISTERS
		iretq

_isr33:
		cli
		PUSH_CONTEXT_INT
		extern fin_intr_pic1
		call fin_intr_pic1

		cmp byte [menu], 0
		je skip_key
		cmp byte [interrupted], 1
		je skip_key
		cmp byte [interrupted_k], 1
		je skip_key
		call is_waiting
		cmp rax, 0
		jne skip_key

		call	set_int_k
		call	keyboard_isr
		call	clr_int_k

	skip_key:
		POP_REGISTERS
		iretq

_isr36:
		cli
		PUSH_CONTEXT_SYSCALL

		call	syscall_isr

		POP_REGISTERS
		iretq

error:
		call	set_int
		call	isr_general
		call	clr_int

		cmp byte [command_int], 1
		je dont_kill

		extern end_task_sys
		call end_task_sys

	dont_kill:
		mov byte [command_int], 0
		POP_REGISTERS
		iretq

global command_int
command_int: DB 0	; boolean: true if the user made an interrupt

interrupted: DB 0	; boolean: true if the kernel has been interrupted

global set_int
set_int:
		mov byte [interrupted], 1
		ret

global clr_int
clr_int:
		mov byte [interrupted], 0
		ret

interrupted_k: DB 0	; boolean: true if the kernel has been
					; interrupted by the keyboard

global set_int_k
set_int_k:
		
		ret

global clr_int_k
clr_int_k:
		mov byte [interrupted_k], 0
		ret
