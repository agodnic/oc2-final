#ifndef __SYS_H__
#define __SYS_H__

/**
	Para llamar a una Syscall:
	En isr.asm se define la RAI de syscall llamando a una función en C.
	Por lo tanto, para conservar el identificador y los parámetros de la
	Syscall, se pasarán como parámetros de la función C.
		RDI: id de Syscall
		RSI: param 1
		RDX: param 2
		RCX: param 3
		R8:  param 4
		R9:  param 5
**/

void search_syscall(unsigned int id, void* args[]);


#endif // __SYS_H__
