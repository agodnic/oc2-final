#include "fat12.h"
#include "../hw/ia32e.h"
#include "../mmu/mmu.h"
#include "../ansi/string.h"
#include "../constants.h"

//unused cluster
#define FAT12_UNUSED	0x000

typedef struct fat12_boot_sector{
	unsigned char	ignored[11];
	unsigned short	bytes_per_sector;
	unsigned char	sectors_per_cluster; 
	unsigned short  number_of_reserved_sectors;
	unsigned char	number_of_FATs;
	unsigned short	max_num_of_root_entry_dirs;
	unsigned short	total_sector_count; //unused //count of all sectors in volume
	unsigned char	ignored2; //unused
	unsigned short	sectors_per_FAT;
	unsigned short	sectors_per_track; //unused
	unsigned short	number_of_heads; //unused
	unsigned char	ignored3[10]; //unused
	unsigned char	boot_signature; //unused
	unsigned int	volume_id; //unused
	unsigned char	volume_label[11]; //unused
	unsigned char	fs_type[8]; //unused
} __attribute__((packed)) fat12_boot_sector_t;

typedef struct fat12_dir_entry {
	char filename[8];
	char extension[3];
	unsigned char attributes;
	unsigned char reserved[2];
	unsigned char creation_time[2];
	unsigned char creation_date[2];
	unsigned char last_access_date[2];
	unsigned char ignored[2];
	unsigned char last_write_time[2];
	unsigned char last_write_date[2];
	unsigned short first_logical_cluster;	//index in fat12 table
	unsigned int file_size;	//size in bytes
} fat12_dir_entry_t;

fat12_dir_entry_t* root_dir_next_entry();
fat12_dir_entry_t* root_dir_first_entry();

fat12_dir_entry_t* find_root_entry(char* name);

//this will point to a fat table loaded in 1 page of memory
static void* fat;
//this will point to the boot_sector info. in 1 page of memory
static fat12_boot_sector_t* boot_sector;
//cache for reading files
fat12_dir_entry_t* dir_cache;

static unsigned int root_dir_start_block;
static unsigned int root_dir_block_size;
static unsigned int data_area_start_block;

static void load_fat12_table(lba28_addr_t* src_block, void* dst_addr){
 //read fat hard disk addr
 read_block(dst_addr, src_block, 8);  //fat size in blocks = 8
}

//dumps a fat12 table to all hard disk fat tables
static void dump_fat12_table(void* src_addr){
 lba28_addr_t fat_addr = {boot_sector->number_of_reserved_sectors};
 write_block(src_addr, &fat_addr, 8);
 //TODO write second fat
}

/* initializes fat12 filesystem driver */
void fat12_init(){

	//initialize hard disk driver
	hd_init();

	dir_cache = (fat12_dir_entry_t*) malloc_page_k(NULL, KERN, HD);

	boot_sector->sectors_per_cluster = 8;
	lba28_addr_t first_block = {0};

	boot_sector = (fat12_boot_sector_t*) malloc_page_k(NULL, KERN, HD);
	read_block( (void*) boot_sector, &first_block, 1 );

	root_dir_start_block = boot_sector->number_of_reserved_sectors +
		(boot_sector->number_of_FATs * boot_sector->sectors_per_FAT);

	root_dir_block_size = boot_sector->max_num_of_root_entry_dirs *
		sizeof(fat12_dir_entry_t) / 
		boot_sector->bytes_per_sector;

	data_area_start_block = root_dir_start_block + root_dir_block_size;

	//fat table on disk is exactly one page frame long
	fat = (void*) malloc_page_k(NULL, KERN, HD);

	lba28_addr_t start_block = {boot_sector->number_of_reserved_sectors};
	load_fat12_table(&start_block, fat);

}


void read_cluster(void* buf, lba28_addr_t* addr){
 read_block(buf, addr, boot_sector->sectors_per_cluster);
}

void write_cluster(void* buf, lba28_addr_t* addr){
 write_block(buf, addr, boot_sector->sectors_per_cluster);
}

///////////////////////////////////////////////////////////////////////////////


fat12_dir_entry_t* find_root_entry(char* name){

	fat12_dir_entry_t* pd = root_dir_first_entry();

	while(pd && strncmp(name, pd->filename, 11))
		pd = root_dir_next_entry();

	return pd;
}


static int dir_index;

#define OUT_OF_RANGE (dir_index*sizeof(fat12_dir_entry_t) >= root_dir_block_size*512)

#define VALID_DIRENT(DE) (				\
		 DE.filename[0] != DIR_UNUSED   &&	\
		 DE.filename[0] != DIR_EMPTY    &&	\
		!(DE.attributes &  DIR_VLABEL)  &&	\
		!(DE.attributes &  DIR_LONGNAME)	\
	)

/*FIXME duplicated code, 
 this functions shall be replaced using find_dir_cluster
*/
fat12_dir_entry_t* root_dir_next_entry(){

	if(OUT_OF_RANGE) return NULL;
	
	while( !OUT_OF_RANGE &&
			!VALID_DIRENT(dir_cache[dir_index]))
		dir_index++;

	if(OUT_OF_RANGE)	return NULL;
	else			return &dir_cache[dir_index++];
}

fat12_dir_entry_t* root_dir_first_entry(){

	//refresh dir cache
	lba28_addr_t root = {root_dir_start_block};
	read_block(dir_cache, &root, 8); //0x1000 bytes of cache
	
	dir_index = 0;
	return root_dir_next_entry();
}

///////////////////////////////////////////////////////////////////////////////


//eof cluster range
#define FAT12_LAST_L	0xff8
#define FAT12_LAST_H	0xfff
#define FAT12_EOF(N)	((N >= FAT12_LAST_L) && (N <= FAT12_LAST_H))

#define EVEN(N) ((N % 2) == 0)

//TODO add some data structure to handle this?
static unsigned short read_fat12_entry(n){

	unsigned char* table = fat;
	unsigned short low = 0, high = 0;
	unsigned short result;

	if (EVEN(n)){
		low  = table[    ((3*n)/2) ]       ;
		high = table[1 + ((3*n)/2) ] & 0x0f;

		result = low | (high << 8)         ;
	}else{
		low  = table[    ((3*n)/2) ] & 0xf0;
		high = table[1 + ((3*n)/2) ]       ;

		result = low | (high << 4)         ;
	}
	return result;
}


//FIXME duplicated code on write/read versions
static void write_fat12_entry(unsigned short n, unsigned short value){
	unsigned char* table = fat;

	if (EVEN(n)){
		table[    ((3*n)/2) ]  = value & 0xff		;
		table[1 + ((3*n)/2) ] |= (value & 0x0f00) >> 8	;
	}else{
		table[    ((3*n)/2) ] |= (value & 0x000f) << 4	;
		table[1 + ((3*n)/2) ]  = (value & 0x0ff0) >> 4	;
	}
}

static fat12_dir_entry_t* search_dir_cluster(fat12_dir_entry_t* dir_cluster, char* fat_name){
	int i;
	
	for(i = 0; i*sizeof(fat12_dir_entry_t) < 0x1000; i++)

		if( !strncmp(dir_cluster[i].filename, fat_name, 11) && 
			VALID_DIRENT(dir_cluster[i]))

			return &dir_cluster[i];

	return NULL;
}


//translate between physical and logical sector nums
#define PHY_ADDR(N) data_area_start_block + ((N-2)*boot_sector->sectors_per_cluster)

static fat12_dir_entry_t* find_dir_entry(fat12_dir_entry_t* pfather, char* fat_name){

	fat12_dir_entry_t* result;
	unsigned short cluster_number = pfather->first_logical_cluster;

	for(;;) {

		lba28_addr_t dir_addr = {PHY_ADDR(cluster_number)};
		read_cluster(dir_cache, &dir_addr);

		result = search_dir_cluster( (void*) dir_cache, fat_name);

		if(result)
			break;

		if( FAT12_EOF(read_fat12_entry(cluster_number)) )
			break;
		else
			cluster_number = read_fat12_entry(cluster_number);
		
	}
	return result;
}


///////////////////////////////////////////////////////////////////////////////


/*
	converts src into fat12 format and stores it in dst
	returns a pointer to the last processed byte plus one
		(a slash or a null byte)
*/
//TODO check how fat handles extensions less than 3 bytes long
static char* ansi2fat(char* dst, char* src){

	unsigned char count = 0;
	memset( (void*) dst, ' ', 11);

	while(*src != 0 && *src != 10 && *src != '/'){
		if(*src == '.') //pad filename[8] with spaces
			while(count < 8)
				dst[count++] = ' ';
		else
			dst[count++] = *src;

		src++;
	}

	return src; //src should point to a slash or a null byte
}


static char  parse_string_buffer[256];
static char  parse_result_buffer[11];
static char* parse_current_token;

/*
ASSERTS
	curr points to a slash or a null byte

RETURN VALUE
	next name, in fat12 naming convention
	null if no more names
*/
static char* parse_next(){

	if(*parse_current_token == 0) return NULL;
	if(*parse_current_token == 10) return NULL;

	parse_current_token++;
	
	parse_current_token = 
		ansi2fat(parse_result_buffer, parse_current_token);

	return parse_result_buffer;
}


static char* parse_start(char* s){
	strcpy(parse_string_buffer, s);
	parse_current_token = parse_string_buffer;

	return parse_next();
}


///////////////////////////////////////////////////////////////////////////////


//returns dirent associated to file or NULL if not found
static fat12_dir_entry_t* file_dirent(char* filename){

	char* name = parse_start(filename);
	fat12_dir_entry_t* dirent = find_root_entry(name);

	while( (name = parse_next()) )
		dirent = find_dir_entry(dirent, name);

	return dirent;
}


int read_file(char* filename, void* buf, unsigned int num_bytes){

	fat12_dir_entry_t* f = file_dirent(filename);

	if (f==0) return 0;

	lba28_addr_t dir_addr = {
		PHY_ADDR(f->first_logical_cluster)
	};

	read_block(buf, &dir_addr, boot_sector->sectors_per_cluster);

	return f->file_size;
}


///////////////////////////////// WRITE  //////////////////////////////////////

//returns cluster for parent directory entries
//returns 0 on error
unsigned int parent_dir_entries_cluster(char* path_to_file){
 char* name = parse_start(path_to_file);
 fat12_dir_entry_t* dirent = find_root_entry(name);
 unsigned int prev_start_cluster=root_dir_start_block;

 while( (name = parse_next()) )
  prev_start_cluster=PHY_ADDR(dirent->first_logical_cluster), dirent = find_dir_entry(dirent, name);

 return prev_start_cluster;
}

//give a pointer to file name
char* get_name(char* path_to_file){
 char* current = parse_start(path_to_file),* previous;
 
 while( previous=current, current=parse_next() )
  ;

 return previous;
}

//initialize a fat12 directory entry to zero size and zero start cluster
void init_empty_dirent( fat12_dir_entry_t* dirent, char* path_to_file){
 memset(dirent, 0, sizeof(fat12_dir_entry_t));
 char* file_name = get_name(path_to_file);
 memcpy( (void*) dirent, file_name, 11); 
}


fat12_dir_entry_t* find_free_entry(fat12_dir_entry_t* dir_entries){
 int i=0;
 do{
  if( dir_entries[i].filename[0] == DIR_UNUSED || dir_entries[i].filename[0] == DIR_EMPTY )
   return &dir_entries[i];  

  i++;
 }while(i < 0x1000/sizeof(fat12_dir_entry_t));

 return NULL; //no free dir entry found
}

//Adds <new_entry> as a new directory entry for <parent>
void add_dir_entry(
 unsigned int cluster, fat12_dir_entry_t* new_entry){

 //read dir info
 void* dir_contents = (void*) malloc_page_k(0, 0, 0);
 lba28_addr_t dir_addr = { cluster };
 read_cluster(dir_contents, &dir_addr);

 //find a free directory entry
 fat12_dir_entry_t* free_entry = find_free_entry( (void*) dir_contents);

 //write new_entry there
 memcpy(free_entry, new_entry, sizeof(fat12_dir_entry_t) ); //OK

 //write back the modified cluster
 write_cluster(dir_contents, &dir_addr);

 free_page_k((unsigned long) dir_contents);
}

//truncate a file to zero size
void truncate_file(char* path_to_file){

 //find dirent for path_to_file
 unsigned int cluster = parent_dir_entries_cluster(path_to_file);
 void* parent_dir_entries = (void*) malloc_page_k(0, KERN, HD);
 lba28_addr_t addr = {cluster};
 read_cluster(parent_dir_entries, &addr);
 fat12_dir_entry_t* entry = search_dir_cluster(parent_dir_entries, get_name(path_to_file) );

 //free dirent cluster (go to fat table and set as free)
 if(entry->first_logical_cluster){
  write_fat12_entry(entry->first_logical_cluster, FAT12_UNUSED); //0x000

  //update fat12 table to free those clusters (dump fat to disk)
  dump_fat12_table(fat);
 } 

 //set dirent as empty an write back
 entry->file_size = entry->first_logical_cluster = 0;
 write_cluster(parent_dir_entries, &addr);
 free_page_k((unsigned long) parent_dir_entries);
}

#define ERROR_NO_PARENT -1
#define SUCCESS 0
//Create a file, truncate it if already exists.
//Returns 0 if OK
int create(char* path_to_file){

 fat12_dir_entry_t* file;
 if ( (file= file_dirent(path_to_file)) == NULL){ //if file does not exist...
  unsigned int cluster;
  if ( (cluster = parent_dir_entries_cluster(path_to_file)) == 0)
   return ERROR_NO_PARENT; //fail if parent doesn't exist

  //Create path_to_file dirent for parent file
  fat12_dir_entry_t empty_dir_entry;
  init_empty_dirent( &empty_dir_entry, path_to_file ); //OK hasta aca
  add_dir_entry(cluster, &empty_dir_entry);
 }

 //truncate file
 truncate_file(path_to_file);


 return SUCCESS;
}


//FIXME hardcoded :(
#define FAT_ENTRIES 400

//malloc for fat12 clusters
//returns LOGICAL addresses! Not PHYSYCAL!!!
unsigned int allocate_cluster(){

 //find a free cluster on fat
 int i=2;
 while(i<FAT_ENTRIES){
  if(read_fat12_entry(i) == FAT12_UNUSED) break;
  i++;
 }

 //mark it as used
 write_fat12_entry(i, FAT12_LAST_H);

 //update fat
 dump_fat12_table(fat);

 return i;
}

//write a cluster to file. Opens in truncate mode.
int write_file(char* path_to_file, void* buf, unsigned int num_bytes){
 create(path_to_file); //open in truncate mode

 int new_cluster_number = allocate_cluster();

 //get parent dirent cluster
 unsigned int cluster = parent_dir_entries_cluster(path_to_file);
 void* parent_dir_entries = (void*) malloc_page_k(0, KERN, HD);
 lba28_addr_t addr = {cluster};
 read_cluster(parent_dir_entries, &addr);
 //find dirent for path_to_file
 fat12_dir_entry_t* entry = search_dir_cluster(parent_dir_entries, get_name(path_to_file) );
 //edit new file len and new start cluster 
 entry->first_logical_cluster = new_cluster_number;
 entry->file_size = num_bytes;
 //write back parent dirent cluster
 write_cluster(parent_dir_entries, &addr); //OK ABOVE

 //write new start cluster with buffer contents
 lba28_addr_t new_cluster_addr = { PHY_ADDR(new_cluster_number) };
 write_cluster(buf, &new_cluster_addr);
 free_page_k((unsigned long) parent_dir_entries);
 return num_bytes;
}


