#ifndef __HD_DRIVER_H__
#define __HD_DRIVER_H__

//driver for hd LBA access via PIO

typedef struct lba_bits {
	unsigned char b0_7;
	unsigned char b8_15;
	unsigned char b16_23;
	unsigned char b24_27:4;
} __attribute__((__packed__)) lba_bits_t;


typedef union lba28_addr {
	unsigned int full_addr;
	lba_bits_t   bits;
} lba28_addr_t;

//TODO add disk parameter
void hd_init();
void  read_block(void* buf, lba28_addr_t* addr, unsigned char sector_count);
void write_block(void* buf, lba28_addr_t* addr, unsigned char sector_count);

#endif
