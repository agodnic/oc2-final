#include "driver.h"
#include "../../hw/ia32e.h"
#include "../../constants.h"

void hd_init(){
	//disable hd interrupts
	outb(0x3f6, inb(0x3f6) | 0x02);
}

//TODO add disk parameter
//TODO add slave/master param
void read_block(void* buf, lba28_addr_t* addr, unsigned char sector_count){

	//protocol stuff
	outb(0x1f1, 0x00);

	//sector count
	outb(0x1f2, sector_count);

	//LBA[0..24)
	outb(0x1f3, addr->bits.b0_7  );
	outb(0x1f4, addr->bits.b8_15 );
	outb(0x1f5, addr->bits.b16_23);

	/*bit 4 is drive bit. all higher bits are magic.
		bits [0..3] are LBA[24..27]	*/
	outb(0x1f6, 0xe0 | addr->bits.b24_27);

	//operation type
	outb(0x1f7, READ_SECTORS);	

	while (!(inb(0x1F7) & 0x08)) {/*Polling*/} 

	//read disk data to buffer
	unsigned short* recv_buf = (unsigned short*) buf;
	int idx;

	while(sector_count--) {
		for (idx = 0; idx < 256; idx++)
			*(recv_buf++) = inw(0x1F0);
	}
}

void write_block(void* buf, lba28_addr_t* addr, unsigned char sector_count){

	//protocol stuff
	outb(0x1f1, 0x00);

	//sector count
	outb(0x1f2, sector_count);

	//LBA[0..24)
	outb(0x1f3, addr->bits.b0_7  );
	outb(0x1f4, addr->bits.b8_15 );
	outb(0x1f5, addr->bits.b16_23);

	/*bit 4 is drive bit. all higher bits are magic.
		bits [0..3] are LBA[24..27]	*/
	outb(0x1f6, 0xe0 | addr->bits.b24_27);

	//operation type
	outb(0x1f7, WRITE_SECTORS);	

	while (!(inb(0x1F7) & 0x08)) {/*Polling*/} 

	//write buffer data to disk
	unsigned short* out_buf = (unsigned short*) buf;
	int idx;
	while(sector_count--) {
		for (idx = 0; idx < 256; idx++)
			outw(0x1F0, *(out_buf++) );
	}
}
