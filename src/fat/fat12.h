#ifndef __FAT_12__H__
#define __FAT_12__H__
#include "hd/driver.h"

/* initialize our fat12 driver */
void fat12_init();

//retuns 0 if filename does not exist. In the other case, the file size
int read_file(char* filename, void* buf, unsigned int num_bytes);

//write a cluster to file. Opens in truncate mode.
//TODO parameter num_bytes is ignored atm. Reads full file instead
int write_file(char* path_to_file, void* buf, unsigned int num_bytes);

#endif
