;
;	kernel startup:
;	quickly switch to ia32e mode
;	then we are ready to switch to C code
;

KERNEL_STACK		equ		0x21000			;RSP

BITS 16
global start_real_mode
start_real_mode:
        cli

	;load gdt
	lgdt	[cs:gdt_register]

	;enable a20 gate
	mov		al, 0xdf
	call	gate_a20
	cmp	al, 0
	;TODO jne	a20_error

	;switch to protected mode
	mov     eax, cr0
	or      eax, 1
	mov     cr0, eax
	jmp     CODE_IA32_SELECTOR:start_protected_mode

BITS 32
start_protected_mode:

	;set segment selectors
        xor     eax, eax
        mov     ax, DATA_IA32_SELECTOR
        mov     ds, ax
        mov     es, ax
        mov     fs, ax
        mov     gs, ax
        mov     ss, ax

	;set stack on page frame [0x20000,0x21000)
        mov     esp, KERNEL_STACK
        xor     ebp, ebp


	;Now prepare for enabling ia32e mode

	;create paging data structures
        call    ia32e_paging_init

	;enable PAE (Physical Adress Extensions) to increase adressable space
        mov     eax, cr4
        or      eax, 1 << 5             ;CR4.PAE = 1
        mov     cr4, eax                ;activar PAE

	;set IA32_EFER.LME
        mov     ecx,0C0000080h          ;EFER MSR (Model Specific Register)
        rdmsr
        or      eax,1 << 8              ;LME (Long Mode Enabled)
        wrmsr

	;enable ia32e paging
        mov     eax, cr0
        or      eax, 0x80000000
        mov     cr0, eax

	;switch to ia32e mode :)
        jmp     CODE_IA32E_SELECTOR:start_ia32e_mode

BITS 64
start_ia32e_mode:

	;load idt
	;we don't save vamd64 abi registers because we don't need them
	call	initialize_idt			;set isr descriptors
	lidt	[idt_register]			;set IDTR register

	;OS main routine in C
	extern	kernel_main
	call	kernel_main

	;run idle, flow control should never return from here
	extern	idle_start
	call	idle_start
	jmp $

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

%include 'hw/a20.inc'
%include 'gdt/gdt.ha'
%include 'idt/idt.ha'
%include 'mmu/mmu.ha'
%include 'isr/isr.asm'
%include 'screen/screen.asm'
