#ifndef __CONSTANTS_H__
#define __CONSTANTS_H__

//PID
//Tasks: 0 - 16
	#define NO_PID -1
	#define KERN -2
	#define USR -3

	#define NUMBER_OF_TASKS 16
	#define VIRTUAL_TASK_CODE 0x400000

//MENU
	#define MENU_NULL		0
	#define MENU_TERMINAL	1
	#define MENU_SCHED		2
	#define MENU_BROWSE		3
	#define MENU_REGS		4
	#define MENU_MMU		5

//Memory Map
	#define KERNEL_CR3		0x21000
	#define KERNEL 0
	#define USER 1
	//Page frame with index 512
	#define KERN_PAGES_BASE	0x200000
	//Page frame with index 512
	#define USER_PAGES_BASE 0x400000

//Page Type
	#define NO_TYPE 0x74//Rojo
	#define HD 0x71		//Azul
	#define SCRN 0x72	//Verde
	#define TERM 0x73	//Cyan
	#define PAGE 0x75	//Purpura
	#define STACK 0x76	//Naranja
	#define OTHER 0x70	//Negro

	#define NULL 0

//Page Attributes
	#define PAGE_PRESENT		0x1
	#define PAGE_READWRITE		0x2

	#define NUMBER_OF_PAGES	512
	#define PAGE_SIZE		0x1000

//Task State
	#define EMPTY 0
	#define LOADED 1
	#define RUNNING 2
	#define BLOCKED 3

	#define DEFAULT_QUANTUM 20

//SCREEN
	#define SCREEN_BUFFER_BASE	0xB8000

	#define SCREEN_COLUMN_COUNT	80
	#define SCREEN_ROW_COUNT	25

//Color
	#define CHAR_BLACK  0x0
	#define CHAR_BLUE   0x1
	#define CHAR_GREEN  0x2
	#define CHAR_CYAN   0x3
	#define CHAR_RED    0x4
	#define CHAR_PURPLE 0x5
	#define CHAR_YELLOW 0x6
	#define CHAR_WHITE  0x7
	#define BRIGHT 		0x8

	#define BG_BLACK	0x00
	#define BG_BLUE		0x10
	#define BG_GREEN	0x20
	#define BG_CYAN		0x30
	#define BG_RED		0x40
	#define BG_PURPLE	0x50
	#define BG_YELLOW	0x60
	#define BG_WHITE	0x70
	#define BLINK		0x80

//Task Screen
	#define TASK_SCRN_START 0xB8000+SCREEN_COLUMN_COUNT*2

//Terminal
	#define LINES_VIEW 21			//amount of lines view at the same time (25 - 3)
	#define LINE_WIDTH 78			//max width of a line (80 - 2)

	#define K_COMMANDS 25

	#define NO_COMMAND	2

//Syscalls
	#define N_SYSCALLS 16

	#define OK			0
	#define ERROR		-1
	#define NO_SYSCALL	-2

//F . A . T
	/* values for .filename[0] */
	#define DIR_UNUSED	0xe5
	#define DIR_EMPTY	0x00

	/* values for .attributes */
	#define DIR_READONLY	0x01
	#define DIR_HIDDEN		0x02
	#define DIR_SYSTEM		0x04
	#define DIR_VLABEL		0x08
	#define DIR_SUBDIR		0x10
	#define DIR_ARCHIVE		0x20
	#define DIR_LONGNAME	0x0f


//HD DRIVER
	#define READ_SECTORS  0x20
	#define WRITE_SECTORS 0x30

//KEYBOARD
	#define KEY_ESC			0x01
	#define KEY_SPACE		0x39
	#define KEY_ENTER		0x1C
	#define KEY_BACKSPACE	0x0E
	#define KEY_TAB			0x0F

	#define KEY_L_SHIFT		0x2A
	#define KEY_R_SHIFT		0x36
	#define KEY_CTRL		0x37
	#define KEY_CAPS		0x3A
	#define KEY_ALT			0x38

	#define BREAK_L_SHIFT	0xAA
	#define BREAK_R_SHIFT	0xB6
	#define BREAK_CAPS		0xBA
	#define KEY_F4			0x3E

	#define KEY_UP			0x48
	#define KEY_DOWN		0x50
	#define KEY_LEFT		0x4B
	#define KEY_RIGHT		0x4D

	#define KEY_INSERT		0x52
	#define KEY_DELETE		0x53
	#define KEY_HOME		0x47
	#define KEY_END			0x4F
	#define KEY_PGUP		0x49
	#define KEY_PGDN		0x51

	#define KEY_1			0x02
	#define KEY_2			0x03
	#define KEY_3			0x04
	#define KEY_4			0x05
	#define KEY_5			0x06
	#define KEY_6			0x07
	#define KEY_7			0x08
	#define KEY_8			0x09
	#define KEY_9			0x0A
	#define KEY_0			0x0B

	#define KEY_A			0x1E
	#define KEY_B			0x30
	#define KEY_C			0x2E
	#define KEY_D			0x20
	#define KEY_E			0x12
	#define KEY_F			0x21
	#define KEY_G			0x22
	#define KEY_H			0x23
	#define KEY_I			0x17
	#define KEY_J			0x24
	#define KEY_K			0x25
	#define KEY_L			0x26
	#define KEY_M			0x32
	#define KEY_N			0x31
	#define KEY_O			0x18
	#define KEY_P			0x19
	#define KEY_Q			0x10
	#define KEY_R			0x13
	#define KEY_S			0x1F
	#define KEY_T			0x14
	#define KEY_U			0x16
	#define KEY_V			0x2F
	#define KEY_W			0x11
	#define KEY_X			0x2D
	#define KEY_Y			0x15
	#define KEY_Z			0x2C

	#define KEY_PLUS		0x4E
	#define KEY_MINUS		0x4A

#endif
