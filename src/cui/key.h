// keyboard primitives
#ifndef __KEY_H__
#define __KEY_H__

//returns an icon for the task 'pid'
char icon(int pid);

//returns true if the key 'k' is a char key
char char_key(unsigned char k);

//this function stops the CPU until a key is pressed on the keyboard
void wait_for_key(unsigned char k);

//returns the last pressed key
unsigned char pressed_key();

//returns the char of the key 'k'
char key_to_char(unsigned char k);

//active capitals; although is an input function is necesary for 'key_to_char'
void input_shift();

//deactive capitals; although is an input function is necesary for 'key_to_char'
void break_shift();

//toggles capitals; although is an input function is necesary for 'key_to_char'
void input_caps();

//returns the char of the next pressed key
char getc();

//writes a string on 'dir' based on the next pressed keys until enter is pressed
//returns the amount of chars written including a '\n' for the enter
unsigned int gets(char* dir);

#endif // __KEY_H__
