#include "clock.h"

#include "key.h"
#include "../hw/ia32e.h"
#include "../constants.h"

unsigned char wait_counter = 0;

char is_waiting() {return wait_counter>0;}
void wait_clock() {if (wait_counter>0) wait_counter--;}
void wait_end() {wait_counter = 0;}

void wait(int t){
	if ((pressed_key() != KEY_SPACE) && (t>0)) {
		wait_counter = t;
		while ((pressed_key() != KEY_SPACE) && (wait_counter > 0)){
			/*polling*/
			sti();
		}
		cli();
	}
}
