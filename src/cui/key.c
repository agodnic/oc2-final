#include "key.h"

#include "../cui/cui.h"
#include "../mmu/mmu.h"
#include "../hw/ia32e.h"
#include "../constants.h"

char is_shift_pressed = 0;
char is_bloq_mayus = 1;

//Each char is in the position if its scancode ¿A8 ¡AD ñA4 ÑA5
char char_key_array[] = {0,								// 0	(0x00)
 0,'1','2','3','4','5','6','7','8','9','0','-','+',0,	// 1-14 (0x01 - 0x0E)
  0,'q','w','e','r','t','y','u','i','o','p','{','}',0,	//15-28 (0x0F - 0x1C)
   0,'a','s','d','f','g','h','j','k','l',';','\'','`',	//29-41 (0x1D - 0x29)
 0,'|','z','x','c','v','b','n','m',',','.','/',0,		//42-54 (0x2A - 0x36)
	0,0,' '};											//55-57 (0x37 - 0x39)
char char_KEY_array[] = {0,								// 0	(0x00)
 0,'!','@','#','$','%','^','&','*','(',')','_','=',0,	// 1-14 (0x01 - 0x0E)
  0,'Q','W','E','R','T','Y','U','I','O','P','[',']',0,	//15-28 (0x0F - 0x1C)
   0,'A','S','D','F','G','H','J','K','L',':','\"','~',	//29-41 (0x1D - 0x29)
 0,0xA7,'Z','X','C','V','B','N','M','<','>','/',0,		//42-54 (0x2A - 0x36)
	0,0,' '};											//55-57 (0x37 - 0x39)

char icon(int pid) {
	if (pid == NO_PID) {
		return 0xB0;
	} else if (pid == KERN) {
		return 2;
	} else if (pid == USR) {
		return 1;
	}
	else return pid + 97;
}

//returns true if the key 'k' is a char key
char char_key(unsigned char k){
	return
		(	((k >= 0x02) && (k <= 0x0D)) // keys 1 - ¿
		||	((k >= 0x10) && (k <= 0x1B)) // keys q - +
		||	((k >= 0x1E) && (k <= 0x29)) // keys a - }
		||	((k >= 0x2B) && (k <= 0x35)) // keys < - -
		||	(k == 0x39)					 // key space
		); 
}

//returns true if the key 'k' is a letter key
char letter_key(unsigned char k){
	return
		(	((k >= 0x10) && (k <= 0x19)) // keys q - p
		||	((k >= 0x1E) && (k <= 0x26)) // keys a - l
		||	((k >= 0x2C) && (k <= 0x32)) // keys z - m
		||	(k == 0x39)					 // ñ
		); 
}

void wait_for_key(unsigned char k) {
	while(inb(0x60) != k) {/*polling*/}
	k+=0x80;
	while(inb(0x60) != k) {/*polling*/}
}

void wait_for_release(unsigned char b) {
	while(inb(0x60) != b) {/*polling*/}
}

unsigned char pressed_key() {
	return inb(0x60);
}

char key_to_char(unsigned char k) {
	char result = 0;
	if (letter_key(k)) {
		if (is_shift_pressed + is_bloq_mayus == 1) {
			result = char_KEY_array[(int) k];
		} else {
			result = char_key_array[(int) k];
		}
	}
	else if (char_key(k)) {
		if (is_shift_pressed)
			result = char_KEY_array[(int) k];
		else
			result = char_key_array[(int) k];
	}
	return result;
}

void input_shift() {
	is_shift_pressed = 1;
	set_mode((pixel_t) {24,76}, BG_RED | BRIGHT | CHAR_YELLOW);
}

void break_shift() {
	is_shift_pressed = 0;
	set_mode((pixel_t) {24,76}, BG_RED | BRIGHT | CHAR_BLACK);
}

void input_caps() {
	is_bloq_mayus = !is_bloq_mayus;
	set_char((pixel_t) {24,77},(is_bloq_mayus ? 'M' : 'm'));
	wait_for_release(BREAK_CAPS);
}

char getc() {
	char key;
	while ( (pressed_key() >= 0x80) ||
			( !char_key(pressed_key()) ) ) { /**polling**/ }
	key = pressed_key();
	return key_to_char(key);
}

unsigned int gets(char* dir) {
	unsigned int result = 0;
	char current = 0;
	char key;
	while (current != '\n') {
		while (
					(pressed_key() >= 0x80)
					||
					(	(	!char_key(pressed_key())	) &&
						(	pressed_key() != KEY_ENTER	)
					)
				) { /**polling**/ }
		key = pressed_key();
		if (key == KEY_ENTER)
			current = '\n';
		else
			current = key_to_char(key);
		dir[result] = current;
		result++;
		while (pressed_key() > 0) { /*polling*/ }
	}
	return result;
}
