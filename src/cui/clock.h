// clock primitives
#ifndef __CLOCK_H__
#define __CLOCK_H__

//returns true if the kernel is waiting the clock
char is_waiting();

//clock interrupted so waiting is 1 less
void wait_clock();

//space pressed so waiting is 0
void wait_end();

//set a waiting time
void wait(int t);

#endif // __CLOCK_H__
