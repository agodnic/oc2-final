#ifndef __DIC_H__
#define __DIC_H__

int ascii_to_int(char a);

int ascii16_to_int(char a);

int string_to_int(char* buf);

//writes the ascii chars for the int i on the buffer buf
//returns the amount of chars written
int int_to_string(int i, char* buf, int max_chars);

unsigned long hex_to_long(char* buf);

//writes the ascii chars for the HEX representation of the
//long l on the buffer buf. returns the amount of chars written
int long_to_hex(unsigned long l, char* buf, int max_chars);

#endif // __DIC_H__
