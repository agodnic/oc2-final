#include "cui.h"

#include "clock.h"
#include "../constants.h"
#include "../hw/ia32e.h"
/*TODO:Delete after Debug*/
#include "../isr/isr.h"
#include "../sched/sched.h"

static screen_buffer_entry_t* screen_buffer = (screen_buffer_entry_t*) SCREEN_BUFFER_BASE;

static unsigned char rainbow_colors[] = {
	( BG_RED	| CHAR_CYAN   ), ( BG_YELLOW	| CHAR_BLUE   ),
	( BG_GREEN	| CHAR_PURPLE ), ( BG_CYAN		| CHAR_RED    ),
	( BG_BLUE	| CHAR_YELLOW ), ( BG_PURPLE	| CHAR_GREEN  )};

//BASIC

void set_char(pixel_t p, char c) {
	set_char_s(p, c, 0);
}

char get_char(pixel_t p) {
	return screen_buffer[ (SCREEN_COLUMN_COUNT*p.row) + p.column].c;
}

void set_mode(pixel_t p, unsigned char mode) {
	set_mode_s(p, mode, 0);
}

unsigned char get_mode(pixel_t p) {
	return screen_buffer[ (SCREEN_COLUMN_COUNT*p.row) + p.column].mode;
}

int print_m(pixel_t start, char* message, unsigned char mode) {
	return print_m_s_t(start, message, mode, 0, 0);
}

int print(pixel_t start, char* message) {
	return print_s_t(start, message, 0, 0);
}

void clear_screen() {
	unsigned int i = 0;
	while ( i < SCREEN_COLUMN_COUNT * (SCREEN_ROW_COUNT - 1) ) {
		screen_buffer[i].c = 32;	//Blank (space bar) char
		screen_buffer[i].mode = BG_BLACK | BRIGHT | CHAR_WHITE;
		i++;
	}
}

void paint_c(pixel_t start, pixel_t end, unsigned char mode, char c) {
	paint_c_s_tt_d(start, end, mode, c, 0, 0, 0, 0);
}

void paint(pixel_t start, pixel_t end, unsigned char mode) {
	paint_s_tt_d(start, end, mode, 0, 0, 0, 0);
}

void rainbow() {

	char blank = 32;
	pixel_t start = {0,0}; 
	pixel_t end = {23,1};
	unsigned char mode = BG_RED | CHAR_CYAN;
	paint_c_t_d(start,end,mode,blank,1,2);

	end.row = 0;
	end.column = 79;
	paint_c_t_d(start,end,mode,blank,1,0);

	start.column = 78;
	end.row = 23;
	paint_c_t_d(start,end,mode,blank,1,0);

	unsigned char up_row = 1;
	unsigned char left_col = 2;

	for (up_row = 1; up_row < 13; up_row++) {
		mode = rainbow_colors[up_row % 6];
		start.row = 24-up_row;
		start.column = left_col;
		end.column-=2;
		paint_c_t_d(start,end,mode,blank,1,2);

		end.column = left_col+1;
		start.row = up_row;
		paint_c_t_d(start,end,mode,blank,1,2);	

		end.row = up_row;
		end.column = 79-left_col;
		paint_c_t_d(start,end,mode,blank,1,0);

		start.column = 78-left_col;
		end.row = 23-up_row;
		paint_c_t_d(start,end,mode,blank,1,0);	

		left_col+=2;
	}
}

//ADVANCE

void set_char_s(pixel_t p, char c, unsigned long begin) {
	screen_buffer_entry_t* buff;
	if (begin == 0) buff = screen_buffer;
	else buff = (screen_buffer_entry_t*) begin;

	buff[ (SCREEN_COLUMN_COUNT*p.row) + p.column].c = c;
}

void set_mode_s(pixel_t p, unsigned char mode, unsigned long begin) {
	screen_buffer_entry_t* buff;
	if (begin == 0) buff = screen_buffer;
	else buff = (screen_buffer_entry_t*) begin;

	buff[ (SCREEN_COLUMN_COUNT*p.row) + p.column].mode = mode;
}

int print_m_s(pixel_t start, char* message, unsigned char mode, unsigned long begin) {
	return print_m_s_t(start, message, mode, begin, 0);
}

int print_m_t(pixel_t start, char* message, unsigned char mode, int time) {
	return print_m_s_t(start, message, mode, 0, time);
}

int print_s(pixel_t start, char* message, unsigned long begin) {
	return print_s_t(start, message, begin, 0);
}

int print_t(pixel_t start, char* message, int time) {
	return print_s_t(start, message, 0, time);
}

void paint_c_s(pixel_t start, pixel_t end, unsigned char mode, char c, unsigned long begin) {
	paint_c_s_tt_d(start, end, mode, c, begin, 0, 0, 0);
}

void paint_s(pixel_t start, pixel_t end, unsigned char mode, unsigned long begin) {
	paint_s_tt_d(start, end, mode, begin, 0, 0, 0);
}

void paint_c_t_d(pixel_t start, pixel_t end, unsigned char mode, char c, int time, unsigned char dir) {
	paint_c_s_tt_d(start, end, mode, c, 0, time, 0, dir);
}

void paint_t_d(pixel_t start, pixel_t end, unsigned char mode, int time, unsigned char dir) {
	paint_s_tt_d(start, end, mode, 0, time, 0, dir);
}

void paint_c_tt_d (pixel_t start, pixel_t end, unsigned char mode, char c, int th, int tv, unsigned char dir) {
	paint_c_s_tt_d(start, end, mode, c, 0, th, tv, dir);
}

void paint_tt_d	 (pixel_t start, pixel_t end, unsigned char mode, int th, int tv, unsigned char dir) {
	paint_s_tt_d(start, end, mode, 0, th, tv, dir);
}

void paint_c_t(pixel_t start, pixel_t end, unsigned char mode, char c, int time) {
	paint_c_s_tt_d(start, end, mode, c, 0, time, 0, 0);
}

void paint_t(pixel_t start, pixel_t end, unsigned char mode, int time) {
	paint_s_tt_d(start, end, mode, 0, time, 0, 0);
}

int print_m_s_t(pixel_t start, char* message, unsigned char mode, unsigned long begin, int time) {
	unsigned int i = 0;
	pixel_t current = start;
	while (( message[i] != 10 ) && ( current.column < 80 )) {
		wait(time);
		set_mode_s(current, mode, begin);
		set_char_s(current, message[i], begin);
		current.column++;
		i++;
	}
	return i;
}

int print_s_t(pixel_t start, char* message, unsigned long begin, int time) {
	unsigned int i = 0;
	pixel_t current = start;
	while (( message[i] != 10 ) && ( current.column < SCREEN_COLUMN_COUNT )) {
		wait(time);
		set_char_s(current, message[i], begin);
		current.column++;
		i++;
	}
	return i;
}

void paint_c_s_tt_d(pixel_t start, pixel_t end, unsigned char mode, char c, unsigned long begin, int th, int tv, unsigned char dir) {
	pixel_t current = start;
	if (dir==0) {				//up right -> down left
		while (( current.row <= end.row ) && ( current.row < SCREEN_ROW_COUNT )) {
			current.column = start.column;
			while (( current.column <= end.column ) && ( current.column < SCREEN_COLUMN_COUNT )) {
				if (th>0) wait(th);
				set_mode_s(current, mode, begin);
				set_char_s(current, c, begin);
				current.column ++;
			}
			if (tv>0) wait(tv);
			current.row++;
		}
	} else if (dir==1) {		//up left -> down right
		current.column = end.column;
		while (( current.row <= end.row ) && ( current.row < SCREEN_ROW_COUNT )) {
			current.column = end.column;
			while (( current.column >= start.column ) && ( current.column >= 0 )) {
				if (th>0) wait(th);
				set_mode_s(current, mode, begin);
				set_char_s(current, c, begin);
				current.column --;
			}
			if (tv>0) wait(tv);
			current.row++;
		}
	} else if (dir==2) {		//down left -> up right
		current = end;
		while (( current.row >= start.row ) && ( current.row >= 0 )) {
			current.column = end.column;
			while (( current.column >= start.column ) && ( current.column >= 0 )) {
				if (th>0) wait(th);
				set_mode_s(current, mode, begin);
				set_char_s(current, c, begin);
				current.column --;
			}
			if (tv>0) wait(tv);
			current.row--;
		}
	} else if (dir==3) {		//down right -> up left
		current.row = end.row;
		while (( current.row >= start.row ) && ( current.row >= 0 )) {
			current.column = start.column;
			while (( current.column <= end.column ) && ( current.column <= SCREEN_COLUMN_COUNT )) {
				if (th>0) wait(th);
				set_mode_s(current, mode, begin);
				set_char_s(current, c, begin);
				current.column ++;
			}
			if (tv>0) wait(tv);
			current.row--;
		}
	}
}

void paint_s_tt_d(pixel_t start, pixel_t end, unsigned char mode, unsigned long begin, int th, int tv, unsigned char dir) {
	pixel_t current = start;
	if (dir==0) {				//up right -> down left
		while (( current.row <= end.row ) && ( current.row < SCREEN_ROW_COUNT )) {
			current.column = start.column;
			while (( current.column <= end.column ) && ( current.column < SCREEN_COLUMN_COUNT )) {
				if (th>0) wait(th);
				set_mode_s(current, mode, begin);
				current.column ++;
			}
			if (tv>0) wait(tv);
			current.row++;
		}
	} else if (dir==1) {		//up left -> down right
		current.column = end.column;
		while (( current.row <= end.row ) && ( current.row < SCREEN_ROW_COUNT )) {
			current.column = end.column;
			while (( current.column >= start.column ) && ( current.column >= 0 )) {
				if (th>0) wait(th);
				set_mode_s(current, mode, begin);
				current.column --;
			}
			if (tv>0) wait(tv);
			current.row++;
		}
	} else if (dir==2) {		//down left -> up right
		current = end;
		while (( current.row >= start.row ) && ( current.row >= 0 )) {
			current.column = end.column;
			while (( current.column >= start.column ) && ( current.column >= 0 )) {
				if (th>0) wait(th);
				set_mode_s(current, mode, begin);
				current.column --;
			}
			if (tv>0) wait(tv);
			current.row--;
		}
	} else if (dir==3) {		//down right -> up left
		current.row = end.row;
		while (( current.row >= start.row ) && ( current.row >= 0 )) {
			current.column = start.column;
			while (( current.column <= end.column ) && ( current.column <= SCREEN_COLUMN_COUNT )) {
				if (th>0) wait(th);
				set_mode_s(current, mode, begin);
				current.column ++;
			}
			if (tv>0) wait(tv);
			current.row--;
		}
	}
}
