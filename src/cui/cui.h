// cui - console user interface
#ifndef __CUI_H__
#define __CUI_H__

/* primitive functions for screen buffer management
  with these we can build all other console user interface funcionalities */

typedef struct pixel {
	int row;
	int column;
} pixel_t;

typedef struct screen_buffer_entry {
	unsigned char c;
	unsigned char mode;
} screen_buffer_entry_t;

/**BASIC**/

//sets the char on a screen square
void set_char(pixel_t p, char c);

//returns the char on a screen square
char get_char(pixel_t p);

//sets the mode on a screen square
void set_mode(pixel_t p, unsigned char mode);

//returns the mode on a screen square
unsigned char get_mode(pixel_t p);

//prints the message on a screen line overwriting the mode
int print_m(pixel_t start, char* message, unsigned char mode);

//writes the message on a screen line leaving the previous mode
int print(pixel_t start, char* message);

//clears all chars on screen
void clear_screen();

//paints a rectangle with a char as brush on the screen
void paint_c(pixel_t start, pixel_t end, unsigned char mode, char c);

//draws a rectangle leaving the previous chars on the screen
void paint(pixel_t start, pixel_t end, unsigned char mode);

//draws a rainbow all over the screen
void rainbow();

/**ADVANCE**/

	//_s: selectable screen (not only videobuffer)
	//_t: timed, not instatly (integer as amount of time)
	//_t_d: timed, directionable ('up right -> down left' or 'down left -> up right')

void set_char_s(pixel_t p, char c,					unsigned long begin);
void set_mode_s(pixel_t p, unsigned char mode,	unsigned long begin);

int print_m_s(	pixel_t start, char* message, unsigned char mode,		unsigned long begin);
int print_s(	pixel_t start, char* message, 							unsigned long begin);
int print_m_t(	pixel_t start, char* message, unsigned char mode,								int time);
int print_t(	pixel_t start, char* message,													int time);
int print_m_s_t(pixel_t start,char* message, unsigned char mode,		unsigned long begin,	int time);
int print_s_t(	pixel_t start, char* message,							unsigned long begin,	int time);

void paint_c_s		(pixel_t start, pixel_t end, unsigned char mode, char c,	unsigned long begin);
void paint_s		(pixel_t start, pixel_t end, unsigned char mode, 			unsigned long begin);
void paint_c_t_d	(pixel_t start, pixel_t end, unsigned char mode, char c,							int time,		unsigned char dir);
void paint_t_d		(pixel_t start, pixel_t end, unsigned char mode, 									int time, 		unsigned char dir);
void paint_c_tt_d	(pixel_t start, pixel_t end, unsigned char mode, char c,							int th, int tv,	unsigned char dir);
void paint_tt_d		(pixel_t start, pixel_t end, unsigned char mode, 									int th, int tv,	unsigned char dir);
void paint_c_t		(pixel_t start, pixel_t end, unsigned char mode, char c,							int time);
void paint_t		(pixel_t start, pixel_t end, unsigned char mode, 									int time);
void paint_c_s_tt_d	(pixel_t start, pixel_t end, unsigned char mode, char c,	unsigned long begin,	int th, int tv, unsigned char dir);
void paint_s_tt_d	(pixel_t start, pixel_t end, unsigned char mode,			unsigned long begin,	int th, int tv, unsigned char dir);

#endif // __CUI_H__
