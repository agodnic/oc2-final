#include "dic.h"
#include "../constants.h"

int ascii_to_int(char a) {
	int result = 0;
	if ((a > 47) && (a < 58)) {
		result = (int) a;
		result -= 48;
	}
	return result;
}

int ascii16_to_int(char a) {
	int result = 0;
	if ((a > 47) && (a < 58)) {
		result = (int) a;
		result -= 48;
	} else if ((a > 64) && (a < 71)) {
		result = (int) a;
		result -= 55;
	} else if ((a > 96) && (a < 102)) {
		result = (int) a;
		result -= 87;
	}
	return result;
}

int string_to_int(char* buf) {
	int result = 0;
	int i = -1;
	int dec = 1;
	if (buf[0] == '-') {
		dec = -1;
		buf++;
	}
	while ((buf[i+1] < 58) && (buf[i+1] > 47)) {
		i++;
	}
	for (; i >= 0; i--) {
		result += ascii_to_int(buf[i])*dec;
		dec *= 10;
	}
	return result;
}

int int_to_string(int i, char* buf, int max_chars) {
	int result = 1;
	char minus = (i<0);
	int dec = 1;
	if (minus) {
		i *= -1;
		buf[0] = '-';
		buf++;
		result++;
	}
	while (i/dec > 9) {
		dec *= 10;
	}
	while ((dec > 1) && (result <= max_chars)) {
		buf[0] = i / dec;
		i -= dec*buf[0];
		buf[0] += 48;
		dec = dec / 10;
		buf++;
		result ++;
	}
	buf[0] = i+48;
	return result;
}

unsigned long hex_to_long(char* buf) {
	buf+=2;
	unsigned long res = 0;
	int i = -1;
	unsigned long dec = 1;
	while (((buf[i+1] < 58) && (buf[i+1] > 47)) ||
			((buf[i+1] < 71) && (buf[i+1] > 64)) ||
			((buf[i+1] < 102) && (buf[i+1] > 96))) {
		i++;
	}
	for (; i >= 0; i--) {
		res += ascii16_to_int(buf[i])*dec;
		dec *= 16;
	}
	return res;
}

int long_to_hex(unsigned long l, char* buf, int max_chars) {
	int result = 1;
	unsigned long dec = 1;
	unsigned long i = l;
	while (i/dec > 15) {
		dec *= 16;
	}
	while ((dec > 1) && (result <= max_chars)) {
		buf[0] = i / dec;
		i -= dec*buf[0];
		buf[0] += 48;
		if (buf[0]>57) buf[0] += 7;
		dec = dec / 16;
		buf++;
		result ++;
	}
	buf[0] = i+48;
	if (buf[0]>57) buf[0] += 7;
	return result;
}
