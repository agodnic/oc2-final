#ifndef __IA_32_E_H__
#define __IA_32_E_H__

/* TODO register clobbers should be added, otherwise we can have trouble */

#define LS_INLINE static __inline __attribute__((always_inline))


LS_INLINE unsigned long rcr3(void) {
        unsigned long val;
        __asm __volatile("movq %%cr3,%0" : "=r" (val));
        return val;
}

LS_INLINE void lcr3(unsigned long val){
	__asm __volatile("movq %q0, %%cr3" : : "a" (val)  );
}

LS_INLINE void breakpoint() {
        __asm __volatile("xchgw %%bx, %%bx" : : );
}

LS_INLINE void sti() {
        __asm __volatile("sti" : : );
}

LS_INLINE void cli() {
        __asm __volatile("cli" : : );
}

LS_INLINE void flush_tlb() {
	unsigned long cr3;
	__asm __volatile("movq %%cr3,%0" : "=r" (cr3));
	__asm __volatile("movq %0,%%cr3" : : "r" (cr3));
}

LS_INLINE void outb(int port, unsigned char data) {
	__asm __volatile("outb %0,%w1" : : "a" (data), "d" (port));
}

LS_INLINE void outbp(int port, unsigned char data) {
	__asm __volatile("outb %0,%w1\noutb %%al,$0x80": :"a"(data), "d" (port));
}

//FIXME!! Will cause trouble if page write attributes are unset for code pages
LS_INLINE void interrupt(unsigned char int_number) {
	__asm __volatile(
		"movb %0, point+1\n" //FIXME we should not be dong this
        	"point:\n"
        	"int $0\n"
	: : "r"(int_number) );
}

LS_INLINE void outw(unsigned short port, unsigned short val){

	__asm __volatile(
		//"xchg  %%bx, %%bx;"
		"movw    %1, %%ax;"
		"movw    %0, %%dx;"
		"outw  %%ax, %%dx;"
 		: /* no output */
		: "r"(port), "r"(val)
 		: "ax", "dx"  //clobbers al and dx
	);

}

LS_INLINE unsigned char inb(unsigned short port){
	unsigned char output;

	__asm __volatile(
		//"xchg  %%bx, %%bx;"
		"movw    %1, %%dx;"
		"inb   %%dx, %%al;"
		"movb  %%al,   %0;"
 		: "=r"(output)	//return value
		: "r"(port)
 		: "al", "dx"	//clobbers al and dx
	);

	return output;
}


LS_INLINE unsigned short inw(unsigned short port){
	unsigned short output;

	__asm __volatile(
		//"xchg  %%bx, %%bx;"
		"movw    %1, %%dx;"
		"inw   %%dx, %%ax;"
		"movw  %%ax,   %0;"
 		: "=r"(output)	//return value
		: "r"(port)
 		: "ax", "dx"	//clobbers al and dx
	);

	return output;
}

#endif
