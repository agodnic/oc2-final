#include "pic.h"

#include "ia32e.h"

#define PIC1_PORT 0x20
#define PIC2_PORT 0xA0

__inline __attribute__((always_inline)) void fin_intr_pic1(void) { outb(0x20, 0x20); } 
__inline __attribute__((always_inline)) void fin_intr_pic2(void) { outb(0x20, 0x20); outb(0xA0, 0x20); } 

void resetear_pic() {
	outb(PIC1_PORT+0, 0x11); /* IRQs activas x flanco, cascada, y ICW4 */
	outb(PIC1_PORT+1, 0x20); /* Addr */
	outb(PIC1_PORT+1, 0x04); /* PIC1 Master, Slave ingresa Int.x IRQ2 */
	outb(PIC1_PORT+1, 0x01); /* Modo 8086 */
	outb(PIC1_PORT+1, 0xFF); /* Enmasca todas! */

	outb(PIC2_PORT+0, 0x11); /* IRQs activas x flanco, cascada, y ICW4 */
	outb(PIC2_PORT+1, 0x28); /* Addr */
	outb(PIC2_PORT+1, 0x02); /* PIC2 Slave, ingresa Int x IRQ2 */
	outb(PIC2_PORT+1, 0x01); /* Modo 8086 */
	outb(PIC2_PORT+1, 0xFF); /* Enmasca todas! */

	//--Esto acelera las interrupciones del clock--//
		#ifndef HZ 
		#define HZ 90
		#endif
		// From include/asm/timex.h
		#define CLOCK_TICK_RATE 1193180 /* Underlying HZ */
		// From include/linux/timex.h
		#define LATCH ((CLOCK_TICK_RATE + HZ/2) / HZ) /* For divider */
		// From arch/i386/kernel/i8259.c
		outbp(0x43,0x34); /* binary, mode 2, LSB/MSB, ch 0 */ 
		outbp(0x40, LATCH & 0xff); /* LSB */
		outb(0x40, LATCH >> 8); /* MSB */
	//--//
}

void habilitar_pic() {
	outb(PIC1_PORT+1, 0x00);
	outb(PIC2_PORT+1, 0x00);
}

void deshabilitar_pic() {
	outb(PIC1_PORT+1, 0xFF);
	outb(PIC2_PORT+1, 0xFF);
}
