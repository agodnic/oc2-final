#ifndef __PIC_H__
#define __PIC_H__

void resetear_pic(void);
void habilitar_pic();
void deshabilitar_pic();

__inline __attribute__((always_inline)) void fin_intr_pic1(void);
__inline __attribute__((always_inline)) void fin_intr_pic2(void);

#endif
