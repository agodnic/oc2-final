#include "screen.h"

#include "../cui/cui.h"
#include "../cui/dic.h"
#include "../cui/key.h"
#include "../mmu/mmu.h"
#include "../term/term.h"
#include "../isr/isr.h"
#include "../hw/ia32e.h"
#include "../sched/sched.h"
#include "mmuscrn.h"
#include "schedscrn.h"
#include "../constants.h"

screen_buffer_entry_t* tmp_buffer;

unsigned char s_counter = 0;
int s_timer_leng = 4;
char s_timer[] =  {('/'),('-'),('\\'),('|')};

int task_registers = 0;

//both terminal_view (shows the terminal) and
//draw terminal (paints the terminal for the first time) call this
void common_terminal(int t);
void frame(int t);

void print_task_label();

void screen_init() {
	tmp_buffer = (screen_buffer_entry_t*) malloc_page_k(NULL, KERN, SCRN);
}

void interrupt_save_screen() {
	mem_cpy (SCREEN_BUFFER_BASE, (long unsigned int) tmp_buffer, (SCREEN_COLUMN_COUNT*(SCREEN_ROW_COUNT-1)*2));
}

void interrupt_load_screen() {
	mem_cpy ((long unsigned int) tmp_buffer, SCREEN_BUFFER_BASE, (SCREEN_COLUMN_COUNT*(SCREEN_ROW_COUNT-1)*2));
}

void toolbar() {
	pixel_t start = {24,0};
	pixel_t end = {24,79};
	paint_c_t(start, end, BG_RED | BRIGHT | CHAR_WHITE, 32, 1);

	end.column = 76;
	set_mode(end, BG_RED | BRIGHT | CHAR_BLACK);
	set_char(end, 30);
	end.column++;
	set_mode(end, BG_RED | BRIGHT | CHAR_GREEN);
	set_char(end,'M');
	end.column++;
	set_mode(end, BG_RED | BRIGHT | CHAR_BLACK);
	set_char(end,'I');
}

void terminal_view() {
	paint_c((pixel_t) {0,0}, (pixel_t) {0,79}, (get_focus() ? BG_BLACK | CHAR_WHITE : BG_BLUE | BRIGHT | CHAR_YELLOW), 32);
	common_terminal(0);
	load_bar();
	show_messages();
}

void mmu_screen() {
	char* title = "Mapa de memoria\n";
	int selected_page = get_selected_pos();
	char map = get_current_map();
	pixel_t start = {0,0};
	pixel_t end = {0,79};
	paint_c(start, end, (get_focus() ? BG_WHITE | BRIGHT | CHAR_BLACK : BG_BLUE | BRIGHT | CHAR_YELLOW), 32);
	start.column=3;
	print(start, title);
	start.column = 25;
	char* i = "    \n";
	int_to_string((map ? selected_page+512 : selected_page), i, 4);
	print(start, i);
	start.column = 30;
	set_char (start, '-');
	start.column+=2;
	char* t = "0x                \n";
	t += 2;
	long_to_hex((long) (map ? index_to_dir_u(selected_page) : index_to_dir_k(selected_page)), t, 16);
	t -= 2;
	print(start, t);
	start.column = 43;

	frame(0);
	load_bar();
	common_mmu_screen();
}

void common_mmu_screen() {
	pixel_t start = {0,43};
	pixel_t end = {22,78};
	int selected_page = get_selected_pos();
	char map = get_current_map();
	char type = get_type_page(map, selected_page);
	set_char(start, icon(get_owner(map, selected_page)));
	set_mode(start, type);
	print_type(type);
	start.row = 2; start.column = 1;
	paint_c(start, end, BG_WHITE | CHAR_BLACK, 32);
	paint((pixel_t){2,7}, (pixel_t){22,72}, BG_BLACK | CHAR_WHITE);
	char* tk = "Paginas del Kernel: 0x200000 - 0x3FF000\n";
	char* tu = "Paginas de Usuario: 0x400000 - 0x5FF000\n";
	print_m((pixel_t) {3,9}, tk, (get_focus() ? BG_BLACK | CHAR_WHITE : BG_RED | BRIGHT | CHAR_WHITE));
	print_m((pixel_t) {13,9}, tu, (get_focus() ? BG_BLACK | CHAR_WHITE : BG_RED | BRIGHT | CHAR_WHITE));

	unsigned int i;
	unsigned char color = (get_focus() ? BG_BLACK | CHAR_WHITE : BG_WHITE | CHAR_RED);
	start.row = 4; start.column = 8;
	for (i = 0; i < NUMBER_OF_PAGES; i++) {
		if (get_owner(KERNEL, i) == -1) {
			set_char(start, 0xB0);
			set_mode(start, color);
		} else {
			set_char(start, icon(get_owner(KERNEL,i)));
			set_mode(start, (get_focus() ? color : get_type_page(KERNEL, i)));
		}
		start.row+=10;
		if (get_owner(USER, i) == -1) {
			set_char(start, 0xB0);
			set_mode(start, color);
		} else {
			set_char(start, icon(get_owner(USER,i)));
			set_mode(start, (get_focus() ? color : get_type_page(USER, i)));
		}
		start.row-=10;
		if (start.column == 71) {
			start.row++; start.column = 8;
		} else start.column++;
	}
	paint_pos(!get_focus());
}

void sched_screen() {
	char* title = "Scheduler\n";
	pixel_t s = {0,0};
	pixel_t e = {0,79};
	paint_c(s, e, BG_WHITE | BRIGHT | CHAR_BLACK, 32);
	s.column=3;
	print(s, title);

	frame(0);
	load_bar();

	s.row = 2;
	s.column = 1;
	e.row = 22;
	e.column--;
	paint_c(s, e, BG_WHITE | CHAR_BLACK, 32);

	int pid = get_current_pid();
	int i = 0;
	s_counter = 0;
	s.row++;
	for (; i < NUMBER_OF_TASKS; i++) {
		s.column = 2 + 40*((i)%2);
		if (pid == i) {
			e.row = s.row;
			e.column = s.column;
			paint(s, e, BG_BLACK | BRIGHT | CHAR_GREEN);
			set_char(s, s_timer[s_counter]);
		} else if (get_state(i) == RUNNING) {
			set_char(s, -20);
		} else if (get_state(i) == BLOCKED) {
			set_char(s, -19);
		} else if (get_state(i) == LOADED) {
			set_char(s, 15);
		} else if (get_state(i) == EMPTY) {
			set_char(s, 28);
		}
		if (get_state(i) != EMPTY) {
			s.column += 2;
			set_char(s, i+97);
			s.column += 2;
			char* q = "    \n";
			q[int_to_string(get_quantum(i), q, 4)] = 10;
			print(s, q);
			s.column += 4;
			set_char(s,'[');
			s.column++;
			print(s, task_name(i));
			s.row++;
			s.column -= 8;
			char* t = "0x                \n";
			t += 2;
			long_to_hex((long) get_stack(i), t, 16);
			t -= 2;
			print(s, t);
			s.row--;
		}
		s.row += 2*((i)%2);
	}
	s.row+=1;
	s.column = 17;
	char* w = "    \n";
	w[int_to_string((int) get_current_quantum(), w, 4)] = 10;
	print(s, w);
	char* w2 = "Quantum Actual:\n";
	s.column = 2;
	print(s,w2);
	paint_task();
}

void print_registers(char interrupt) {
	char* title = "Registros\n";
	pixel_t s = {0,0};
	pixel_t e = {0,79};
	paint_c(s, e, (interrupt ? BG_BLUE | BRIGHT | CHAR_YELLOW : BG_WHITE | BRIGHT | CHAR_BLACK), 32);
	s.column = 3;
	print(s, title);

	frame(0);
	load_bar();

	e.row = 22;
	s.row = 2;
	s.column = 1;
	e.column = 78;
	paint_c(s, e, (interrupt ? BG_GREEN | CHAR_BLACK : BG_BLACK | CHAR_WHITE), 32);

	print_task_label();

	s.row = 4; s.column = 3;
	char* rax = "RAX:\n";
	print(s, rax);
	s.row++;
	char* rbx = "RBX:\n";
	print(s, rbx);
	s.row++;
	char* rcx = "RCX:\n";
	print(s, rcx);
	s.row++;
	char* rdx = "RDX:\n";
	print(s, rdx);
	s.row++;
	char* rdi = "RDI:\n";
	print(s, rdi);
	s.row++;
	char* rsi = "RSI:\n";
	print(s, rsi);
	s.row++;
	char* r8 = "R8 :\n";
	print(s, r8);
	s.row++;
	char* r9 = "R9 :\n";
	print(s, r9);
	s.row++;
	char* r10 = "R10:\n";
	print(s, r10);
	s.row++;
	char* r11 = "R11:\n";
	print(s, r11);
	s.row++;
	char* r12 = "R12:\n";
	print(s, r12);
	s.row++;
	char* r13 = "R13:\n";
	print(s, r13);
	s.row++;
	char* r14 = "R14:\n";
	print(s, r14);
	s.row++;
	char* r15 = "R15:\n";
	print(s, r15);
	s.row++;
	char* cr3 = "CR3:\n";
	print(s, cr3);
	s.row++;
	char* flg = "flg:\n";
	print(s, flg);
	s.row++;
	char* rip = "RIP:\n";
	print(s, rip);
	print_task_registers((interrupt ? get_current_pid() : task_registers));
}

void draw_terminal() {
	common_terminal(1);
	char* welcome = " Bienvenido al Kernel. Teclee \'LIST\' para obtener la lista de comandos.\n";
	send_message(KERN, welcome);
	show_messages();
}

void common_terminal(int t) {
	char* title = "Terminal\n";
	pixel_t start = {0,0};
	pixel_t end = {0,79};
	paint(start, end, BG_BLACK | CHAR_BLACK);
	start.column = 3;
	print(start, title);
	start.column = 0;
	paint_t(start, end, (get_focus() ? BG_WHITE | BRIGHT | CHAR_BLACK : BG_BLUE | BRIGHT | CHAR_YELLOW), t);

	frame(t);
	pixel_t s = {2,1};
	pixel_t e = {22,78};
	paint_c_tt_d(s, e, (get_focus() ? BG_WHITE | CHAR_BLACK : BG_CYAN | CHAR_BLACK), 32, 0, t, 0);

	if (t>0) {
		start.row = 23;
		start.column = 2;
		set_mode(start, (get_focus() ? BG_YELLOW | BLINK | CHAR_RED : BG_BLACK | BRIGHT | CHAR_BLACK));
		set_char(start, 0xAF);
	}
}

void update_screen_malloc(char m, unsigned int p, int pid, char type) {
	if ((get_menu() == MENU_MMU)) {
		pixel_t q = (m ? index_to_pos_u(p) : index_to_pos_k(p));
		set_char(q, icon(pid));
		pixel_t s = {0,43};
		set_char(s, icon(pid));
		int index = get_selected_pos();
		if (p==index && get_current_map()==m) print_type(type);
		if (!get_focus()) {
			if (!(p==index && get_current_map()==m)) {
				set_mode(q, type);
			}
			set_mode(s, type);
		}
	}
}

void update_screen_free(char m, unsigned int p) {
	if ((get_menu() == MENU_MMU)) {
		pixel_t q = (m ? index_to_pos_u(p) : index_to_pos_k(p));
		set_char(q, 0xB0);
		pixel_t s = {0,43};
		set_char(s, 0xB0);
		int index = get_selected_pos();
		if (p==index && get_current_map()==m) print_type(NO_TYPE);
		if (!get_focus()) {
			if (!(p==index && get_current_map()==m)) {
				set_mode(q, BG_WHITE | CHAR_RED);
			}
			set_mode(s, BG_WHITE | CHAR_RED);
		}
	}
}

void update_screen_tick() {
	int pid = get_current_pid();
	char* w = "   \n";
	pixel_t s = {0, 51};
	pixel_t e = {0, 53};
	if (get_menu() == MENU_SCHED) {
		s.row = 3 + (pid/2)*2;
		s.column = 2 + (40*(pid%2));
		s_counter++;
		if (s_counter == s_timer_leng) s_counter = 0;
		set_char(s, s_timer[s_counter]);
		s.row = 4 + NUMBER_OF_TASKS;
		s.column = 17;
		e = s;
		e.column += 2;
		paint_c(s,e, (get_focus() ? BG_WHITE | CHAR_BLACK : BG_YELLOW | CHAR_BLACK), 32);
		w[int_to_string((int) get_current_quantum(), w, 3)] = 10;
		print(s, w);
	} else if (get_menu() == 6+pid) {
		paint_c(s,e, (get_focus() ? BG_WHITE | BRIGHT | CHAR_BLACK
			: BG_BLUE | BRIGHT | CHAR_YELLOW), 32);
		w[int_to_string((int) get_current_quantum(), w, 3)] = 10;
		print(s, w);
		s.column = 1;
		s_counter++;
		if (s_counter == s_timer_leng) s_counter = 0;
		set_char(s, s_timer[s_counter]);
	} else if (get_menu() >= 6) {
		paint_c(s,e, (get_focus() ? BG_WHITE | BRIGHT | CHAR_BLACK
			: BG_BLUE | BRIGHT | CHAR_YELLOW), 32);
		set_char(s,'-');
	}
}

void update_screen_switch(int old_pid, int new_pid) {
	if (get_menu() == MENU_SCHED) {
		char c = (get_focus() ? BG_WHITE | CHAR_BLACK : BG_YELLOW | CHAR_BLACK);
		pixel_t s = {3 + (old_pid/2)*2, 2 + (40*(old_pid%2))};
		paint(s,s, c);
		s.row++;
		s.column++;
		char* t = "0x                \n";
		t += 2;
		long_to_hex((long) get_stack(old_pid), t, 16);
		t -= 2;
		if (get_state(old_pid)!=0) print(s, t);
		s.row = 3 + (new_pid/2)*2;
		s.column = 2 + (40*(new_pid%2));
		c = (get_focus() ? BG_BLACK | BRIGHT | CHAR_GREEN : BG_CYAN | BRIGHT | CHAR_GREEN);
		paint(s,s, c);
	}
}
void update_screen_time_out(int pid) {
	if (get_menu() == MENU_SCHED) {
		pixel_t p = {3 + (pid/2)*2, 2 + (40*(pid%2))};
		set_char(p, -20);
	} else if (get_menu() == 6+pid) {
		set_char((pixel_t){0,1}, -20);
	}
}

void update_screen_block(int pid) {
	if (get_menu() == MENU_SCHED) {
		pixel_t p = {3 + (pid/2)*2, 2 + (40*(pid%2))};
		set_char(p, -19);
	} else if (get_menu() == 6+pid) {
		set_char((pixel_t){0,1}, -19);
	}
}

void update_screen_load(int pid) {
	if (get_menu() == MENU_SCHED) {
		char c = (get_focus() ?
			BG_WHITE | CHAR_BLACK :
			BG_YELLOW | CHAR_BLACK );
		pixel_t s = {3 + (pid/2)*2, 2 + (40*(pid%2))};
		set_char(s, 15);
		s.column += 2;
		set_char(s, pid+97);
		s.column += 2;
		char* q = "    \n";
		q[int_to_string((int) get_quantum(pid), q, 4)] = 10;
		print(s, q);
		s.column += 4;
		set_char(s,'[');
		s.column++;
		print(s, task_name(pid));
		s.row++;
		pixel_t e = s;
		s.column -= 8;
		e.column += 20;
		paint_c(s,e,c,32);
		char* t = "0x                \n";
		t += 2;
		long_to_hex((long) get_stack(pid), t, 16);
		t -= 2;
		print(s, t);
		s.row--;
	}
}

void update_screen_run(int pid) {
	if (get_menu() == MENU_SCHED) {
		pixel_t s = {3 + (pid/2)*2, 2 + (40*(pid%2))};
		set_char(s, -20);
		s.column += 2;
		set_char(s, pid+97);
		s.column += 2;
		char* q = "    \n";
		q[int_to_string((int) get_quantum(pid), q, 4)] = 10;
		print(s, q);
		s.column += 5;
		print(s, task_name(pid));
	} else if (get_menu() == 6+pid) {
		set_char((pixel_t){0,1}, -20);
	}
}

void update_screen_unblock(int pid) {
	if (get_menu() == MENU_SCHED) {
		pixel_t s = {3 + (pid/2)*2, 2 + (40*(pid%2))};
		set_char(s, -20);
	} else if (get_menu() == 6+pid) {
		set_char((pixel_t){0,1}, -20);
	}
}

void update_screen_end(int pid) {
	if (get_menu() == MENU_SCHED) {
		char c = (get_focus() ?
			(pid==get_selected_task() ? BG_BLACK | CHAR_WHITE : BG_WHITE | CHAR_BLACK) :
			(pid==get_selected_task() ? BG_BLUE | BRIGHT | CHAR_WHITE : BG_YELLOW | CHAR_BLACK) );
		pixel_t s = {3 + (pid/2)*2, 2 + (40*(pid%2))};
		set_char(s, 28);
		s.column ++;
		pixel_t e = s;
		e.column+=34;
		paint_c(s,e, c,32);
		s.row++;
		e.row++;
		c = (get_focus() ? (BG_WHITE | CHAR_BLACK) : (BG_YELLOW | CHAR_BLACK) );
		paint_c(s,e, c,32);
	}
}

void update_screen_name(int pid) {
	if (get_menu() == MENU_SCHED) {
		char c = (get_focus() ?
			(pid==get_selected_task() ? BG_BLACK | CHAR_WHITE : BG_WHITE | CHAR_BLACK) :
			(pid==get_selected_task() ? BG_BLUE | BRIGHT | CHAR_WHITE : BG_YELLOW | CHAR_BLACK) );
		pixel_t s = {3 + (pid/2)*2, 11 + (40*(pid%2))};
		pixel_t e = s;
		e.column+=23;
		paint_c(s,e, c,32);
		print(s, task_name(pid));
	}
}

void update_screen_quantum(int pid) {
	if (get_menu() == MENU_SCHED) {
		char c = (get_focus() ?
			(pid==get_selected_task() ? BG_BLACK | CHAR_WHITE : BG_WHITE | CHAR_BLACK) :
			(pid==get_selected_task() ? BG_BLUE | BRIGHT | CHAR_WHITE : BG_YELLOW | CHAR_BLACK) );
		pixel_t s = {3 + (pid/2)*2, 6 + (40*(pid%2))};
		pixel_t e = s;
		e.column+=3;
		paint_c(s,e,c,32);
		char* q = "    \n";
		q[int_to_string((int) get_quantum(pid), q, 4)] = 10;
		print(s, q);
	}
}

void print_task_label() {
	pixel_t s = {3,19};
	pixel_t e = {3,58};
	paint_c(s,e,(get_focus() ? 0x07 : 0x20),32);
	set_char(s, '<');
	s.column = 27;
	s.column = 29 + print(s, task_name(task_registers));
	set_char(s, '>');
	s.column = 24;
	set_char(s, icon(task_registers));
	s.column = 26;
	set_char(s, '[');
	s.column = 21;
	if (get_state(task_registers) == RUNNING) {
		set_char(s, -20);
	} else if (get_state(task_registers) == BLOCKED) {
		set_char(s, -19);
	} else if (get_state(task_registers) == LOADED) {
		set_char(s, 15);
	} else if (get_state(task_registers) == EMPTY) {
		set_char(s, 28);
	}
}

void regs_up() {
	if (get_menu() == MENU_REGS) {
	}
}

void regs_dn() {
	if (get_menu() == MENU_REGS) {
	}
}

void regs_right() {
	if (get_menu() == MENU_REGS) {
		int i;
		for(i = 1; i != NUMBER_OF_TASKS; i++) {
			if(get_state( (task_registers + i) % NUMBER_OF_TASKS) != EMPTY) {
				task_registers = (task_registers + i) % NUMBER_OF_TASKS;
				print_task_label();
				update_registers();
				break;
			}
		}
	}
}

void regs_left() {
	if (get_menu() == MENU_REGS) {
		int i;
		for(i = 1; i != NUMBER_OF_TASKS; i++) {
			if(get_state( (NUMBER_OF_TASKS + task_registers - i) % NUMBER_OF_TASKS) != EMPTY) {
				task_registers = (NUMBER_OF_TASKS + task_registers - i) % NUMBER_OF_TASKS;
				print_task_label();
				update_registers();
				break;
			}
		}
	}
}

void update_registers() {
	if ((get_menu() == MENU_REGS)) {
		print_task_registers(task_registers);
	}
}

void print_type() {
	char* m = "\n";
	pixel_t s = {0, 45};
	pixel_t e = {0, 79};
	paint_c(s, e, (get_focus() ? BG_WHITE | BRIGHT | CHAR_BLACK : BG_BLUE | CHAR_WHITE), 32);
	int selected_page = get_selected_pos();
	char map = get_current_map();
	char t = get_type_page(map, selected_page);
	switch (t) {
		case HD:
		m = "Driver del Disco Duro\n";
		break;
		case TERM:
		m = "Registro de la Terminal\n";
		break;
		case SCRN:
		m = "Pantalla\n";
		break;
		case STACK:
		m = "Pila (Stack)\n";
		break;
		case PAGE:
		m = "Tablas de Paginacion\n";
		break;
		case OTHER:
		m = "Otras\n";
		break;
	}
	print(s, m);
}

void frame(int t) {
	pixel_t start = {0,0};
	pixel_t end = {1,79};

	set_mode(end, BG_PURPLE | BRIGHT | CHAR_YELLOW);
	set_char(end,0xBB);
	end.row = 22;
	start.row = 2;
	start.column = 79;
	paint_c_t_d(start, end, BG_PURPLE | BRIGHT | CHAR_YELLOW, 0xBA, t, 0);
	end.row++;
	set_mode(end, BG_PURPLE | BRIGHT | CHAR_YELLOW);
	set_char(end, 0xB6);
	end.column--;
	set_mode(end, BG_PURPLE | BRIGHT | CHAR_YELLOW);
	set_char(end, 0xDA);
	end.column--;
	start.row = 23;
	start.column = 2;
	if (t>0) {
		paint_c_t_d(start, end, (get_focus() ? BG_YELLOW | CHAR_BLACK : BG_BLACK | BRIGHT | CHAR_BLACK), 32, t, 2);
	}
	start.column--;
	set_mode(start, BG_PURPLE | BRIGHT | CHAR_YELLOW);
	set_char(start, 0xBF);
	start.column--;
	set_mode(start, BG_PURPLE | BRIGHT | CHAR_YELLOW);
	set_char(start, 0xC7);
	end.row--;
	end.column = 0;
	start.row = 2;
	paint_c_t_d(start, end, BG_PURPLE | BRIGHT | CHAR_YELLOW, 0xBA, t, 2);
	start.row--;
	set_mode(start, BG_PURPLE | BRIGHT | CHAR_YELLOW);
	set_char(start, 0xC9);
	start.column++;
	end.row = 1;
	end.column = 78;
	paint_c_t_d(start, end, BG_PURPLE | BRIGHT | CHAR_YELLOW, 0xCD, t, 0);
}
