// screen manager - control over the shown screen
#ifndef __SCREEN_H__
#define __SCREEN_H__

extern void mem_cpy(unsigned long src, unsigned long dst, unsigned int number_of_bytes);

//creates buffers to strore screens - this might dissapeard soon...
void screen_init();

//saves the screen after the CPU is interrupted
void interrupt_save_screen();

//restores the screen after returning from an interrupt
void interrupt_load_screen();

//prints toolbar
void toolbar();

//shows terminal when change to terminal view
void terminal_view();

//shows mmu on screen
void mmu_screen();
void common_mmu_screen();

//shows scheduler on screen
void sched_screen();

//shows registers on screen
void print_registers(char interrupt);

//paints animated terminal screen on SCREEN
//it must be called only the first time, then use terminal_view()
void draw_terminal();

void update_screen_free(char m, unsigned int p);
void update_screen_malloc(char m, unsigned int p, int pid, char type);

void update_screen_tick();
void update_screen_switch(int old_pid, int new_pid);
void update_screen_time_out(int pid);
void update_screen_block(int pid);
void update_screen_load(int pid);
void update_screen_run(int pid);
void update_screen_unblock(int pid);
void update_screen_end(int pid);
void update_screen_name(int pid);
void update_screen_quantum(int pid);

void regs_up();
void regs_dn();
void regs_right();
void regs_left();
void update_registers();

void print_type();

#endif // __SCREEN_H__
