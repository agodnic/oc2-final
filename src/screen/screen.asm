BITS 64

global mem_cpy

;void mem_cpy(unsigned long src, unsigned long dst, unsigned int number_of_bytes)
;copy qword-sized blocks
mem_cpy:
	push	Rbp
	mov	Rbp,	Rsp
		push	Rax									;apilo Rax, pues voy a utilizarlo
		push	Rbx									;apilo Rbx, pues voy a utilizarlo
		push	Rcx									;apilo Rcx, pues voy a utilizarlo
		push	Rdx									;apilo Rdx, pues voy a utilizarlo
		push	Rdi									;apilo Rdi, pues voy a utilizarlo
		push	Rsi									;apilo Rsi, pues voy a utilizarlo

		xchg	Rdi, Rsi
		mov		Rcx, Rdx
		shr		Rcx, 3
		rep		movsq

		pop		Rsi									;desapilo Rsi
		pop		Rdi									;desapilo Rdi
		pop		Rdx									;desapilo Rdx
		pop		Rcx									;desapilo Rcx
		pop		Rbx									;desapilo Rbx
		pop		Rax									;desapilo Rax
		pop		Rbp
		ret
