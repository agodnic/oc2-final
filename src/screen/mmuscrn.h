#ifndef __MMUSCRN_H__
#define __MMUSCRN_H__

#include "../cui/cui.h"		//Para pixel_t

int get_selected_pos();

void mmu_up();
void mmu_down();
void mmu_left();
void mmu_right();

void mmu_malloc();
void mmu_free();

void mmu_owner_up();
void mmu_owner_dn();
void mmu_type_up();
void mmu_type_dn();

void paint_pos(char i);
char get_current_map();

//returns the virtual adress of the page in the index i in the mmu
unsigned long index_to_dir_k(int i);
unsigned long index_to_dir_u(int i);
//returns the pixel for the page indexed with i in the mmu
pixel_t index_to_pos_k(int i);
pixel_t index_to_pos_u(int i);

#endif // __MMUSCRN_H__
