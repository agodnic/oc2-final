#include "taskscrn.h"
#include "../isr/isr.h"			// Para set_menu
#include "../mmu/mmu.h"			// Para malloc y free
#include "../sched/sched.h"		// Para get_current_pid
#include "../hw/ia32e.h"		// Para breakpoint
#include "../cui/key.h"			// Para icon
#include "../cui/dic.h"			// Para int_to_string
#include "screen.h"				// Para update_screen_*
#include "../constants.h"

extern void mem_cpy(unsigned long src, unsigned long dst, unsigned int number_of_bytes);

static unsigned long task_screen[NUMBER_OF_TASKS];

void init_task_scrn(int pid) {
	task_screen[pid] = (unsigned long) malloc_page_k(NULL, pid, SCRN);
}

void end_task_scrn(int pid) {
	free_page_k(task_screen[pid]);
}

void task_scrn_view(int pid) {
	char* title = "Pantalla de la tarea   [ \n";
	pixel_t s = {0,0};
	pixel_t e = {0,79};
	paint_c(s, e, BG_WHITE | BRIGHT | CHAR_BLACK, 32);
	s.column = 3;
	print(s, title);
	s.column = 27;
	print(s, task_name(pid));
	s.column = 24;
	set_char(s, icon(pid));
	s.column = 51;
	char* w = "   \n";
	int q = (get_current_pid() == pid ? get_current_quantum() : 0);
	w[int_to_string(q, w, 3)] = 10;
	s.column = 54;
	set_char(s, '/');
	s.column ++;
	char* w2 = "   \n";
	w2[int_to_string(get_quantum(pid), w2, 3)] = 10;
	print(s, w2);
	s.column = 1;
	if (get_state(pid) == RUNNING) {
		set_char(s, -20);
	} else if (get_state(pid) == BLOCKED) {
		set_char(s, -19);
	} else if (get_state(pid) == LOADED) {
		set_char(s, 15);
	} else if (get_state(pid) == EMPTY) {
		set_char(s, 28);
	}

	mem_cpy(task_screen[pid], (unsigned long) TASK_SCRN_START,
	(SCREEN_COLUMN_COUNT*(SCREEN_ROW_COUNT-3)*2));
	set_menu(6+pid);
}

void update_task_screen_name(int pid) {
	if (get_menu()==6+pid) {
		pixel_t start = {0,27};
		pixel_t end = {0,50};
		paint_c(start, end, (get_focus() ? BG_WHITE | BRIGHT | CHAR_BLACK
			: BG_BLUE | BRIGHT | CHAR_YELLOW), 32);
		print(start, task_name(pid));
	}
}

void update_task_screen_q(int pid) {
	if (get_menu()==6+pid) {
		pixel_t s = {0, 55};
		pixel_t q = s;
		q.column += 2;
		paint_c(s,q, (get_focus() ? BG_WHITE | BRIGHT | CHAR_BLACK
			: BG_BLUE | BRIGHT | CHAR_YELLOW), 32);
		char* w = "   \n";
		w[int_to_string((int) get_quantum(pid), w, 3)] = 10;
		print(s, w);
	}
}

void task_qu() {
	int pid = get_menu() -6;
	if (pid > 0 && pid < NUMBER_OF_TASKS) {
		if (get_state(pid)!=EMPTY) {
			set_quantum(pid, get_quantum(pid) + 10);
			update_screen_quantum(pid);
			update_task_screen_q(pid);
		}
	}
}

void task_qd() {
	int pid = get_menu() -6;
	if (pid > 0 && pid < NUMBER_OF_TASKS) {
		if (get_state(pid)!=EMPTY) {
			set_quantum(pid, get_quantum(pid) - 10);
			update_screen_quantum(pid);
			update_task_screen_q(pid);
		}
	}
}

void task_run() {
	int pid = get_menu() -6;
	if (pid > 0 && pid < NUMBER_OF_TASKS) {
		if (get_state(pid)==LOADED) {
			run_task(pid);
			update_screen_run(pid);
		}
	}
}

void task_kill() {
	int pid = get_menu() -6;
	if (pid > 0 && pid < NUMBER_OF_TASKS) {
		if (get_state(pid)!=EMPTY) {
			//it might not return from here (if this task is currently running)
			//so, if(get_current_pid()==pid) menu_terminal()
			end_task_cmd(pid);
		}
	}
}

void paint_task_scrn(pixel_t s, pixel_t e, unsigned char mode) {
	int pid = get_current_pid();
	paint_c_s(s, e, mode, 219, task_screen[pid]);
	if (get_menu()==6+pid) {
		if (s.row<0) s.row = 0; if (e.row>21) e.row=21;
		paint_c_s(s, e, mode, 219, TASK_SCRN_START);
	}
}

void print_task_scrn(pixel_t s, void* dir, unsigned char mode) {
	int pid = get_current_pid();
	if ((s.row > 0) && (s.row < 22)) {
		if (mode == 0) print_s(s, dir, task_screen[pid]);// if the mode is 0, leave previos mode
		else print_m_s(s, dir, mode, task_screen[pid]);
		if (get_menu()==6+pid) {
			if (mode == 0) print_s(s, dir, TASK_SCRN_START);// if the mode is 0, leave previos mode
			else print_m_s(s, dir, mode, TASK_SCRN_START);
		}
	}
}

void stamp_task_scrn(int x, int y, int w, int h, void* dir) {
	if (x+w <= 0) return;
	if (x >= 80) return;
	int cutXr = x+w-80;
	int pid = get_current_pid();
	int counter = (2 * w * h) -1;
	pixel_t p = {y+h-1,x+w-1};
	unsigned char* source = dir;
	if (cutXr > 0) {
		counter -= 2*cutXr;
		p.column -= cutXr;
	}
	for (; counter > 0; counter-=2) {
		if (source[counter-1] != 0) {// if the char is 0, leave transparent
			set_char_s(p,source[counter-1], task_screen[pid]);
			if ((source[counter] & 0b10000000) == 0) {//if 'blink' then keep mode
				set_mode_s(p,source[counter], task_screen[pid]);
			}
			if ((get_menu()==6+pid) && (p.row >= 0) && (p.row < 22)) {
				set_char_s(p,source[counter-1], TASK_SCRN_START);
				if ((source[counter] & 0b10000000) == 0) {//if 'blink' then keep mode
					set_mode_s(p,source[counter], TASK_SCRN_START);
				}
			}
		}
		if (p.column > x) {
			p.column--;
		} else {
			if (cutXr > 0) {
				counter -= 2*cutXr;
				p.column = 79;
			} else {
				p.column = x+w-1;
			}
			p.row--;
		}
		if (p.column < 0) {
			counter += 2*x;
			p.column = x+w-1;
			p.row--;
		}
	}
}
