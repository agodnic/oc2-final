#ifndef __SCHEDSCRN_H__
#define __SCHEDSCRN_H__

void sched_up();

void sched_down();

void sched_left();

void sched_right();

void sched_load();

void sched_run();

void sched_run_all();

void sched_qu();

void sched_qd();

void sched_kill();

void sched_block_();

void sched_unblock();

void paint_task();

int get_selected_task();

#endif // __SCHEDSCRN_H__
