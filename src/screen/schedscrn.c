#include "schedscrn.h"

#include "../cui/cui.h"
#include "../isr/isr.h"
#include "../sched/sched.h"
#include "screen.h"
#include "../constants.h"

int selected_task = 0;

void sched_up() {
	if ((get_menu() == MENU_SCHED) && (!get_focus())) {
			pixel_t s = {3 + 2*(selected_task/2), 3 + 40*((selected_task)%2)};
			pixel_t e = s;
			e.column = s.column+34;
			paint(s,e, BG_YELLOW | CHAR_BLACK);
			if (selected_task < 2) {
				selected_task += NUMBER_OF_TASKS;
				s.row = 1 + NUMBER_OF_TASKS;
			} else {
				s.row -= 2;
			}
			e.row = s.row;
			selected_task -= 2;
			paint(s,e, BG_BLUE | BRIGHT | CHAR_WHITE);
	}
}

void sched_down() {
	if ((get_menu() == MENU_SCHED) && (!get_focus())) {
			pixel_t s = {3 + 2*(selected_task/2), 3 + 40*((selected_task)%2)};
			pixel_t e = s;
			e.column = s.column+34;
			paint(s,e, BG_YELLOW | CHAR_BLACK);
			selected_task += 2;
			if (selected_task >= NUMBER_OF_TASKS) {
				selected_task -= NUMBER_OF_TASKS;
				s.row = 3;
			} else {
				s.row += 2;
			}
			e.row = s.row;
			paint(s,e, BG_BLUE | BRIGHT | CHAR_WHITE);
	}
}

void sched_left() {
	if ((get_menu() == MENU_SCHED) && (!get_focus())) {
			pixel_t s = {3 + 2*(selected_task/2), 3 + 40*((selected_task)%2)};
			pixel_t e = s;
			e.column = s.column+34;
			paint(s,e, BG_YELLOW | CHAR_BLACK);
			if (s.column > 40) {
				s.column = 3;
			} else {
				s.column = 43;
			}
			if (selected_task == 0) {
				selected_task = NUMBER_OF_TASKS;
				s.row = 1 + NUMBER_OF_TASKS;
			} else if (s.column > 40) {
				s.row -= 2;
			}
			e.row = s.row;
			e.column = s.column + 34;
			selected_task --;
			paint(s,e, BG_BLUE | BRIGHT | CHAR_WHITE);
	}
}

void sched_right() {
	if ((get_menu() == MENU_SCHED) && (!get_focus())) {
			pixel_t s = {3 + 2*(selected_task/2), 3 + 40*((selected_task)%2)};
			pixel_t e = s;
			e.column = s.column+34;
			paint(s,e, BG_YELLOW | CHAR_BLACK);
			selected_task ++;
			if (s.column > 40) {
				s.column = 3;
			} else {
				s.column = 43;
			}
			if (selected_task == NUMBER_OF_TASKS) {
				selected_task = 0;
				s.row = 3;
			} else if (s.column < 40) {
				s.row += 2;
			}
			e.row = s.row;
			e.column = s.column + 34;
			paint(s,e, BG_BLUE | BRIGHT | CHAR_WHITE);
	}
}

void sched_load() {
	if (get_state(selected_task)==EMPTY) {
		if (load_task_i("/BIN/IDLE.TSK", selected_task, DEFAULT_QUANTUM)>0)
			update_screen_load(selected_task);
	}
}

void sched_run() {
	if (get_state(selected_task)==LOADED) {
		run_task(selected_task);
		update_screen_run(selected_task);
	}
}

void sched_run_all() {
	int i=0;
	for (; i < NUMBER_OF_TASKS; i++) {
		if (get_state(i) == LOADED) {
			run_task(i);
			update_screen_run(i);
		}
	}
}

void sched_qu() {
	if (get_state(selected_task)!=EMPTY && selected_task!=0) {
		set_quantum(selected_task, get_quantum(selected_task) + 10);
		update_screen_quantum(selected_task);
	}
}

void sched_qd() {
	if (get_state(selected_task)!=EMPTY && selected_task!=0) {
		set_quantum(selected_task, get_quantum(selected_task) - 10);
		update_screen_quantum(selected_task);
	}
}

void sched_kill() {
	if (get_state(selected_task)!=EMPTY && selected_task!=0) {
		end_task_cmd(selected_task);
	}
}

void sched_block_() {
	if (get_state(selected_task)==RUNNING && selected_task!=0) {
		block_task(selected_task);
	}
}

void sched_unblock() {
	unblock(selected_task);
}

void paint_task() {
	pixel_t s = {3 + 2*(selected_task/2), 3 + 40*((selected_task)%2)};
	pixel_t e = s;
	e.column = s.column+34;
	paint(s,e, (get_focus() ? BG_BLACK | CHAR_WHITE : BG_BLUE | BRIGHT | CHAR_WHITE));
}

int get_selected_task() {
	return selected_task;
}
