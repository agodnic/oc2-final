#ifndef __TASKSCRN_H__
#define __TASKSCRN_H__
#include "../cui/cui.h"			// Para pixel_t

void init_task_scrn(int pid);
void end_task_scrn(int pid);

void task_scrn_view(int pid);
void update_task_screen_name(int pid);
void update_task_screen_q(int pid);

void task_qu();
void task_qd();
void task_run();
void task_kill();

void paint_task_scrn(pixel_t s, pixel_t e, unsigned char mode);
void print_task_scrn(pixel_t s, void* dir, unsigned char mode);
void stamp_task_scrn(int x, int y, int w, int h, void* dir);

#endif // __TASKSCRN_H__
