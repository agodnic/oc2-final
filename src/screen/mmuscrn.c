#include "mmuscrn.h"

#include "../cui/cui.h"
#include "../cui/dic.h"		// Para index_to_dir
#include "../cui/key.h"		// Para icon
#include "../isr/isr.h"
#include "../mmu/mmu.h"
#include "../sched/sched.h"
#include "screen.h"
#include "../constants.h"

pixel_t map_pos = {4,8};

int get_selected_pos() {
	int result = 64;
	if (map_pos.row > 12) {	//USER
		result *= map_pos.row-14;
	} else {				//KERNEL
		result *= map_pos.row-4;
	}
	result += map_pos.column - 8;
	return result;
}

void update_selected() {
	pixel_t s = {0, 32};
	pixel_t e = {0, 42};
	paint_c(s, e, (BG_BLUE | BRIGHT | CHAR_YELLOW), 32);
	char* t = "0x                \n";
	t += 2;
	int selected_page = get_selected_pos();
	char map = get_current_map();
	t[long_to_hex((long) (map ? index_to_dir_u(selected_page) : index_to_dir_k(selected_page)), t, 16)] = 10;
	t -= 2;
	print(s, t);
	s.column = 43;
	set_char(s, icon(get_owner(map, selected_page)));
	set_mode(s, get_type_page(map, selected_page));
	s.column = 25;
	e.column = 28;
	paint_c(s, e, (BG_BLUE | BRIGHT | CHAR_YELLOW), 32);
	char* i = "    \n";
	i[int_to_string((map ? selected_page+512 : selected_page), i, 4)] = 10;
	print(s, i);
}

void mmu_up() {
	if ((get_menu() == MENU_MMU) && (!get_focus())) {
		int selected_page = get_selected_pos();
		char map = get_current_map();
		char type = NO_TYPE;
		type = get_type_page(map, selected_page);
		set_mode(map_pos, type);
		if (map_pos.row == 4) {
			map_pos.row = 21;
			if (map_pos.column==8) map_pos.column = 71;
			else map_pos.column --;
		} else if (map_pos.row == 14) map_pos.row = 11; 
		else map_pos.row--;
		set_mode(map_pos, BG_RED | BLINK | CHAR_GREEN);
		print_type();
		update_selected();
	}
}

void mmu_down() {
	if ((get_menu() == MENU_MMU) && (!get_focus())) {
		int selected_page = get_selected_pos();
		char map = get_current_map();
		char type = NO_TYPE;
		type = get_type_page(map, selected_page);
		set_mode(map_pos, type);
		if (map_pos.row == 21) {
			map_pos.row = 4;
			if (map_pos.column==71) map_pos.column = 8;
			else map_pos.column ++;
		} else if (map_pos.row == 11) map_pos.row = 14;
		else map_pos.row++;
		set_mode(map_pos, BG_RED | BLINK | CHAR_GREEN);
		print_type();
		update_selected();
	}
}

void mmu_left() {
	if ((get_menu() == MENU_MMU) && (!get_focus())) {
		int selected_page = get_selected_pos();
		char map = get_current_map();
		char type = NO_TYPE;
		type = get_type_page(map, selected_page);
		set_mode(map_pos, type);
		if (map_pos.column > 8) map_pos.column--;
		else {
			map_pos.column = 71;
			if (map_pos.row==14) map_pos.row = 11;
			else if (map_pos.row==4) map_pos.row = 21;
			else map_pos.row--;
		}
		set_mode(map_pos, BG_RED | BLINK | CHAR_GREEN);
		print_type();
		update_selected();
	}
}

void mmu_right() {
	if ((get_menu() == MENU_MMU) && (!get_focus())) {
		int selected_page = get_selected_pos();
		char map = get_current_map();
		char type = NO_TYPE;
		type = get_type_page(map, selected_page);
		set_mode(map_pos, type);
		if (map_pos.column < 71) map_pos.column++;
		else {
			map_pos.column = 8;
			if (map_pos.row==11) map_pos.row = 14;
			else if (map_pos.row==21) map_pos.row = 4;
			else map_pos.row++;
		}
		set_mode(map_pos, BG_RED | BLINK | CHAR_GREEN);
		print_type();
		update_selected();
	}
}

void mmu_malloc() {
	int selected_page = get_selected_pos();
	char map = get_current_map();
	if (get_owner(map, selected_page) == NO_PID) {
		own_frame(map, selected_page, USR);
	}
}

void mmu_free() {
	int selected_page = get_selected_pos();
	char map = get_current_map();
	if ((get_owner(map, selected_page) != NO_PID) && (!map || selected_page)) {
		own_frame(map, selected_page, NO_PID);
	}
}

void mmu_owner_up() {
	char ok = 0;
	int selected_page = get_selected_pos();
	char map = get_current_map();
	int pid = 0;
	pid = get_owner(map, selected_page);
	if (pid != NO_PID) {
		if (pid < KERN) {
			pid ++;
		} else {
			if (pid == KERN) pid = NO_PID;
			pid++;
			for (; pid<NUMBER_OF_TASKS; pid++) {
				if (get_state(pid)!=EMPTY) {
					ok = 1;
					break;
				}
			}
			if (!ok) pid = USR;
		}
		own_frame(map, selected_page, pid);
	}
}

void mmu_owner_dn() {
	char ok = 0;
	int selected_page = get_selected_pos();
	char map = get_current_map();
	int pid = 0;
	pid = get_owner(map, selected_page);
	if (pid != NO_PID) {
		if (pid == KERN) {
			pid --;
		} else {
			if (pid == USR) pid = NUMBER_OF_TASKS;
			pid--;
			for (; pid>=0; pid--) {
				if (get_state(pid)!=EMPTY) {
					ok = 1;
					break;
				}
			}
			if (!ok) pid = KERN;
		}
		own_frame(map, selected_page, pid);
	}
}

void mmu_type_up() {
	int selected_page = get_selected_pos();
	char map = get_current_map();
	char t = get_type_page(map, selected_page);
	switch (t) {
		case HD:
			set_type_page(map, selected_page,SCRN);
			break;
		case SCRN:
			set_type_page(map, selected_page,TERM);
			break;
		case TERM:
			set_type_page(map, selected_page,PAGE);
			break;
		case PAGE:
			set_type_page(map, selected_page,STACK);
			break;
		case STACK:
			set_type_page(map, selected_page,OTHER);
			break;
		case OTHER:
			set_type_page(map, selected_page,HD);
			break;
	}
	print_type();
}

void mmu_type_dn() {
	int selected_page = get_selected_pos();
	char map = get_current_map();
	char t = get_type_page(map, selected_page);
	switch (t) {
		case HD:
			set_type_page(map, selected_page,OTHER);
			break;
		case SCRN:
			set_type_page(map, selected_page,HD);
			break;
		case TERM:
			set_type_page(map, selected_page,SCRN);
			break;
		case PAGE:
			set_type_page(map, selected_page,TERM);
			break;
		case STACK:
			set_type_page(map, selected_page,PAGE);
			break;
		case OTHER:
			set_type_page(map, selected_page,STACK);
			break;
	}
	print_type();
}

void paint_pos(char i) {
	unsigned char color = (i == 0 ? BG_WHITE | CHAR_GREEN : BG_RED | BLINK | CHAR_GREEN);
	set_mode(map_pos, color);
}

char get_current_map() {
	return map_pos.row > 12;
}

unsigned long index_to_dir_k(int i) {
	return KERN_PAGES_BASE + 0x1000 * i;
}

unsigned long index_to_dir_u(int i) {
	return USER_PAGES_BASE + 0x1000 * i;
}

pixel_t index_to_pos_k(int i) {
	pixel_t r = {4,8};
	r.row += i/64;
	r.column += i%64;
	return r;
}

pixel_t index_to_pos_u(int i) {
	pixel_t r = {14,8};
	r.row += i/64;
	r.column += i%64;
	return r;
}
