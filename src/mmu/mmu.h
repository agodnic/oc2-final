#ifndef __MMU_H__
#define __MMU_H__
#include "../constants.h"

/* The OS will support 25 * 80 pages in physical memory for users,
	and the same amount for kernel memory
   Main idea is to have two memory maps, one for user space and
   the other for kernel space.
   They will be queried by kmap and umap binaries


	0x000000	KERNEL CODE/DATA (512 frames)

	[0x20000, 0x21000)	4kb stack frame
	[0x21000, 0x22000)	PLM4 (level 4 page directory)
	[0x22000, 0x23000)	PDP  (level 3 page directory)
	[0x23000, 0x24000)	PD (level 2 page directory)
	[0x24000, 0x25000)	PT (page table)	
	[0x25000, 0x26000)	PT (page table)	
	[0x26000, 0x27000)	PT (page table)	
	--
	0x200000	KERNEL DYNAMIC MEM (512 frames)
	--
	0x401000	USER DYNAMIC MEM (512 frames)
	--
	0x600000	FREAKING SCARY MONSTERS BELOW HERE

   We will just give a malloc-style wrapper that
	returns any free page, or NULL if there are none.

*/

//    :)

//initialize memory management unit
void mmu_init();

//if virt_addr is zero then page virtual address is any
//returns NULL when no pages are available
unsigned long malloc_page_k(unsigned long virt_addr, int pid, char type);
void free_page_k(unsigned long virt_addr);

//if virt_addr is zero then page virtual address is any
//returns NULL when no pages are available
unsigned long malloc_page_u(unsigned long virt_addr, int pid, char type);
void free_page_u(unsigned long virt_addr, unsigned long cr3);

//Returns the physical address of the virtual address 'va'
//using the param cr3. Assumes VA pte exists
unsigned long va2phy(unsigned long va, unsigned long cr3);

/*
	FIND A WAY TO JOIN IA32E_PAGING_INIT and user dir creation
		or give proof that's not possible


   CR3 could be an implicit parameter, if we call malloc in the
	context of a task, CR3 will have an obvious value.

   But we will have problems creating a new page directory,
	because we need to specify which PLM4 to work on
 */

//could this replace ia32e_paging_init?
unsigned long init_user_page_dir(int pid);
void free_user_page_dir(int pid);

/*Similar to malloc but:
1. phy page is a param instead of taking the first free frame
2. it does not map/unmap the frame*/
void own_frame(char m, int i, int new_pid);

int get_owner(char m, int i);
void set_type_page(char m, int i, char type);
char get_type_page(char m, int i);

#endif
