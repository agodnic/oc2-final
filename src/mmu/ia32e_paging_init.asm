ADDR_PAGEDIR_LV4	equ     0x21000         ;PLM4 (Kernel CR3)
ADDR_PAGEDIR_LV3	equ     0x22000         ;PDP
ADDR_PAGEDIR_LV2	equ     0x23000         ;PD
ADDR_PAGETABLE1		equ     0x24000         ;PT
ADDR_PAGETABLE2		equ     0x25000         ;PT2
ADDR_PAGETABLE3		equ     0x26000         ;PT3

PAGE_PRESENT		equ	1
PAGE_READWRITE		equ	2

BITS 32
global ia32e_paging_init
ia32e_paging_init:

	;cargar la base de PML4
	mov	eax, ADDR_PAGEDIR_LV4
	mov	cr3, eax
	
	;cerear las 4 tablas de una sola pasada
	mov	edi, ADDR_PAGEDIR_LV4
	mov	ecx, 4000h >> 2
	xor	eax, eax
	rep	stosd			

	;setear las entradas necesarias en la PDP,PD y PT (privilegios de supervisor 0)
	mov	dword [ADDR_PAGEDIR_LV4], ADDR_PAGEDIR_LV3 | PAGE_PRESENT | PAGE_READWRITE	; PD pointer
	mov	dword [ADDR_PAGEDIR_LV3], ADDR_PAGEDIR_LV2 | PAGE_PRESENT | PAGE_READWRITE	; PD

	mov	dword [ADDR_PAGEDIR_LV2], ADDR_PAGETABLE1  | PAGE_PRESENT | PAGE_READWRITE	; PT1
	mov	dword [ADDR_PAGEDIR_LV2+8], ADDR_PAGETABLE2  | PAGE_PRESENT | PAGE_READWRITE	; PT2
	mov	dword [ADDR_PAGEDIR_LV2+16], ADDR_PAGETABLE3  | PAGE_PRESENT | PAGE_READWRITE	; PT3

	;mapear 3 * 512 paginas con identity mapping (tres page tables)
	mov	edi, ADDR_PAGETABLE1		; address of first page table
	mov	eax, 0 | PAGE_PRESENT | PAGE_READWRITE
	mov	ecx, 3 * 512 			; number of pages to map (2 MB)

make_page_entries:
	stosd
	add	edi,4
	add	eax, 1000h
	loop	make_page_entries

	;We dont make any empty entries for PT 3
	;  because we assume memory starts zero-initialized
	;  that is true for bochs
	;Otherwise we should be zeroing PT3

	ret
