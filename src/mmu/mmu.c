#include "mmu.h"

#include "../hw/ia32e.h"
#include "../ansi/string.h"
#include "../screen/screen.h"
#include "../sched/sched.h"
#include "../constants.h"

//may be expanded in the future
typedef struct page_status {
	int pid;
	char type;
} page_status_t;

//user and kernel memory maps
page_status_t kern_map[NUMBER_OF_PAGES];
page_status_t user_map[NUMBER_OF_PAGES];

//virtual address page table indexes
typedef struct ia32e_virt_addr_indexes {
	unsigned int offset  : 12;
	unsigned int pt      :  9;
	unsigned int pd2     :  9;
	unsigned int pd3     :  9;
	unsigned int pd4     :  9;
	unsigned int pad     : 25;
} __attribute__ ((__packed__)) ia32e_virt_addr_indexes_t;

typedef union ia32e_virt_addr {
	unsigned long addr;
	ia32e_virt_addr_indexes_t indexes;
} ia32e_virt_addr_t; 

//TODO should return an union of a structure and an unsigned long
/*typedef union page_table_entry {
	unsigned long qword;
	struct {
	} 
} page_table_entry_t;
*/
/*TODO do something about that horrible bitmask
	a union of a struct and a bitfield could be used:
	union zarlanga 
		long addr
		struct{
			table_addr
			readwrite
			present
		}
*/
//TODO check for table values other than page present,
	//table creation on the fly logic may be stolen from the old project


//set memory maps as totally free
//TODO static free_map could be needed if mmu_init grows too large
void mmu_init() {

	int i = 0;
	for(i = 0; i < NUMBER_OF_PAGES; i++) {
		kern_map[i].pid = NO_PID;
		user_map[i].pid = NO_PID;
		kern_map[i].type = NO_TYPE;
		user_map[i].type = NO_TYPE;
	}
	user_map[0].pid = -65;
	user_map[0].type = 0;
}

//requests a physical page frame to the memory management unit
//TODO remove duplicated code on *_frame functions
static unsigned long req_frame(char m, int pid, char type) {
	
	page_status_t* map = (m ? user_map : kern_map);
	unsigned long base = (m ? USER_PAGES_BASE : KERN_PAGES_BASE);
		
	int i = 0;
	for(i = 0; i < NUMBER_OF_PAGES; i++) {
		if (map[i].pid == NO_PID){
			map[i].pid = pid;
			map[i].type = type;
			update_screen_malloc(m, i, pid, type);
			return ( base + (PAGE_SIZE*i) );
		}
	}
	return NULL;
}

//reports a physical page frame as free to the memory management unit
//FIXME assumes physical address in range
static void free_frame(char m, unsigned long phy_addr) {
	
	page_status_t* map = (m ? user_map : kern_map);
	unsigned long base = (m ? USER_PAGES_BASE : KERN_PAGES_BASE);

	int i = (phy_addr - base) / PAGE_SIZE;

	map[i].pid = NO_PID;
	map[i].type = NO_TYPE;
	update_screen_free(m, i);
}


//search for an entry in page directory pointers table
//FIXME assumes pte exists
static unsigned long* search_pte(unsigned long virt_addr, unsigned long cr3) {

	ia32e_virt_addr_t va = {virt_addr};

	//index all tables, assume entries exist until lv1
	unsigned long* pd4 = (unsigned long*) (cr3 & 0xffffffffff000);
	unsigned long* pd3 = (unsigned long*) (pd4[va.indexes.pd4] & 0xffffffffff000);
	unsigned long* pd2 = (unsigned long*) (pd3[va.indexes.pd3] & 0xffffffffff000);
	unsigned long* pt  = (unsigned long*) (pd2[va.indexes.pd2] & 0xffffffffff000);

	
	unsigned long* pte = (unsigned long*) &pt[va.indexes.pt];
	return pte;
} 


//get pte and edit phy addr, then mark page as present and rw
//FIXME assumes pte exists
static void map_page(unsigned long virt_addr, unsigned long phy_addr, unsigned long cr3) {
	unsigned long* pte = search_pte(virt_addr, cr3);
	*pte = phy_addr;
	*pte |= PAGE_READWRITE | PAGE_PRESENT;
}

//get pte and mark entry as not present
//FIXME assumes pte exists, otherwise crashes
static unsigned long unmap_page(unsigned long virt_addr, unsigned long cr3) {
	unsigned long* pte = search_pte(virt_addr, cr3);
	*pte &= ~((unsigned long) PAGE_PRESENT);
	flush_tlb();
	return (unsigned long) (*pte & 0xffffffffff000);
}


//if virt_addr is zero then page virtual address is any
//returns NULL when no pages are available
//atm virt_addr must be between (0,0x600000)
unsigned long malloc_page(char m, unsigned long virt_addr, int pid, char type) {

	unsigned long frame = req_frame(m, pid, type);
	if (frame == NULL) return NULL;

	virt_addr = virt_addr ? virt_addr : frame;
	if (pid != USR) map_page(virt_addr, frame, (m ? get_cr3(pid) : KERNEL_CR3));

	return virt_addr;
}

unsigned long malloc_page_k(unsigned long virt_addr, int pid, char type) { return malloc_page(KERNEL, virt_addr, pid, type);}
unsigned long malloc_page_u(unsigned long virt_addr, int pid, char type) { return malloc_page(USER, virt_addr, pid, type);}


//FIXME assumes VA pte exists
void free_page(char m, unsigned long virt_addr, unsigned long cr3) {
	unsigned long phy_addr = unmap_page(virt_addr, cr3);
	free_frame(m, phy_addr);
}

void free_page_k(unsigned long virt_addr) {free_page(KERNEL, virt_addr, KERNEL_CR3);}
void free_page_u(unsigned long virt_addr, unsigned long cr3) {free_page(USER, virt_addr, cr3);}

unsigned long va2phy(unsigned long va, unsigned long cr3) {
	unsigned long phy = (*search_pte(va, cr3) & 0xfffffffffffff000);
	unsigned long offset = (va & 0x0000000000000fff);
	return (phy | offset);
}

unsigned long init_user_page_dir(int pid) {
	unsigned long*	p4  = (unsigned long*) malloc_page(KERNEL, NULL, pid, PAGE);
	memset(p4, 0, PAGE_SIZE);
	unsigned long*	p3  = (unsigned long*) malloc_page(KERNEL, NULL, pid, PAGE);
	memset(p3, 0, PAGE_SIZE);
	unsigned long*	p2  = (unsigned long*) malloc_page(KERNEL, NULL, pid, PAGE);
	memset(p2, 0, PAGE_SIZE);
	unsigned long*	pt3 = (unsigned long*) malloc_page(KERNEL, NULL, pid, PAGE);
	memset(pt3, 0, PAGE_SIZE);

	p4[0] = ((unsigned long) p3) | PAGE_PRESENT | PAGE_READWRITE;

	p3[0] = ((unsigned long) p2) | PAGE_PRESENT | PAGE_READWRITE;

	p2[0] = ((unsigned long) 0x24000) | PAGE_PRESENT | PAGE_READWRITE;
	p2[1] = ((unsigned long) 0x25000) | PAGE_PRESENT | PAGE_READWRITE;
	p2[2] = ((unsigned long) pt3) | PAGE_PRESENT | PAGE_READWRITE;

	return (unsigned long) p4;
}

void free_user_page_dir(int pid) {
	int i=0;
	for(i = 0; i < NUMBER_OF_PAGES; i++) {
		if (user_map[i].pid == pid) {
			user_map[i].pid = NO_PID;
			user_map[i].type = NO_TYPE;
			update_screen_free(USER, i);
		}
	}
	unsigned long cr3 = get_cr3(pid);
	unsigned long* p4 = (unsigned long*) cr3;
	unsigned long* p3 = (unsigned long*) (p4[0] & 0xfffffffffffff000);
	unsigned long* p2 = (unsigned long*) (p3[0] & 0xfffffffffffff000);
	unsigned long* pt3 = (unsigned long*) (p2[2] & 0xfffffffffffff000);
	free_page(KERNEL, (unsigned long) pt3, KERNEL_CR3);
	free_page(KERNEL, (unsigned long) p2, KERNEL_CR3);
	free_page(KERNEL, (unsigned long) p3, KERNEL_CR3);
	free_page(KERNEL, (unsigned long) p4, KERNEL_CR3);
}

void own_frame(char m, int i, int new_pid) {
	page_status_t* map = (m ? user_map : kern_map);
	if (map[i].pid == NO_PID) map[i].type = OTHER;
	map[i].pid = new_pid;
	if (new_pid == NO_PID) {
		map[i].type = NO_TYPE;
		update_screen_free(m, i);
	} else {
		update_screen_malloc(m, i, new_pid, map[i].type);
	}
}

int get_owner(char m, int i) {
	page_status_t* map = (m ? user_map : kern_map);
	return map[i].pid;
}

void set_type_page(char m, int i, char type) {
	page_status_t* map = (m ? user_map : kern_map);
	map[i].type = type;
	update_screen_malloc(m, i, map[i].pid, type);
}

char get_type_page(char m, int i) {
	return (m ? user_map[i].type : kern_map[i].type);
}
