#include "gdt.h"

gdt_entry_t gdt[] = {
	/* entrada 0: Descriptor nulo*/
	(gdt_entry_t){ 
		(unsigned short) 0x0000, 
		(unsigned short) 0x0000,
		(unsigned char) 0x00, 
		(unsigned char) 0x0, 
		(unsigned char) 0, 
		(unsigned char) 0, 
		(unsigned char) 0, 
		(unsigned char) 0x0,
		(unsigned char) 0,  
		(unsigned char) 0,  
		(unsigned char) 0,  
		(unsigned char) 0, 
		(unsigned char) 0x00 
	},
	/* entrada 1: Descriptor de la memoria de Video (32 bits)*/
	(gdt_entry_t){
		(unsigned short) 0x0000,	//limit_0_15
		(unsigned short) 0x0000,	//base_0_15
		(unsigned char) 0x0A, 		//base_23_16
		(unsigned char) 0x2, 		//type (RW)
		(unsigned char) 1, 		//s
		(unsigned char) 0, 		//dpl Tiene que ser 0
		(unsigned char) 1,	 	//p
		(unsigned char) 0x2,		//limit_16_19
		(unsigned char) 0,  		//avl
		(unsigned char) 0,  		//l
		(unsigned char) 1,  		//db	(operaciones en dw)
		(unsigned char) 0, 		//g (bytes)
		(unsigned char) 0x00 		//base_31_24
	},
	/* entrada 2: Descriptor de la memoria de Video (64 bits)*/
	(gdt_entry_t){ 
		(unsigned short) 0x0000,	//limit_0_15
		(unsigned short) 0x0000,	//base_0_15
		(unsigned char) 0x0A, 		//base_23_16
		(unsigned char) 0x2, 		//type (RW)
		(unsigned char) 1, 		//s
		(unsigned char) 0, 		//dpl Tiene que ser 0
		(unsigned char) 1,	 	//p
		(unsigned char) 0x2,		//limit_16_19
		(unsigned char) 0,  		//avl
		(unsigned char) 1,  		//l		(operaciones en qw)
		(unsigned char) 0,  		//db
		(unsigned char) 0, 		//g (bytes)
		(unsigned char) 0x00 		//base_31_24
	},
	/* entrada 3: Descriptor de Segmento de datos (64 bits)*/
	(gdt_entry_t){ 
 		(unsigned short) 0xFFFF,	//limit_0_15
		(unsigned short) 0x0000,	//base_0_15
		(unsigned char) 0x00, 		//base_23_16
		(unsigned char) 0x2, 		//type (RW)
		(unsigned char) 1, 		//s
		(unsigned char) 0, 		//dpl Tiene que ser 0
		(unsigned char) 1,		//p
		(unsigned char) 0xF,		//limit_16_19
		(unsigned char) 0,  		//avl
		(unsigned char) 1,  		//l		(operaciones en qw)
		(unsigned char) 0,  		//db
		(unsigned char) 1, 		//g (4KBs)
		(unsigned char) 0x00 		//base_31_24
	},
	/* entrada 4: Descriptor de Segmento de datos (32 bits)*/
	(gdt_entry_t){ 
		(unsigned short) 0xFFFF,	//limit_0_15
		(unsigned short) 0x0000,	//base_0_15
		(unsigned char) 0x00, 		//base_23_16
		(unsigned char) 0x2, 		//type (RW)
		(unsigned char) 1, 		//s
		(unsigned char) 0, 		//dpl Tiene que ser 0
		(unsigned char) 1,		//p
		(unsigned char) 0xF,		//limit_16_19
		(unsigned char) 0,  		//avl
		(unsigned char) 0,  		//l
		(unsigned char) 1,  		//db	(operaciones en dw)
		(unsigned char) 1, 		//g (4KBs)
		(unsigned char) 0x00 		//base_31_24
	},
	/* entrada 5: Descriptor de Segmento de codigo (64 bits)*/
	(gdt_entry_t){ 
		(unsigned short) 0xFFFF,	//limit_0_15
		(unsigned short) 0x0000,	//base_0_15
		(unsigned char) 0x00, 		//base_23_16
		(unsigned char) 0xA, 		//type (X/R)
		(unsigned char) 1, 		//s
		(unsigned char) 0, 		//dpl Tiene que ser 0
		(unsigned char) 1,		//p
		(unsigned char) 0xF,		//limit_16_19
		(unsigned char) 0,  		//avl
		(unsigned char) 1,  		//l		(operaciones en qw)
		(unsigned char) 0,  		//db
		(unsigned char) 1, 		//g (4KBs)
		(unsigned char) 0x00 		//base_31_24
	},
	/* entrada 6: Descriptor de Segmento de codigo (32 bits)*/
	(gdt_entry_t){
		(unsigned short) 0xFFFF,	//limit_0_15
		(unsigned short) 0x0000,	//base_0_15
		(unsigned char) 0x00, 		//base_23_16
		(unsigned char) 0xA, 		//type (X/R)
		(unsigned char) 1, 		//s
		(unsigned char) 0, 		//dpl Tiene que ser 0
		(unsigned char) 1,		//p
		(unsigned char) 0xF,		//limit_16_19
		(unsigned char) 0,  		//avl
		(unsigned char) 0,  		//l
		(unsigned char) 1,  		//db	(operaciones en dw)
		(unsigned char) 1, 		//g (4KBs)
		(unsigned char) 0x00 		//base_31_24
	}
};

gdt_register_t gdt_register = {sizeof(gdt)-1, (unsigned long) &gdt};
