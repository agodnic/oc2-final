#ifndef __GDT_H__
#define __GDT_H__

typedef struct gdt_register {
	unsigned short gdt_length;
	unsigned long gdt_addr;
} __attribute__((__packed__)) gdt_register_t;

typedef struct gdt_entry {
	unsigned short limit_0_15;
	unsigned short base_0_15;
	unsigned char base_23_16;
	unsigned char type:4;
	unsigned char s:1;
	unsigned char dpl:2;
	unsigned char p:1;
	unsigned char limit_16_19:4;
	unsigned char avl:1;
	unsigned char l:1;
	unsigned char db:1;
	unsigned char g:1;
	unsigned char base_31_24;
} __attribute__((__packed__, aligned (8))) gdt_entry_t;



extern gdt_entry_t gdt[];//__attribute__ ((section ("gdtdata")));

extern gdt_register_t gdt_register;//__attribute__ ((section ("gdtdata")));

#endif //__GDT_H__
