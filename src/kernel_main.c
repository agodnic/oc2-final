#include "hw/ia32e.h"

#include "cui/cui.h"
#include "screen/screen.h"
#include "mmu/mmu.h"
#include "isr/isr.h"
#include "term/term.h"
#include "hw/pic.h"
#include "sched/sched.h"
#include "fat/fat12.h"

#include "cui/dic.h"

//main kernel code
void kernel_main(){

	//initialize PIC
	resetear_pic();
	habilitar_pic();
	//initialize memory management unit
	mmu_init();
	//initialize screen buffers
	screen_init();
	//initialize hd driver
	fat12_init();
	//initialize scheduler estructures
	sched_init();
	//initialize terminal messages
	terminal_init();

	//begin
	run_task(load_task_q("/BIN/IDLE.TSK\n",1));
	clear_screen();
	toolbar();
	set_menu(1);
	draw_terminal();
}
