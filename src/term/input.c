#include "input.h"

#include "../cui/cui.h"	//Para set_mode y set_char
#include "term.h"		//Para send_message
#include "../mmu/mmu.h"
#include "comandos.h"
#include "../constants.h"
#include "../hw/ia32e.h"

unsigned char input_pointer = 0;
unsigned char last_pointer = 0;
unsigned char is_insert = 0;

unsigned char input_mode = BG_YELLOW | BLINK | CHAR_BLUE;

void input_char(unsigned char c) {
	pixel_t p = {23,last_pointer+3};
	pixel_t p2 = {23,last_pointer+2};
	if ((last_pointer == 0) && (get_char(p2) != 0xDB)) set_char(p2, 0xDB);
	if ((!is_insert) && (last_pointer < 75)) {
		while (p.column-2 > input_pointer) {
			set_char(p, get_char(p2));
			p.column--;
			p2.column--;
		}
		p2.column = input_pointer+2;
		set_mode(p2, BG_YELLOW | CHAR_BLACK);
		set_char(p2, c);
		p2.column ++;
		set_mode(p2, input_mode);
		p.column = last_pointer+3;
		if (last_pointer == 74) {
			set_mode(p, BG_YELLOW | BLINK | CHAR_RED);
			set_char(p, 0x19);
		}
		last_pointer++;
		input_pointer++;
	} else
	if (is_insert) {
		if (input_pointer == last_pointer) {
			if (last_pointer < 75) {
				p.column = input_pointer+2;
				set_char(p, c);
				set_mode(p, BG_YELLOW | CHAR_BLACK);
				p.column++;
				if (last_pointer == 74) {
					set_mode(p, BG_YELLOW | BLINK | CHAR_RED);
					set_char(p, 0x19);
				} else {
					set_mode(p, input_mode);
					set_char(p, 0xDB);
				}
				last_pointer++;
				input_pointer++;
			}
		} else {
			p.column = input_pointer+2;
			set_char(p, c);
		}
	}
}

void input_back() {
	pixel_t p = {23, input_pointer+1};
	pixel_t p2 = p;
	if (input_pointer > 0) {
		while (p.column-2 <= last_pointer) {
			p2.column = p.column+1;
			set_mode(p, BG_YELLOW | CHAR_BLACK);
			if (p.column < 76) set_char(p, get_char(p2));
			else set_char(p, 32);
			p.column++;
		}
		p.column = input_pointer+1;
		if (input_pointer == 75) set_char(p, 0xDB);
		set_mode(p, input_mode);
		input_pointer--;
		last_pointer--;
	}
}

void input_right() {
	pixel_t p = {23, input_pointer+2};
	if (input_pointer < last_pointer) {
		set_mode(p, BG_YELLOW | CHAR_BLACK);
		p.column++;
		input_pointer++;
		if (input_pointer != 75) {
			set_mode(p, input_mode);
			if (input_pointer == last_pointer) set_char(p, 0xDB);
		}
	}
}

void input_left() {
	pixel_t p = {23, input_pointer+2};
	if (input_pointer > 0) {
		if (input_pointer != 75) {
			set_mode(p, BG_YELLOW | CHAR_BLACK);
			if (input_pointer == last_pointer) set_char(p, 32);
		}
		p.column--;
		input_pointer--;
		set_mode(p, input_mode);
	}
}

void input_home() {
	if (input_pointer !=0 ) {
		pixel_t p = {23, input_pointer+2};
		if (input_pointer != 75) {
			set_mode(p, BG_YELLOW | CHAR_BLACK);
			if (input_pointer == last_pointer) set_char(p, 32);
		}
		p.column = 2;
		set_mode(p, input_mode);
		input_pointer = 0;
	}
}

void input_end() {
	if ((input_pointer != 75) && (last_pointer != 0)) {
		pixel_t p = {23, input_pointer+2};
		set_mode(p, BG_YELLOW | CHAR_BLACK);
		if (input_pointer == last_pointer) set_char(p, 32);
		p.column = last_pointer+2;
		if (last_pointer != 75) {
			set_mode(p, input_mode);
			set_char(p, 0xDB);
		}
		input_pointer = last_pointer;		
	}
}

void input_delete() {
	if (input_pointer < last_pointer) {
		input_pointer++;
		input_back();
	}
}

void input_insert() {
	pixel_t p = {23, input_pointer+2};
	if (is_insert) {
		input_mode = BG_YELLOW | BLINK | CHAR_BLUE;
		set_mode((pixel_t) {24,78}, BG_RED | BRIGHT | CHAR_BLACK);
	}
	else {
		input_mode = BG_BLACK | BLINK | BRIGHT | CHAR_YELLOW;
		set_mode((pixel_t) {24,78}, BG_RED | BRIGHT | CHAR_YELLOW);
	}
	is_insert = !is_insert;
	if ((input_pointer != 75) && (get_char((pixel_t){23,2}) != (char) 0xAF)) {
		set_mode(p, input_mode);
	}
}

void input_enter() {
	char tmp_c[78];
	tmp_c[0] = 32;
	input_pointer = 0;
	pixel_t p = {23, 2};
	last_pointer+=2;
	for(; p.column < last_pointer; p.column++) {
		tmp_c[p.column-1] = get_char(p);
	}
	tmp_c[p.column-1] = 10;
	send_message(USR,tmp_c);
	if (last_pointer == 2) {
		clear_term();
	} else {
		search_command(tmp_c);
	}
	clear_input();
}

void clear_input() {
	last_pointer = 0;
	pixel_t p = {23, 2};
	pixel_t q = {23, 77};
	paint_c(p,q,BG_YELLOW | CHAR_BLACK,32);
	set_mode(p, BG_YELLOW | BLINK | CHAR_RED);
	set_char(p, 0xAF);
}

void paint_pointer() {
	pixel_t p = {23, input_pointer+2};
	if ((input_pointer == 75) || (get_char((pixel_t){23,2}) == (char) 0xAF))
		set_mode(p, BG_YELLOW | BLINK | CHAR_RED);
	else
		set_mode(p, input_mode);
}
