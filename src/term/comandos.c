#include "comandos.h"

#include "../cui/cui.h"
#include "../cui/dic.h"
#include "../cui/clock.h"
#include "../cui/key.h"
#include "../ansi/string.h"
#include "../hw/ia32e.h"
#include "../screen/screen.h"
#include "../screen/taskscrn.h"
#include "../screen/mmuscrn.h"
#include "../isr/isr.h"
#include "../mmu/mmu.h"
#include "../sched/sched.h"
#include "term.h"
#include "../constants.h"

#define K_HELP 6
char* help[] = {
	" * LIST        - Lista de comandos general\n",
	" * LIST P      - Lista de comandos de pantalla\n",
	" * LIST D      - Lista de comandos de Debug\n",
	" * LIST M      - Lista de comandos de memoria\n",
	" * LIST S      - Lista de comandos del scheduler\n",
	" * LIST T      - Lista de comandos de tareas\n"
	};

#define K_HELP_P 5
char* helpp[] = {
	" * TERM        - Mostrar al Terminal\n",
	" * SCHED       - Mostrar el Scheduler\n",
	" * MMU         - Mostrar el Mapa de Memoria\n",
	" * REG         - Mostrar los Registros\n",
	" * TSK <t>     - Mostrar la pantalla de la tarea <t> (pid)\n"
	};

#define K_HELP_D 4
char* helpd[] = {
	" * INT <i>     - Interrupcion voluntaria de tipo <i>\n",
	" * WAIT <t>    - Detener el procesador durante <t> ciclos\n",
	" * CLEAR       - Borrar el registro de la terminal\n",
	" * USER <s>    - Cambiar el nombre de usuario por <s>\n"
	};

#define K_HELP_M 6
char* helpm[] = {
	" * MALL-K      - Reservar una pagina de memoria del kernel\n",
	" * MALL-K <t>  - Reservar una pagina de memoria del kernel para la tarea <t>\n",
	" * MALL-U      - Reservar una pagina de memoria de usuario\n",
	" * MALL-U <t>  - Reservar una pagina de memoria de usuario para la tarea <t>\n",
	" * FREE   <p>  - Liberar la pagina de ususario <p> (indice en la MMU)\n",
	" * FREE   <p>  - Liberar la pagina de ususario <p> (direccion fisica)\n"
	};

#define K_HELP_T 3
char* helpt[] = {
	" * TSK <t>     - Mostrar la pantalla de la tarea <t> (pid)\n",
	" * CAT <f>     - Ejecutar CAT con el archivo <f> (ruta del archivo)\n",
	" * VIM <f> <s> - Ejecutar VIM con <f> (ruta del archivo) y la cadena <s>\n"
	};

#define K_HELP_S 9
char* helps[] = {
	" * LOAD <f>    - Crear una tarea a partir de binario <f> (ruta del archivo)\n",
	" * RUN <t>     - Iniciar la ejecucion de la tarea <t> (pid)\n",
	" * START       - Iniciar la ejecucion de todas las tareas cargadas\n",
	" * EXEC <f>    - Ejecutar el binario <f> (ruta del archivo)\n",
	" * PLAY <f>    - Ejecutar y mostrar el binario <f> (ruta del archivo)\n",
	" * KILL <t>    - Eliminar la tarea <t> (pid)\n",
	" * Q-UP <t>    - Incrementar el quantum de la tarea <t> (pid)\n",
	" * Q-DN <t>    - Decrementar el quantum de la tarea <t> (pid)\n",
	" * QUAN <t> <q>- Setear el quantum de la tarea <t> (pid) en <q> ticks\n"
	};

char* cont = " Presione ESC para continuar\n";

char* err_no_path = " Error: Debe ingresar la ruta del binario\n";
char* err_max_task = " Error: Ya no se pueden cargar mas tareas\n";
char* err_file_not_found = " Error: No se encuentra el binario\n";
char* err_bad_pid = " Error: parametro ivalido. Debe pertencer a [0,__]\n";
char* err_null_pid = " Error: no hay ninguna tarea con ese pid\n";
char* err_no_pid = " Error: Debe ingresar el pid de la tarea\n";
char* err_idle = " Error: no se puede cambiar el quantum de la tarea Idle\n";

extern char command_int;

void command_init() {
	int_to_string(NUMBER_OF_TASKS, err_bad_pid+47, 2);
}

int commandcmp(char* c, char* m);

void c_int(char* params, int first) {
	char* c = " Comando reconocido: Interrupcion voluntaria\n";
	send_message(KERN, c);
	int i = 0;
	if (params[first] == 32) {
		params += first;
		params++;
		i = string_to_int(params);
	}
	if ((i<0) || (i>19) || (i==1) || (i==9) || (i==15)) {
		char* e = " Error: parametro invalido. Debe pertenecer a [0,19] - {1,9,15}\n";
		add_line(e);
	} else {
		if (get_menu() == 1) {
			add_line(cont);
			wait_for_key(KEY_ESC);
		}
		command_int = 1;
		interrupt( i );
	}
}

void c_wait(char* params, int first) {
	char* c = " Comando reconocido: Detener el procesador\n";
	send_message(KERN, c);
	int t = 30;
	if (params[first] == 32) {
		params += first;
		params++;
		t = string_to_int(params);
	}
	if (t < 0) {
		char* e = " Error: parametro invalido. Debe ser positivo o cero\n";
		add_line(e);
	} else
		wait(t);
}

void c_term(char* params, int first) {
	char* c = " Comando reconocido: Mostrar Terminal\n";
	send_message(KERN, c);
	if (get_menu() != 1) {
		set_menu(1);
		terminal_view();
	}
}

void c_mmu(char* params, int first) {
	char* c = " Comando reconocido: Mostrar Mapa de Memoria\n";
	send_message(KERN, c);
	if (get_menu() == 1) {
		add_line(cont);
		wait_for_key(KEY_ESC);
	}
	if (get_menu() != 5) {
		set_menu(5);
		mmu_screen();
	}
}

void c_help(char* params, int first) {
	int i = 10;
	if (params[first] == 32) {
		i = params[first+1];
	}
	if (i==10) {
		char* c = " Comando reconocido: Lista de comandos general\n";
		send_message(KERN, c);
		for (i=0; i < K_HELP; i++) {
			add_line(help[i]);
		}
	} else if (i=='P') {
		char* c = " Comando reconocido: Lista de comandos de pantalla\n";
		send_message(KERN, c);
		for (i=0; i < K_HELP_P; i++) {
			add_line(helpp[i]);
		}
	} else if (i=='D') {
		char* c = " Comando reconocido: Lista de comandos de Debug\n";
		send_message(KERN, c);
		for (i=0; i < K_HELP_D; i++) {
			add_line(helpd[i]);
		}
	} else if (i=='M') {
		char* c = " Comando reconocido: Lista de comandos de memoria\n";
		send_message(KERN, c);
		for (i=0; i < K_HELP_M; i++) {
			add_line(helpm[i]);
		}
	} else if (i=='T') {
		char* c = " Comando reconocido: Lista de comandos de tareas\n";
		send_message(KERN, c);
		for (i=0; i < K_HELP_T; i++) {
			add_line(helpt[i]);
		}
	} else if (i=='S') {
		char* c = " Comando reconocido: Lista de comandos del scheduler\n";
		send_message(KERN, c);
		for (i=0; i < K_HELP_S; i++) {
			add_line(helps[i]);
		}
	} else {
		char* c = " Comando reconocido: Lista de comandos\n";
		send_message(KERN, c);
		char* e = " Error: parametro invalido. Debe ser alguno de los siguientes: P, D, M, T, S\n";
		add_line(e);
	}
}

void c_clear(char* params, int first) {
	clear_term();
}

void c_user(char* params, int first) {
	char* c = " Comando reconocido: Cambiar nombre de usuario\n";
	send_message(KERN, c);
	if ((params[first] == 10) || (params[first+1] == 10)) {
		char* e = " Error: Debe ingresar una cadena\n";
		add_line(e);
	} else {
		params+=first;
		params++;
		rename_user(params);	
	}
}

void c_load(char* params, int first) {
	char* c = " Comando reconocido: Cargar una tarea\n";
	send_message(KERN, c);
	if ((params[first] == 10) || (params[first+1] == 10)) {
		add_line(err_no_path);
	} else {
		params+=first;
		params++;
		int r = load_task(params);
		if (r == -1) {
			add_line(err_max_task);
		} else if (r == 0) {
			add_line(err_file_not_found);
		} else {
			char* e = " La tarea se ha cargado con el pid: __\n";
			e[36] = (r/10) + 48;
			e[37] = (r%10) + 48;
			add_line(e);
			update_screen_load(r);
		}
	}
}

void c_exec(char* params, int first) {
	char* c = " Comando reconocido: Ejeutar un binario\n";
	send_message(KERN, c);
	if ((params[first] == 10) || (params[first+1] == 10)) {
		add_line(err_no_path);
	} else {
		params+=first;
		params++;
		int r = load_task(params);
		if (r == -1) {
			add_line(err_max_task);
		} else if (r == 0) {
			add_line(err_file_not_found);
		} else {
			char* e = " La tarea se ha cargado con el pid: __\n";
			e[36] = (r/10) + 48;
			e[37] = (r%10) + 48;
			add_line(e);
			update_screen_load(r);
			run_task(r);
			update_screen_run(r);
		}
	}
}

void c_run(char* params, int first) {
	char* c = " Comando reconocido: Iniciar ejecucion\n";
	send_message(KERN, c);
	int i = 0;
	if (params[first] == 32) {
		params += first;
		params++;
		i = string_to_int(params);
		if ((i < 0) || (i > NUMBER_OF_TASKS)) {
			add_line(err_bad_pid);
		} else if (get_state(i) == EMPTY) {
			add_line(err_null_pid);
		} else if (get_state(i) == LOADED) {
			run_task(i);
			update_screen_run(i);
		} else {
			char* e = " Error: La tarea ya esta corriendo\n";
			add_line(e);
		}
	} else {
		add_line(err_no_pid);
	}
}

void c_start(char* params, int first) {
	char* c = " Comando reconocido: Iniciar ejecucion\n";
	send_message(KERN, c);
	int i = 0;
	for (; i < NUMBER_OF_TASKS; i++) {
		if (get_state(i) == LOADED) {
			run_task(i);
			update_screen_run(i);
		}
	}
}

void c_reg(char* params, int first) {
	char* c = " Comando reconocido: Mostrar los Registros\n";
	send_message(KERN, c);
	if (get_menu() == 1) {
		add_line(cont);
		wait_for_key(KEY_ESC);
	}
	if (get_menu() != 4) {
		set_menu(4);
		print_registers(0);
	}
}

//TODO
void c_browse(char* params, int first) {
	char* c = " Comando reconocido:\n";
	send_message(KERN, c);
	if (get_menu() == 1) {
		add_line(cont);
		wait_for_key(KEY_ESC);
	}
}

void c_sched(char* params, int first) {
	char* c = " Comando reconocido: Mostrar el Scheduler\n";
	send_message(KERN, c);
	if (get_menu() == 1) {
		add_line(cont);
		wait_for_key(KEY_ESC);
	}
	if (get_menu() != 2) {
		set_menu(2);
		sched_screen();
	}
}

void tsk(int i) {
	if (get_menu() == 1) {
		add_line(cont);
		wait_for_key(KEY_ESC);
	}
	if (get_menu() != 6+i) {
		task_scrn_view(i);
	}
}
void c_tsk(char* params, int first) {
	char* c = " Comando reconocido: Mostrar la pantalla de una tarea\n";
	send_message(KERN, c);
	int i = 0;
	if ((params[first] == 32) && (params[first+1] != 10)) {
		params += first;
		params++;
		i = string_to_int(params);
		if ((i < 0) || (i > NUMBER_OF_TASKS)) {
			add_line(err_bad_pid);
		} else if (get_state(i) == EMPTY) {
			add_line(err_null_pid);
		} else tsk(i);
	} else tsk(i);
}

void c_kill(char* params, int first) {
	char* c = " Comando reconocido: Eliminar una tarea\n";
	send_message(KERN, c);
	int i = 0;
	if (params[first] == 32) {
		params += first;
		params++;
		i = string_to_int(params);
		if ((i < 0) || (i > NUMBER_OF_TASKS)) {
			add_line(err_bad_pid);
		} else if (i == 0) {
			char* e = " Error: no se puede eliminar la tarea Idle\n";
			add_line(e);
		} else if (get_state(i) == EMPTY) {
			add_line(err_null_pid);
		} else {
			end_task_cmd(i);
		}
	} else {
		add_line(err_no_pid);
	}
}

void c_mallock(char* params, int first) {
	char* c = " Comando reconocido: Reservar una pagina del kernel\n";
	send_message(KERN, c);
	int i = USR;
	if ((params[first] == 32) && (params[first+1] != 10)) {
		params += first;
		params++;
		i = string_to_int(params);
		if ((i < 0) || (i > NUMBER_OF_TASKS)) {
			add_line(err_bad_pid);
		} else if (get_state(i) == EMPTY) {
			add_line(err_null_pid);
		} else malloc_page_k(0, i, OTHER);
	} else malloc_page_k(0, i, OTHER);
}

void c_mallocu(char* params, int first) {
	char* c = " Comando reconocido: Reservar una pagina de usuario\n";
	send_message(KERN, c);
	int i = USR;
	if ((params[first] == 32) && (params[first+1] != 10)) {
		params += first;
		params++;
		i = string_to_int(params);
		if ((i < 0) || (i > NUMBER_OF_TASKS)) {
			add_line(err_bad_pid);
		} else if (get_state(i) == EMPTY) {
			add_line(err_null_pid);
		} else malloc_page_u(0, i, OTHER);
	} else malloc_page_u(0, i, OTHER);
}

char invalid_dir_k(unsigned long i) { return ((i<KERN_PAGES_BASE) || (i>=0x400000) || (i%0x1000 != 0)); }
char invalid_dir_u(unsigned long i) { return ((i<=USER_PAGES_BASE)|| (i>=0x600000) || (i%0x1000 != 0)); }
char invalid_index(int i) { return ((i<0) || (i>=1024) || (i==512)); }

void c_free(char* params, int first) {
	char* c = " Comando reconocido: Liberar una pagina\n";
	send_message(KERN, c);
	int i = 0;
	if ((params[first] == 32) && (params[first+1] != 10)) {
		params += first;
		params++;
		i = string_to_int(params);
		if ((i == 0) && ((params[1] == 'x') || (params[1] == 'X'))) {
			unsigned long d = hex_to_long(params);
			if (invalid_dir_k(d) && invalid_dir_u(d)) {
				char* e1 = " Error: direccion invalida. Debe ser multiplo de 0x1000 y\n";
				add_line(e1);
				char* e2 = " debe pertenecer a [0x200000, 0x400000) U (0x400000, 0x600000)\n";
				add_line(e2);
			} else {
				if (d>=USER_PAGES_BASE) free_page_u(d, KERNEL_CR3);
				else free_page_k(d);
			}
		}
		else {
			if (invalid_index(i)) {
				char* e = " Error: indice invalido. Debe pertenecer a [0, 512) U (512, 1024)\n";
				add_line(e);
			} else {
				if (i >= 512) free_page_u(index_to_dir_u(i-512), KERNEL_CR3);
				else free_page_k(index_to_dir_k(i));
			}
		}
	} else {
		char* e = " Error: Debe ingresar el indice o la direccion fisica de la pagina a liberar\n";
		add_line(e);
	}
}

void c_qup(char* params, int first) {
	char* c = " Comando reconocido: Incrementar el quantum de una tarea\n";
	send_message(KERN, c);
	int i = 0;
	if (params[first] == 32) {
		params += first;
		params++;
		i = string_to_int(params);
		if ((i < 0) || (i > NUMBER_OF_TASKS)) {
			add_line(err_bad_pid);
		} else if (i == 0) {
			add_line(err_idle);
		} else if (get_state(i) == EMPTY) {
			add_line(err_null_pid);
		} else {
			set_quantum(i, get_quantum(i) + 10);
			update_screen_quantum(i);
			update_task_screen_q(i);
		}
	} else {
		add_line(err_no_pid);
	}
}

void c_qdn(char* params, int first) {
	char* c = " Comando reconocido: Decrementar el quantum de una tarea\n";
	send_message(KERN, c);
	int i = 0;
	if (params[first] == 32) {
		params += first;
		params++;
		i = string_to_int(params);
		if ((i < 0) || (i > NUMBER_OF_TASKS)) {;
			add_line(err_bad_pid);
		} else if (i == 0) {
			add_line(err_idle);
		} else if (get_state(i) == EMPTY) {
			add_line(err_null_pid);
		} else {
			set_quantum(i, get_quantum(i) - 10);
			update_screen_quantum(i);
			update_task_screen_q(i);
		}
	} else {
		add_line(err_no_pid);
	}
}

int next_space(char* msg) {
	int res = -1;
	int i = 0;
	while (msg[i] != 10) {
		if (msg[i] == 32) {
			res = i;
			break;
		}
		i++;
	}
	return res;
}

void c_quan(char* params, int first) {
	char* c = " Comando reconocido: Cambiar el quantum de una tarea\n";
	send_message(KERN, c);
	int i = 0;
	int q = 0;
	if (params[first] == 32) {
		params += first;
		params++;
		i = string_to_int(params);
		if ((i < 0) || (i > NUMBER_OF_TASKS)) {
			add_line(err_bad_pid);
		} else if (i == 0) {
			add_line(err_idle);
		} else if (get_state(i) == EMPTY) {
			add_line(err_null_pid);
		} else {
			first = next_space(params);
			if (first == -1) {
				char* e = " Error: Debe ingresar un valor de quantum\n";
				add_line(e);
			} else {
				params += first;
				params ++;
				q = string_to_int(params);
				if ((q < 1) || (q > 999)) {
					char* e = " Error: parametro ivalido. Debe pertencer a [1,999]\n";
					add_line(e);
				} else {
					set_quantum(i, q);
					update_screen_quantum(i);
					update_task_screen_q(i);
				}
			}
		}
	} else {
		add_line(err_no_pid);
	}
}

void c_cat(char* params, int first) {
	char* c = " Comando reconocido: Ejecutar CAT\n";
	send_message(KERN, c);
	char f[90];
	f[89] = 10;
	char* path = "/BIN/CAT.TSKE";
	path[12] = 0;
	strcpy(f, path);
	params[77] = 0;
	params += first;
	strcpy(((char*) f)+12, params);
	run_task(load_task(f));
}

void c_vim(char* params, int first) {
	char* c = " Comando reconocido: Ejecutar VIM\n";
	send_message(KERN, c);
	char f[90];
	f[89] = 10;
	char* path = "/BIN/VIM.TSKE";
	path[12] = 0;
	strcpy(f, path);
	params[77] = 0;
	params += first;
	strcpy(((char*) f)+12, params);
	run_task(load_task(f));
}

void c_play(char* params, int first) {
	char* c = " Comando reconocido: Ejeutar un binario\n";
	send_message(KERN, c);
	if ((params[first] == 10) || (params[first+1] == 10)) {
		add_line(err_no_path);
	} else {
		params+=first;
		params++;
		int r = load_task(params);
		if (r == -1) {
			add_line(err_max_task);
		} else if (r == 0) {
			add_line(err_file_not_found);
		} else {
			char* e = " La tarea se ha cargado con el pid: __\n";
			e[36] = (r/10) + 48;
			e[37] = (r%10) + 48;
			add_line(e);
			update_screen_load(r);
			run_task(r);
			update_screen_run(r);
			task_scrn_view(r);
		}
	}
}

void* functions[] = {	/*0*/	c_help,
						/*1*/	c_term,
						/*2*/	c_sched,
						/*3*/	c_mmu,
						/*4*/	c_reg,
						/*5*/	c_browse,
						/*6*/	c_tsk,
						/*7*/	c_int,
						/*8*/	c_wait,
						/*9*/	c_clear,
						/*10*/	c_user,
						/*11*/	c_mallock,
						/*12*/	c_mallocu,
						/*13*/	c_free,
						/*14*/	c_load,
						/*15*/	c_exec,
						/*16*/	c_run,
						/*17*/	c_start,
						/*18*/	c_play,
						/*19*/	c_kill,
						/*20*/	c_qup,
						/*21*/	c_qdn,
						/*22*/	c_quan,
						/*23*/	c_cat,
						/*24*/	c_vim
					};
char* commands[] = {	/*0*/	"LIST\n",
						/*1*/	"TERM\n",
						/*2*/	"SCHED\n",
						/*3*/	"MMU\n",
						/*4*/	"REG\n",
						/*5*/	"BROWSE\n",
						/*6*/	"TSK\n",
						/*7*/	"INT\n",
						/*8*/	"WAIT\n",
						/*9*/	"CLEAR\n",
						/*10*/	"USER\n",
						/*11*/	"MALL-K\n",
						/*12*/	"MALL-U\n",
						/*13*/	"FREE\n",
						/*14*/	"LOAD\n",
						/*15*/	"EXEC\n",
						/*16*/	"RUN\n",
						/*17*/	"START\n",
						/*18*/	"PLAY\n",
						/*19*/	"KILL\n",
						/*20*/	"Q-UP\n",
						/*21*/	"Q-DN\n",
						/*22*/	"QUAN\n",
						/*23*/	"CAT\n",
						/*24*/	"VIM\n"
					};

void search_command(char* msg) {
	void (*command)(char*, int);
	msg++;
	unsigned int i = 0;
	for (; i < K_COMMANDS; i++) {
		int compare = commandcmp(commands[i], msg);
		if (compare != -1) {
			command = functions[i];
			command(msg, compare);
			break;
		}
	}
}


//if both strings are equal (until the first space) returns the index
//of that space. If they differ before the first space, returns -1
int commandcmp(char* c, char* m) {
		unsigned char i = 0;
		while((c[i] != 10)) {
				//si el mensaje termina antes, no son iguales
				if ((m[i] == 32) || (m[i] == 10)) return -1;
				//si un caracter del mensaje es distinto a uno del comando, no son iguales
				if (m[i] != c[i]) return -1;
				i++;
		}
		//c[i] es 32, si m[i] también (termina el mensaje) son iguales
		if((m[i] == 32) || (m[i] == 10)) return i;
		else return -1;
}
