// term - everything related to the input field
#ifndef __INPUT_H__
#define __INPUT_H__

//adds the char to the input field
void input_char(unsigned char c);

//erases the last char
void input_back();

//moves the pointer to the right
void input_right();

//moves the pointer to the left
void input_left();

//moves the pointer to the beginning
void input_home();

//moves the pointer to the end
void input_end();

//deletes the next char
void input_delete();

//toggles insert mode
void input_insert();

//hit enter!
void input_enter();

//clears the input field
void clear_input();

void paint_pointer();

#endif // __INPUT_H__
