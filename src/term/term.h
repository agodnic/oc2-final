// term - everything related to the terminal
#ifndef __TERM_H__
#define __TERM_H__

//creates the structure to manage messages
void terminal_init();

//sends a message to the terminal
void send_message(int pid, char* msg);

//scrolls 1 line up
void term_up();

//scrolls 1 line down
void term_down();

//scrolls 1 page up (21 lines)
void term_pgup();

//scrolls 1 page down (21 lines)
void term_pgdn();

//scrolls to start
void term_home();

//scrolls to end
void term_end();

//adds one single line to the terminal msgs screen
void add_line(char* msg);

//shows visible messages on the terminal msgs screen
void show_messages();

//erases all messages
void clear_term();

void load_bar();

void rename_user(char* newu);

#endif // __TERM_H__
