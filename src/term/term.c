#include "term.h"

#include "../hw/ia32e.h"
#include "../cui/cui.h"		//para print
#include "../mmu/mmu.h"		//para malloc
#include "../isr/isr.h"		//Para get_menu
#include "../sched/sched.h"	//Para task_name
#include "../cui/clock.h"
#include "../cui/key.h"
#include "../constants.h"
#include "comandos.h"		//Para command_init

char* terminal_buffer;

char* user = "Default user\n  \n";

unsigned char k_lines = 0;		//amount of lines written on terminal screen
unsigned char scroll = 0;		//amount of scroll view
unsigned int terminal_counter = 0;	//the next byte where i will write (until 4096)
unsigned int first_line_counter = 0;//index of the beginning of the first line that is visible

//shows a line on the messages screen of the terminal
void show_line(unsigned char pos, unsigned int start_line) {
	pixel_t p = {pos+2,1};
	print(p, &(terminal_buffer[start_line]));
}

void terminal_init() {
	terminal_buffer = (char*) malloc_page_k(NULL, KERN, TERM);
	command_init();
}

void send_message(int pid, char* msg) {
	char* sender = "Unknown\n";
	if ((pid >= 0) && (pid <= 16)) sender = task_name(pid);
	else if (pid == KERN) sender = "kernel\n";
	else if (pid == USR) sender = user;
	char tmp_m[78];
	unsigned int c = 0;
	tmp_m[0] = icon(pid);
	tmp_m[1] = 32;
	while (sender[c] != 10) {
		tmp_m[c+2] = sender[c];
		c++;
	}
	tmp_m[c+2] = 32; tmp_m[c+3] = 's'; tmp_m[c+4] = 'a'; tmp_m[c+5] = 'i';
	tmp_m[c+6] = 'd'; tmp_m[c+7] = ':'; tmp_m[c+8] = 10;
	add_line(tmp_m);
	add_line(msg);
}

void clear_view() {
	pixel_t s = {2,1};
	pixel_t e = {22,78};
	paint_c(s,e, (get_focus() ? BG_WHITE | CHAR_BLACK : BG_CYAN | CHAR_BLACK), 32);
}

unsigned int search_previous_line(unsigned int line) {
	unsigned int result = line;
	result-=2;
	while ((terminal_buffer[result] != 10) && (result > 0)) {
		result--;
	}
	if (result>0) result++;
	return result;
}

unsigned int search_next_line(unsigned int line) {
	unsigned int result = line;
	while (terminal_buffer[result] != 10) {
		result++;
	}
	result++;
	return result;
}

void term_up() {
	if (scroll > 0) {
		clear_view();
		scroll--;
		first_line_counter = search_previous_line(first_line_counter);
		show_messages();
	}
}

void term_down() {
	if (scroll + 21 < k_lines) {
		if (get_menu() ==1) clear_view();
		scroll++;
		first_line_counter = search_next_line(first_line_counter);
		if (get_menu() ==1) show_messages();
	}
}

void term_pgup() {
	unsigned int i = 20;
	while ((i > 0) && (scroll > 0)) {
		term_up();
		wait(1);
		i--;
	}
}

void term_pgdn() {
	unsigned int i = 20;
	while ((i > 0) && (scroll + 21 < k_lines)) {
		term_down();
		wait(1);
		i--;
	}
}

void term_home() {
	clear_view();
	scroll = 0;
	first_line_counter = 0;
	show_messages();
}

void term_end() {
	while (scroll + 21 < k_lines) {
		term_down();
	}
}

void add_line(char* msg) {
	unsigned int tmp_counter = terminal_counter;
	unsigned int counter = 0;
	while ((counter<=77) && (msg[counter] != 10 ) && (terminal_counter < 4095)) {
		terminal_buffer[terminal_counter] = msg[counter];
		counter++;
		terminal_counter++;
	}
	if (counter==78) msg[counter] = 10;
	if (terminal_counter == 4095) terminal_buffer[terminal_counter] = 10;
	else terminal_buffer[terminal_counter] = msg[counter];
	if (tmp_counter != terminal_counter) {
		terminal_counter++;
		k_lines++;
		if (k_lines <= 21) {
			if (get_menu() == 1) show_line(scroll+k_lines-1, tmp_counter);
		} else if (scroll == k_lines-22) {
			term_down();
		}
		if (get_menu() < 5) load_bar();
	}
}

void show_messages() {
	unsigned char line = 0;
	unsigned int line_counter = first_line_counter;
	for(; ((line < LINES_VIEW) && (scroll+line < k_lines)); line++ ) {
		show_line(line, line_counter);
		line_counter = search_next_line(line_counter);
	}
}

void clear_term() {
	k_lines = 0;
	scroll = 0;
	terminal_counter = 0;
	first_line_counter = 0;
	if (get_menu() < 6) {
		if (get_menu()==1) clear_view();
		pixel_t s = {1,0};
		pixel_t e = {1,79};
		paint(s, e, BG_PURPLE | BRIGHT | CHAR_YELLOW);
	}
}

void load_bar() {
	pixel_t s = {1,0};
	pixel_t e = {1,(terminal_counter/51) - 1};
	paint(s, e, (terminal_counter > 4020 ? BG_RED : BG_GREEN) | BRIGHT | CHAR_YELLOW);
}

void rename_user(char* newu) {
	int i = 0;
	for (; (i < 15) && (newu[i] != 10); i++) {
		user[i] = newu[i];
	}
	user[i] = 10;
}
