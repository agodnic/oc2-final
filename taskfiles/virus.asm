BITS 64
org 0x400000

start:
	;rename task as 'Virus'
	mov RDI, 10
	mov RSI, name
	int 36

	;obtain pid and save it on my_pid label
	mov RDI, 11
	int 36
	mov RDX, my_pid
	mov dword [RDX], EAX

	;send message to kernel terminal
	mov RDI, 2
	mov RSI, msg
	int 36

	;Allocate pages
	mov RDI, 0
	mov	RCX, 10
loopm:
	nop
	nop
	nop
	nop
	nop
	nop

	;allocate 1 page
	int 36

	loop loopm

	;run another like me
	mov	RDI, 12
	mov RSI, path
	int 36

	;exit
	mov RDI, 9
	int 36

	jmp $

name:
	DB 'Virus', 10

path:
	DB '/BIN/VIRUS.TSK', 10

msg:
	DB ' Tu sistema ha sido infectado!!', 10

my_pid:
	DD 10
