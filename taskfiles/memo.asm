BITS 64
org 0x400000

start:
	;rename task as 'Allocator'
	mov RDI, 10
	mov RSI, name
	int 36

	;obtain pid and save it on my_pid label
	mov RDI, 11
	int 36
	mov RDX, my_pid
	mov dword [RDX], EAX

	;send message to kernel terminal
	mov RDI, 2
	mov RSI, msg1
	int 36

	;wait for key '1'
	mov RDI, 6
	mov RSI, 2
	int 36

	mov RDX, pages
	mov	RCX, 5
loopm:

	;allocate 1 page
	mov RDI, 0
	push RDX
	int 36

	pop	RDX
	mov qword [RDX], RAX

	add RDX, 8

	loop loopm

	;send message to kernel terminal
	mov RDI, 2
	mov RSI, msg2
	int 36

	;wait for key '2'
	mov RDI, 6
	mov RSI, 3
	int 36

	mov RDX, pages
	mov	RCX, 5
loopf:

	;free 1 page
	mov RDI, 1
	mov RSI, [RDX]
	push	RDX
	int 36
	pop		RDX

	add RDX, 8

	loop loopf

	;send message to kernel terminal
	mov RDI, 2
	mov RSI, msg3
	int 36

	;wait for key '3'
	mov RDI, 6
	mov RSI, 4
	int 36

	;exit
	mov RDI, 9
	int 36

	jmp $

name:
	DB 'Allocator', 10

msg1:
	DB ' Necesito 5 paginas de memoria. Presione 1 para continuar', 10

msg2:
	DB ' Ya tengo 5 paginas, ahora las devuelvo. Presione 2 para continuar', 10

msg3:
	DB ' Ya termine. Presiona 3 para finalizar', 10

my_pid:
	DD 10

pages:
	DQ 1,2,3,4,5
