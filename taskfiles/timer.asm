BITS 64
org 0x400000

start:
	;rename task as 'Alarma'
	mov RDI, 10
	mov RSI, name
	int 36

	;obtain pid and save it on my_pid label
	mov RDI, 11
	int 36
	mov RDX, my_pid
	mov dword [RDX], EAX

	;send message to kernel terminal
	mov RDI, 2
	mov RSI, msg1
	int 36

loop:
	;wait 50 ticks
	mov RDI, 5
	mov RSI, 50
	int 36

	;send message to kernel terminal
	mov RDI, 2
	mov RSI, msg2
	int 36

	jmp loop

name:
	DB 'Timer', 10

msg1:
	DB ' Voy a enviar un mensaje cada 50 ticks',10

msg2:
	DB ' Pasaron 50 ticks',10

my_pid:
	DD 10
