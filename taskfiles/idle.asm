BITS 64
org 0x400000

start:
	;rename task as 'Idle'
	mov RDI, 10
	mov RSI, name
	int 36

	;obtain pid and save it on my_pid label
	mov RDI, 11
	int 36
	mov RDX, my_pid
	mov dword [RDX], EAX

	;int to string(pid)
	mov RDI, 7
	mov RSI, RAX
	mov RDX, spid
	mov RCX, 2
	int 36

	;send message to kernel terminal
	mov RDI, 2
	mov RSI, msg
	int 36

	;loop
loop:
	nop
	nop
	nop
	nop
	nop
	nop
	jmp loop

name:
	DB 'Idle', 10

msg:
	DB ' Hola, soy la tarea Idle y Mi pid es '

spid:
	DB '  ', 10

my_pid:
	DD 10
