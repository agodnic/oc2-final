BITS 64
org 0x400000

start:
	;rename task as 'Epilepsia'
	mov RDI, 10
	mov RSI, name
	int 36

	;obtain pid and save it on my_pid label
	mov RDI, 11
	int 36
	mov RDX, my_pid
	mov dword [RDX], EAX

	;send message to kernel terminal
	mov RDI, 2
	mov RSI, msg
	int 36

	;paint on screen
	mov	RDI, 3
	mov RSI, 0
	mov	RDX, 0
	mov RCX, 0
	mov	R8, 80
	mov	R9, 22

paint:
	int 36

	mov 	RDI, 5
	push	RSI
	mov		RSI, 2
	int		36
	pop		RSI
	mov		RDI, 3
	inc		SI

	jmp paint

name:
	DB 'Epilepsia', 10

msg:
	DB ' No mires mi pantalla',10

my_pid:
	DD 10
