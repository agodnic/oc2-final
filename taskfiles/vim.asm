BITS 64
org 0x400000

start:
	mov R10, RDI
	mov R11, RSI
	;rename task as 'VIM'
	mov RDI, 10
	mov RSI, name
	int 36

	cmp R10, 2
	jl noparam

	;write file
	mov RDI, 15
	mov RSI, [R11]
	add R11, 8
	mov RDX, [R11]
	mov RCX, 6
	int 36

	;send message  to kernel terminal
	mov RDI, 2
	mov RSI, msg2
	int 36
	jmp end

noparam:
	;send message  to kernel terminal
	mov RDI, 2
	mov RSI, np
	int 36

end:
	;exit
	mov RDI, 9
	int 36

name:
	DB 'VIM', 10

np:
	DB ' Error: Debe pasar un archivo y una cadena como parametro',10

msg2:
	DB ' Archivo guardado',10
