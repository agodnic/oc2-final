BITS 64
org 0x400000

start:
	mov RCX, RDI
	mov R10, RSI

	;obtain pid and save it on my_pid label
	mov RDI, 11
	int 36
	mov RDX, my_pid
	mov dword [RDX], EAX

	cmp qword RCX, 0
	je no_params

	;send message to kernel terminal
	mov RDI, 2
	mov RSI, msg1
	int 36

loop_param:
	mov RSI, [R10]

	;send message to kernel terminal
	mov RDI, 2
	int 36

	add R10, 8
	loop loop_param

	;send message to kernel terminal
	mov RDI, 2
	mov RSI, msg3
	int 36
	jmp end

no_params:
	;send message to kernel terminal
	mov RDI, 2
	mov RSI, msgnp
	int 36

end:
	;exit
	mov RDI, 9
	int 36

	jmp $

msgnp:
	DB ' Hola otra vez, ya me voy',10

msg1:
	DB ' Hola otra vez, me mandaste estos parametros:',10

msg3:
	DB ' ya me voy',10

my_pid:
	DD 10
