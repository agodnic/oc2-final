BITS 64
org 0x400000

start:
	;rename task as 'Deletreador'
	mov RDI, 10
	mov RSI, name
	int 36

	;obtain pid and save it on my_pid label
	mov RDI, 11
	int 36
	mov RDX, my_pid
	mov dword [RDX], EAX

	;send message to kernel terminal
	mov RDI, 2
	mov RSI, msg1
	int 36

	;send message to kernel terminal
	mov RDI, 2
	mov RSI, msg2
	int 36

	;wait for key 'H'
	mov RDI, 6
	mov RSI, 35
	int 36

	mov RCX, 8
	mov R8, keys
	mov R9, letters
loop:
	mov byte DL, [R9]
	mov byte [letter], DL
	push r9
	push r8

	;send message to kernel terminal
	mov RDI, 2
	mov RSI, msg3
	int 36

	pop r8
	;wait for key
	mov RDI, 6
	mov byte SIL, [R8]
	push r8
	int 36

	pop r8
	pop r9
	inc R8
	inc R9
	loop loop

	;send message to kernel terminal
	mov RDI, 2
	mov RSI, msg4
	int 36

	;exit
	mov RDI, 9
	int 36

	jmp $

name:
	DB 'Deletreador', 10

msg1:
	DB ' Voy a enseniarte a deletrar HOLA MUNDO',10

msg2:
	DB ' Primero oprime la tecla H',10

msg3:
	DB ' Muy bien, ahora oprime la tecla '

letter:
	DB 'X',10

msg4:
	DB ' Perfecto, Aprendiste a deletrear HOLA MUNDO. Adios',10

my_pid:
	DD 10

letters:
	DB 'OLAMUNDO'

keys:
	DB 24,38,30,50,22,49,32,24
