BITS 64
org 0x400000

start:
	mov R10, RDI
	mov R11, RSI
	;rename task as 'CAT'
	mov RDI, 10
	mov RSI, name
	int 36

	mov byte[buf+512], 10
	cmp R10, 0
	je noparam

	;read file
	mov RDI, 8
	mov RSI, [R11]
	mov RDX, buf
	int 36

	cmp qword RAX, -1
	je error

	;send message  to kernel terminal
	mov RDI, 2
	mov RSI, msg2
	int 36
	jmp end

noparam:
	;send message  to kernel terminal
	mov RDI, 2
	mov RSI, np
	int 36
	jmp end

error:
	;send message  to kernel terminal
	mov RDI, 2
	mov RSI, err
	int 36	

end:
	;exit
	mov RDI, 9
	int 36

name:
	DB 'CAT', 10

err:
	DB ' Error: Archivo no encontrado',10

np:
	DB ' Error: Debe pasar un archivo como parametro',10

msg2:
	DB ' Contenido del archivo: '
buf:
